<?php

namespace App\Form\RessourceHumaine;

use App\Entity\RessourceHumaine\Candidat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\DateType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class CandidatType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("civilite", ChoiceType::class, [
                    'required' => false,
                    'choices' => [
                        'Ms' => 'Ms',
                        'Mr' => 'Mr',
                        'Mrs' => 'Mrs'
                    ]
                ]
            )
            ->add("nomPrenom")
            ->add("numIdentite")
            ->add("dateIdentite", DateType::class, [
                "required" => true,
                "widget" => "single_text",
                "format" => DateType::HTML5_FORMAT,
            ])
            ->add("dateNaissance", DateType::class, [
                "required" => true,
                "widget" => "single_text",
                "format" => DateType::HTML5_FORMAT,
            ])
            ->add("lieuNaissance")
            ->add("gouvernorat", null, [
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("adresse")
            ->add("tel1")
            ->add("tel2")
            ->add("tel3")
            ->add("modeService", null, [
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("typeService", null, [
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("repos", null, [
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("anneeInscrire")
            ->add("numFiche")
            ->add("archive", null, [
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("etoile", IntegerType::class,[
                  "required" => false
                                ])
            ->add("remarque", CKEditorType::class, [
                    "required" => false,
                "config" => [
                    "uiColor" => "#ffffff",
                ]])
            ->add("cariere", CKEditorType::class, [
                    "required" => false,
                "config" => [
                    "uiColor" => "#ffffff",
                ]])
            ->add("imgIdentiteRectoFile", FileType::class, [
                "required" => false,
                "attr" => [
                    "class" => "input-file",
                    "data-show-caption" => "false",
                    "data-show-upload" => "false"
                ]
            ])
            ->add("imgIdentiteRectoRemove", CheckboxType::class, [
                "required" => false,
                "label" => false,
                "attr" => [
                    "hidden" => true
                ]
            ])
            ->add("imgIdentiteVersoFile", FileType::class, [
                "required" => false,
                "attr" => [
                    "class" => "input-file",
                    "data-show-caption" => "false",
                    "data-show-upload" => "false"
                ]
            ])
            ->add("imgIdentiteVersoRemove", CheckboxType::class, [
                "required" => false,
                "label" => false,
                "attr" => [
                    "hidden" => true
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Candidat::class,
        ]);
    }

}
