<?php

namespace App\Form\RessourceHumaine\Search;

use App\Entity\RessourceHumaine\Search\ClientSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use App\Entity\Referentiel\Gouvernorat;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ClientSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("id", HiddenType::class,[
                "required" => false
            ])
            ->add("nomPrenom", null,[
                "required" => false
            ])
            ->add("cin", null,[
                "required" => false
            ])
            ->add("dateCin_de", DateType::class, [
                "required" => false,
                "widget" => "single_text",
                "format" => DateType::HTML5_FORMAT,
            ])
            ->add("dateCin_au", DateType::class, [
                "required" => false,
                "widget" => "single_text",
                "format" => DateType::HTML5_FORMAT,
            ])
            ->add("tel", null,[
                "required" => false
            ])
            ->add("gsm", null,[
                "required" => false
            ])
            ->add("fax", null,[
                "required" => false
            ])
            ->add("email", null,[
                "required" => false
            ])
            ->add("adresse", null,[
                "required" => false
            ])
            ->add("avis_de", IntegerType::class,[
                "required" => false
            ])
            ->add("avis_au", IntegerType::class,[
                "required" => false
            ])
            ->add("gouvernorat", EntityType::class, [
                "class"=>Gouvernorat::class,
                "required" => false,
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => ClientSearch::class,
            "method"=>"get",
            "csrf_protection"=>false
        ]);
    }

    public function getBlockPrefix()
    {
        return "filtre";
    }

}
