<?php

namespace App\Form\RessourceHumaine\Search;

use App\Entity\RessourceHumaine\Search\CandidatSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use App\Entity\Referentiel\Gouvernorat;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Referentiel\ModeService;
use App\Entity\Referentiel\TypeService;
use App\Entity\Referentiel\Repos;
use App\Entity\Referentiel\Archive;

class CandidatSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("id", HiddenType::class,[
                "required" => false
            ])
            ->add("civilite", ChoiceType::class, [
                    'required' => false,
                    'choices' => [
                        'Ms' => 'Ms',
                        'Mr' => 'Mr',
                        'Mrs' => 'Mrs'
                    ]
                ]
            )
            ->add("nomPrenom", null,[
                "required" => false
            ])
            ->add("numIdentite", null,[
                "required" => false
            ])
            ->add("dateNaissance_de", DateType::class, [
                "required" => false,
                "widget" => "single_text",
                "format" => DateType::HTML5_FORMAT,
            ])
            ->add("dateNaissance_au", DateType::class, [
                "required" => false,
                "widget" => "single_text",
                "format" => DateType::HTML5_FORMAT,
            ])
            ->add("lieuNaissance", null,[
                "required" => false
            ])
            ->add("gouvernorat", EntityType::class, [
                "class"=>Gouvernorat::class,
                "required" => false,
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("adresse", null,[
                "required" => false
            ])
            ->add("tel1", null,[
                "required" => false
            ])
            ->add("tel2", null,[
                "required" => false
            ])
            ->add("tel3", null,[
                "required" => false
            ])
            ->add("modeService", EntityType::class, [
                "class"=>ModeService::class,
                "required" => false,
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("typeService", EntityType::class, [
                "class"=>TypeService::class,
                "required" => false,
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("repos", EntityType::class, [
                "class"=>Repos::class,
                "required" => false,
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("anneeInscrire", null,[
                "required" => false
            ])
            ->add("numFiche", null,[
                "required" => false
            ])
            ->add("archive", EntityType::class, [
                "class"=>Archive::class,
                "required" => false,
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("etoile_de", IntegerType::class,[
                "required" => false
            ])
            ->add("etoile_au", IntegerType::class,[
                "required" => false
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => CandidatSearch::class,
            "method"=>"get",
            "csrf_protection"=>false
        ]);
    }

    public function getBlockPrefix()
    {
        return "filtre";
    }

}
