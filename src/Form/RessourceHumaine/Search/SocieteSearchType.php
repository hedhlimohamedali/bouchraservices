<?php

namespace App\Form\RessourceHumaine\Search;

use App\Entity\RessourceHumaine\Search\SocieteSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use App\Entity\Referentiel\Gouvernorat;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SocieteSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("id", HiddenType::class,[
                "required" => false
            ])
            ->add("nom", null,[
                "required" => false
            ])
            ->add("code", null,[
                "required" => false
            ])
            ->add("activite", null,[
                "required" => false
            ])
            ->add("gerant", null,[
                "required" => false
            ])
            ->add("tel", null,[
                "required" => false
            ])
            ->add("gsm", null,[
                "required" => false
            ])
            ->add("fax", null,[
                "required" => false
            ])
            ->add("email", null,[
                "required" => false
            ])
            ->add("gouvernorat", EntityType::class, [
                "class"=>Gouvernorat::class,
                "required" => false,
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("adresse", null,[
                "required" => false
            ])
            ->add("avis_de", IntegerType::class,[
                "required" => false
            ])
            ->add("avis_au", IntegerType::class,[
                "required" => false
            ])
            ->add("rib", null,[
                "required" => false
            ])
            ->add("matriculeFiscale", null,[
                "required" => false
            ])
            ->add("registreCommerce", null,[
                "required" => false
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => SocieteSearch::class,
            "method"=>"get",
            "csrf_protection"=>false
        ]);
    }

    public function getBlockPrefix()
    {
        return "filtre";
    }

}
