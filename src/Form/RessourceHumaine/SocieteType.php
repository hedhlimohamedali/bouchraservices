<?php

namespace App\Form\RessourceHumaine;

use App\Entity\RessourceHumaine\Societe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class SocieteType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("nom")
            ->add("code")
            ->add("activite")
            ->add("gerant")
            ->add("tel")
            ->add("gsm")
            ->add("fax")
            ->add("email")
            ->add("gouvernorat", null, [
                "attr" => [
                    "class" => "select-search"
                ]
            ])
            ->add("adresse")
            ->add("avis", IntegerType::class,[
                  "required" => false
                                ])
            ->add("rib")
            ->add("matriculeFiscale")
            ->add("registreCommerce")
            ->add("remarque", CKEditorType::class, [
                    "required" => false,
                "config" => [
                    "uiColor" => "#ffffff",
                ]])
            ->add("pieceJointFile", FileType::class, [
                "required" => false,
                "attr" => [
                    "class" => "input-file",
                    "data-show-caption" => "false",
                    "data-show-upload" => "false"
                ]
            ])
            ->add("pieceJointRemove", CheckboxType::class, [
                "required" => false,
                "label" => false,
                "attr" => [
                    "hidden" => true
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Societe::class,
        ]);
    }

}
