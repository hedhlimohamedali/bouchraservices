<?php
/**
 * Created by PhpStorm.
 * User: MohamedAli
 * Date: 21/04/2020
 * Time: 01:46
 */

namespace App\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use Twig\TwigFunction;
use Twig\TwigFilter;
use Twig\Extension\AbstractExtension;

class GeneraleExtension extends AbstractExtension
{
    private $em;

    /**
     * GeneraleExtension constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('InfoGeneral', [$this, 'InfoGeneral']),
            new TwigFunction('fileIsImage', [$this, 'fileIsImage']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('viewBooleanValue', [$this, 'viewBooleanValue']),
        ];
    }

    public function InfoGeneral()
    {
        return [

        ];
    }

    public function fileIsImage($mediapath)
    {
        if(@is_array(getimagesize($mediapath)))
            return true;
        return false;
    }

    public function viewBooleanValue($val)
    {
        if ($val)
            return '<i class="icon-checkmark4" style="color: green"></i>';
        return '<i class="icon-cross2" style="color: red"></i>';
    }
}