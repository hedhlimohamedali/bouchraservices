<?php

namespace App\Entity\RessourceHumaine\Search;

use App\Entity\Referentiel\Gouvernorat;
use App\Entity\Referentiel\ModeService;
use App\Entity\Referentiel\TypeService;
use App\Entity\Referentiel\Repos;
use App\Entity\Referentiel\Archive;

class CandidatSearch
{
    /** @var int|null */
    private $id;

    /** @var string|null */
    private $civilite;

    /** @var string|null */
    private $nomPrenom;

    /** @var string|null */
    private $numIdentite;

    /** @var \DateTime|null */
    private $dateNaissance_de;

    /** @var \DateTime|null */
    private $dateNaissance_au;

    /** @var string|null */
    private $lieuNaissance;

    /** @var Gouvernorat|null */
    private $gouvernorat;

    /** @var string|null */
    private $adresse;

    /** @var string|null */
    private $tel1;

    /** @var string|null */
    private $tel2;

    /** @var string|null */
    private $tel3;

    /** @var ModeService|null */
    private $modeService;

    /** @var TypeService|null */
    private $typeService;

    /** @var Repos|null */
    private $repos;

    /** @var string|null */
    private $anneeInscrire;

    /** @var string|null */
    private $numFiche;

    /** @var Archive|null */
    private $archive;

    /** @var int|null */
    private $etoile_de;

    /** @var int|null */
    private $etoile_au;

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): CandidatSearch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    /**
     * @param null|string $civilite
     * @return CandidatSearch
     */
    public function setCivilite(?string $civilite): CandidatSearch
    {
        $this->civilite = $civilite;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNomPrenom(): ?string
    {
        return $this->nomPrenom;
    }

    /**
     * @param null|string $nomPrenom
     * @return CandidatSearch
     */
    public function setNomPrenom(?string $nomPrenom): CandidatSearch
    {
        $this->nomPrenom = $nomPrenom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNumIdentite(): ?string
    {
        return $this->numIdentite;
    }

    /**
     * @param null|string $numIdentite
     * @return CandidatSearch
     */
    public function setNumIdentite(?string $numIdentite): CandidatSearch
    {
        $this->numIdentite = $numIdentite;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getDateNaissanceDe(): ?\DateTime
    {
        return $this->dateNaissance_de;
    }

    /**
     * @param null|\DateTime $dateNaissance
     * @return CandidatSearch
     */
    public function setDateNaissanceDe(?\DateTime $dateNaissance): CandidatSearch
    {
        $this->dateNaissance_de = $dateNaissance;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getDateNaissanceAu(): ?\DateTime
    {
        return $this->dateNaissance_au;
    }

    /**
     * @param null|\DateTime $dateNaissance
     * @return CandidatSearch
     */
    public function setDateNaissanceAu(?\DateTime $dateNaissance): CandidatSearch
    {
        $this->dateNaissance_au = $dateNaissance;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLieuNaissance(): ?string
    {
        return $this->lieuNaissance;
    }

    /**
     * @param null|string $lieuNaissance
     * @return CandidatSearch
     */
    public function setLieuNaissance(?string $lieuNaissance): CandidatSearch
    {
        $this->lieuNaissance = $lieuNaissance;
        return $this;
    }

    /**
     * @return null|Gouvernorat
     */
    public function getGouvernorat(): ?Gouvernorat
    {
        return $this->gouvernorat;
    }

    /**
     * @param null|Gouvernorat $gouvernorat
     * @return CandidatSearch
     */
    public function setGouvernorat(?Gouvernorat $gouvernorat): CandidatSearch
    {
        $this->gouvernorat = $gouvernorat;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    /**
     * @param null|string $adresse
     * @return CandidatSearch
     */
    public function setAdresse(?string $adresse): CandidatSearch
    {
        $this->adresse = $adresse;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    /**
     * @param null|string $tel1
     * @return CandidatSearch
     */
    public function setTel1(?string $tel1): CandidatSearch
    {
        $this->tel1 = $tel1;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    /**
     * @param null|string $tel2
     * @return CandidatSearch
     */
    public function setTel2(?string $tel2): CandidatSearch
    {
        $this->tel2 = $tel2;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTel3(): ?string
    {
        return $this->tel3;
    }

    /**
     * @param null|string $tel3
     * @return CandidatSearch
     */
    public function setTel3(?string $tel3): CandidatSearch
    {
        $this->tel3 = $tel3;
        return $this;
    }

    /**
     * @return null|ModeService
     */
    public function getModeService(): ?ModeService
    {
        return $this->modeService;
    }

    /**
     * @param null|ModeService $modeService
     * @return CandidatSearch
     */
    public function setModeService(?ModeService $modeService): CandidatSearch
    {
        $this->modeService = $modeService;
        return $this;
    }

    /**
     * @return null|TypeService
     */
    public function getTypeService(): ?TypeService
    {
        return $this->typeService;
    }

    /**
     * @param null|TypeService $typeService
     * @return CandidatSearch
     */
    public function setTypeService(?TypeService $typeService): CandidatSearch
    {
        $this->typeService = $typeService;
        return $this;
    }

    /**
     * @return null|Repos
     */
    public function getRepos(): ?Repos
    {
        return $this->repos;
    }

    /**
     * @param null|Repos $repos
     * @return CandidatSearch
     */
    public function setRepos(?Repos $repos): CandidatSearch
    {
        $this->repos = $repos;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAnneeInscrire(): ?string
    {
        return $this->anneeInscrire;
    }

    /**
     * @param null|string $anneeInscrire
     * @return CandidatSearch
     */
    public function setAnneeInscrire(?string $anneeInscrire): CandidatSearch
    {
        $this->anneeInscrire = $anneeInscrire;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNumFiche(): ?string
    {
        return $this->numFiche;
    }

    /**
     * @param null|string $numFiche
     * @return CandidatSearch
     */
    public function setNumFiche(?string $numFiche): CandidatSearch
    {
        $this->numFiche = $numFiche;
        return $this;
    }

    /**
     * @return null|Archive
     */
    public function getArchive(): ?Archive
    {
        return $this->archive;
    }

    /**
     * @param null|Archive $archive
     * @return CandidatSearch
     */
    public function setArchive(?Archive $archive): CandidatSearch
    {
        $this->archive = $archive;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getEtoileDe(): ?int
    {
        return $this->etoile_de;
    }

    /**
     * @param null|int $etoile
     * @return CandidatSearch
     */
    public function setEtoileDe(?int $etoile): CandidatSearch
    {
        $this->etoile_de = $etoile;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getEtoileAu(): ?int
    {
        return $this->etoile_au;
    }

    /**
     * @param null|int $etoile
     * @return CandidatSearch
     */
    public function setEtoileAu(?int $etoile): CandidatSearch
    {
        $this->etoile_au = $etoile;
        return $this;
    }

}
