<?php

namespace App\Entity\RessourceHumaine\Search;

use App\Entity\Referentiel\Gouvernorat;

class ClientSearch
{
    /** @var int|null */
    private $id;

    /** @var string|null */
    private $nomPrenom;

    /** @var string|null */
    private $cin;

    /** @var \DateTime|null */
    private $dateCin_de;

    /** @var \DateTime|null */
    private $dateCin_au;

    /** @var string|null */
    private $tel;

    /** @var string|null */
    private $gsm;

    /** @var string|null */
    private $fax;

    /** @var string|null */
    private $email;

    /** @var string|null */
    private $adresse;

    /** @var int|null */
    private $avis_de;

    /** @var int|null */
    private $avis_au;

    /** @var Gouvernorat|null */
    private $gouvernorat;

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ClientSearch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNomPrenom(): ?string
    {
        return $this->nomPrenom;
    }

    /**
     * @param null|string $nomPrenom
     * @return ClientSearch
     */
    public function setNomPrenom(?string $nomPrenom): ClientSearch
    {
        $this->nomPrenom = $nomPrenom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCin(): ?string
    {
        return $this->cin;
    }

    /**
     * @param null|string $cin
     * @return ClientSearch
     */
    public function setCin(?string $cin): ClientSearch
    {
        $this->cin = $cin;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getDateCinDe(): ?\DateTime
    {
        return $this->dateCin_de;
    }

    /**
     * @param null|\DateTime $dateCin
     * @return ClientSearch
     */
    public function setDateCinDe(?\DateTime $dateCin): ClientSearch
    {
        $this->dateCin_de = $dateCin;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getDateCinAu(): ?\DateTime
    {
        return $this->dateCin_au;
    }

    /**
     * @param null|\DateTime $dateCin
     * @return ClientSearch
     */
    public function setDateCinAu(?\DateTime $dateCin): ClientSearch
    {
        $this->dateCin_au = $dateCin;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTel(): ?string
    {
        return $this->tel;
    }

    /**
     * @param null|string $tel
     * @return ClientSearch
     */
    public function setTel(?string $tel): ClientSearch
    {
        $this->tel = $tel;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getGsm(): ?string
    {
        return $this->gsm;
    }

    /**
     * @param null|string $gsm
     * @return ClientSearch
     */
    public function setGsm(?string $gsm): ClientSearch
    {
        $this->gsm = $gsm;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }

    /**
     * @param null|string $fax
     * @return ClientSearch
     */
    public function setFax(?string $fax): ClientSearch
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     * @return ClientSearch
     */
    public function setEmail(?string $email): ClientSearch
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    /**
     * @param null|string $adresse
     * @return ClientSearch
     */
    public function setAdresse(?string $adresse): ClientSearch
    {
        $this->adresse = $adresse;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getAvisDe(): ?int
    {
        return $this->avis_de;
    }

    /**
     * @param null|int $avis
     * @return ClientSearch
     */
    public function setAvisDe(?int $avis): ClientSearch
    {
        $this->avis_de = $avis;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getAvisAu(): ?int
    {
        return $this->avis_au;
    }

    /**
     * @param null|int $avis
     * @return ClientSearch
     */
    public function setAvisAu(?int $avis): ClientSearch
    {
        $this->avis_au = $avis;
        return $this;
    }

    /**
     * @return null|Gouvernorat
     */
    public function getGouvernorat(): ?Gouvernorat
    {
        return $this->gouvernorat;
    }

    /**
     * @param null|Gouvernorat $gouvernorat
     * @return ClientSearch
     */
    public function setGouvernorat(?Gouvernorat $gouvernorat): ClientSearch
    {
        $this->gouvernorat = $gouvernorat;
        return $this;
    }

}
