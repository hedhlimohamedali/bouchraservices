<?php

namespace App\Entity\RessourceHumaine\Search;

use App\Entity\Referentiel\Gouvernorat;

class SocieteSearch
{
    /** @var int|null */
    private $id;

    /** @var string|null */
    private $nom;

    /** @var string|null */
    private $code;

    /** @var string|null */
    private $activite;

    /** @var string|null */
    private $gerant;

    /** @var string|null */
    private $tel;

    /** @var string|null */
    private $gsm;

    /** @var string|null */
    private $fax;

    /** @var string|null */
    private $email;

    /** @var Gouvernorat|null */
    private $gouvernorat;

    /** @var string|null */
    private $adresse;

    /** @var int|null */
    private $avis_de;

    /** @var int|null */
    private $avis_au;

    /** @var string|null */
    private $rib;

    /** @var string|null */
    private $matriculeFiscale;

    /** @var string|null */
    private $registreCommerce;

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): SocieteSearch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param null|string $nom
     * @return SocieteSearch
     */
    public function setNom(?string $nom): SocieteSearch
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     * @return SocieteSearch
     */
    public function setCode(?string $code): SocieteSearch
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getActivite(): ?string
    {
        return $this->activite;
    }

    /**
     * @param null|string $activite
     * @return SocieteSearch
     */
    public function setActivite(?string $activite): SocieteSearch
    {
        $this->activite = $activite;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getGerant(): ?string
    {
        return $this->gerant;
    }

    /**
     * @param null|string $gerant
     * @return SocieteSearch
     */
    public function setGerant(?string $gerant): SocieteSearch
    {
        $this->gerant = $gerant;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTel(): ?string
    {
        return $this->tel;
    }

    /**
     * @param null|string $tel
     * @return SocieteSearch
     */
    public function setTel(?string $tel): SocieteSearch
    {
        $this->tel = $tel;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getGsm(): ?string
    {
        return $this->gsm;
    }

    /**
     * @param null|string $gsm
     * @return SocieteSearch
     */
    public function setGsm(?string $gsm): SocieteSearch
    {
        $this->gsm = $gsm;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }

    /**
     * @param null|string $fax
     * @return SocieteSearch
     */
    public function setFax(?string $fax): SocieteSearch
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     * @return SocieteSearch
     */
    public function setEmail(?string $email): SocieteSearch
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return null|Gouvernorat
     */
    public function getGouvernorat(): ?Gouvernorat
    {
        return $this->gouvernorat;
    }

    /**
     * @param null|Gouvernorat $gouvernorat
     * @return SocieteSearch
     */
    public function setGouvernorat(?Gouvernorat $gouvernorat): SocieteSearch
    {
        $this->gouvernorat = $gouvernorat;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    /**
     * @param null|string $adresse
     * @return SocieteSearch
     */
    public function setAdresse(?string $adresse): SocieteSearch
    {
        $this->adresse = $adresse;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getAvisDe(): ?int
    {
        return $this->avis_de;
    }

    /**
     * @param null|int $avis
     * @return SocieteSearch
     */
    public function setAvisDe(?int $avis): SocieteSearch
    {
        $this->avis_de = $avis;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getAvisAu(): ?int
    {
        return $this->avis_au;
    }

    /**
     * @param null|int $avis
     * @return SocieteSearch
     */
    public function setAvisAu(?int $avis): SocieteSearch
    {
        $this->avis_au = $avis;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getRib(): ?string
    {
        return $this->rib;
    }

    /**
     * @param null|string $rib
     * @return SocieteSearch
     */
    public function setRib(?string $rib): SocieteSearch
    {
        $this->rib = $rib;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMatriculeFiscale(): ?string
    {
        return $this->matriculeFiscale;
    }

    /**
     * @param null|string $matriculeFiscale
     * @return SocieteSearch
     */
    public function setMatriculeFiscale(?string $matriculeFiscale): SocieteSearch
    {
        $this->matriculeFiscale = $matriculeFiscale;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getRegistreCommerce(): ?string
    {
        return $this->registreCommerce;
    }

    /**
     * @param null|string $registreCommerce
     * @return SocieteSearch
     */
    public function setRegistreCommerce(?string $registreCommerce): SocieteSearch
    {
        $this->registreCommerce = $registreCommerce;
        return $this;
    }

}
