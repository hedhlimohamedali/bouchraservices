<?php

namespace App\Entity\RessourceHumaine;

use App\Entity\Referentiel\Gouvernorat;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @Vich\Uploadable
 * @ORM\Entity
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_prenom", type="string", length=30)
     */
    private $nomPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="cin", type="string", length=8)
     */
    private $cin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_cin", type="date")
     */
    private $dateCin;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=20)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="gsm", type="string", length=20,nullable=true)
     */
    private $gsm;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=20,nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=20,nullable=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Gouvernorat")
     * @ORM\JoinColumn(nullable=false)
     */
    private $gouvernorat;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=50,nullable=true)
     */
    private $adresse;

    /**
     * @var integer
     *
     * @ORM\Column(name="avis", type="integer",nullable=true)
     */
    private $avis;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text",nullable=true)
     */
    private $remarque;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $pieceJoint;

    /** @var boolean|null */
    private $pieceJointRemove;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="client", fileNameProperty="pieceJoint")
     */
    private $pieceJointFile;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPrenom(): ?string
    {
        return $this->nomPrenom;
    }

    public function setNomPrenom(string $nomPrenom): self
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    public function getCin(): ?string
    {
        return $this->cin;
    }

    public function setCin(string $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function getDateCin(): ?\DateTimeInterface
    {
        return $this->dateCin;
    }

    public function setDateCin(\DateTimeInterface $dateCin): self
    {
        $this->dateCin = $dateCin;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getGsm(): ?string
    {
        return $this->gsm;
    }

    public function setGsm(string $gsm): self
    {
        $this->gsm = $gsm;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getAvis(): ?int
    {
        return $this->avis;
    }

    public function setAvis(int $avis): self
    {
        $this->avis = $avis;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getPieceJoint(): ?string
    {
        return $this->pieceJoint;
    }

    public function setPieceJoint(?string $pieceJoint): self
    {
        $this->pieceJoint = $pieceJoint;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getGouvernorat(): ?Gouvernorat
    {
        return $this->gouvernorat;
    }

    public function setGouvernorat(?Gouvernorat $gouvernorat): self
    {
        $this->gouvernorat = $gouvernorat;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPieceJointRemove(): ?bool
    {
        return $this->pieceJointRemove;
    }

    /**
     * @param bool|null $pieceJointRemove
     * @return Client
     */
    public function setPieceJointRemove(?bool $pieceJointRemove): Client
    {
        $this->pieceJointRemove = $pieceJointRemove;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getPieceJointFile(): ?File
    {
        return $this->pieceJointFile;
    }

    /**
     * @param File|null $pieceJointFile
     * @return Client
     */
    public function setPieceJointFile(?File $pieceJointFile): Client
    {
        $this->pieceJointFile = $pieceJointFile;

        if ($this->pieceJointFile instanceof UploadedFile)
            $this->updated = new \DateTime('now');

        return $this;
    }

    public function __toString()
    {
        return $this->nomPrenom;
    }


}
