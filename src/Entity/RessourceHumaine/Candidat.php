<?php

namespace App\Entity\RessourceHumaine;

use App\Entity\Referentiel\Archive;
use App\Entity\Referentiel\Gouvernorat;
use App\Entity\Referentiel\ModeService;
use App\Entity\Referentiel\Repos;
use App\Entity\Referentiel\TypeService;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Candidat
 *
 * @ORM\Table(name="candidat")
 * @Vich\Uploadable
 * @ORM\Entity
 */
class Candidat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $nomPrenom;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=5,nullable=true)
     */
    private $civilite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Gouvernorat")
     * @ORM\JoinColumn(nullable=false)
     */
    private $gouvernorat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $lieuNaissance;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $numIdentite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dateIdentite;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     */
    private $tel1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20,nullable=true)
     */
    private $tel2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20,nullable=true)
     */
    private $tel3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\ModeService")
     * @ORM\JoinColumn(nullable=false)
     */
    private $modeService;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\TypeService")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeService;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Repos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $repos;

    /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $cariere;

    /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $remarque;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     */
    private $numFiche;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Archive")
     * @ORM\JoinColumn(nullable=false)
     */
    private $archive;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4)
     */
    private $anneeInscrire;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $etoile;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $imgIdentiteRecto;

    /** @var boolean|null */
    private $imgIdentiteRectoRemove;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="candidat", fileNameProperty="imgIdentiteRecto")
     */
    private $imgIdentiteRectoFile;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $imgIdentiteVerso;

    /** @var boolean|null */
    private $imgIdentiteVersoRemove;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="candidat", fileNameProperty="imgIdentiteVerso")
     */
    private $imgIdentiteVersoFile;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPrenom(): ?string
    {
        return $this->nomPrenom;
    }

    public function setNomPrenom(string $nomPrenom): self
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getLieuNaissance(): ?string
    {
        return $this->lieuNaissance;
    }

    public function setLieuNaissance(string $lieuNaissance): self
    {
        $this->lieuNaissance = $lieuNaissance;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getNumIdentite(): ?string
    {
        return $this->numIdentite;
    }

    public function setNumIdentite(string $numIdentite): self
    {
        $this->numIdentite = $numIdentite;

        return $this;
    }

    public function getDateIdentite(): ?\DateTimeInterface
    {
        return $this->dateIdentite;
    }

    public function setDateIdentite(\DateTimeInterface $dateIdentite): self
    {
        $this->dateIdentite = $dateIdentite;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getTel3(): ?string
    {
        return $this->tel3;
    }

    public function setTel3(string $tel3): self
    {
        $this->tel3 = $tel3;

        return $this;
    }

    public function getCariere(): ?string
    {
        return $this->cariere;
    }

    public function setCariere(string $cariere): self
    {
        $this->cariere = $cariere;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getNumFiche(): ?string
    {
        return $this->numFiche;
    }

    public function setNumFiche(string $numFiche): self
    {
        $this->numFiche = $numFiche;

        return $this;
    }

    public function getAnneeInscrire(): ?string
    {
        return $this->anneeInscrire;
    }

    public function setAnneeInscrire(string $anneeInscrire): self
    {
        $this->anneeInscrire = $anneeInscrire;

        return $this;
    }

    public function getEtoile(): ?int
    {
        return $this->etoile;
    }

    public function setEtoile(int $etoile): self
    {
        $this->etoile = $etoile;

        return $this;
    }

    public function getImgIdentiteRecto(): ?string
    {
        return $this->imgIdentiteRecto;
    }

    public function setImgIdentiteRecto(?string $imgIdentiteRecto): self
    {
        $this->imgIdentiteRecto = $imgIdentiteRecto;

        return $this;
    }

    public function getImgIdentiteVerso(): ?string
    {
        return $this->imgIdentiteVerso;
    }

    public function setImgIdentiteVerso(?string $imgIdentiteVerso): self
    {
        $this->imgIdentiteVerso = $imgIdentiteVerso;

        return $this;
    }

    public function getGouvernorat(): ?Gouvernorat
    {
        return $this->gouvernorat;
    }

    public function setGouvernorat(?Gouvernorat $gouvernorat): self
    {
        $this->gouvernorat = $gouvernorat;

        return $this;
    }

    public function getModeService(): ?ModeService
    {
        return $this->modeService;
    }

    public function setModeService(?ModeService $modeService): self
    {
        $this->modeService = $modeService;

        return $this;
    }

    public function getTypeService(): ?TypeService
    {
        return $this->typeService;
    }

    public function setTypeService(?TypeService $typeService): self
    {
        $this->typeService = $typeService;

        return $this;
    }

    public function getRepos(): ?Repos
    {
        return $this->repos;
    }

    public function setRepos(?Repos $repos): self
    {
        $this->repos = $repos;

        return $this;
    }

    public function getArchive(): ?Archive
    {
        return $this->archive;
    }

    public function setArchive(?Archive $archive): self
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getImgIdentiteRectoRemove(): ?bool
    {
        return $this->imgIdentiteRectoRemove;
    }

    /**
     * @param bool|null $imgIdentiteRectoRemove
     * @return Candidat
     */
    public function setImgIdentiteRectoRemove(?bool $imgIdentiteRectoRemove): Candidat
    {
        $this->imgIdentiteRectoRemove = $imgIdentiteRectoRemove;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImgIdentiteRectoFile(): ?File
    {
        return $this->imgIdentiteRectoFile;
    }

    /**
     * @param File|null $imgIdentiteRectoFile
     * @return Candidat
     */
    public function setImgIdentiteRectoFile(?File $imgIdentiteRectoFile): Candidat
    {
        $this->imgIdentiteRectoFile = $imgIdentiteRectoFile;

        if ($this->imgIdentiteRectoFile instanceof UploadedFile)
            $this->updated = new \DateTime('now');

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getImgIdentiteVersoRemove(): ?bool
    {
        return $this->imgIdentiteVersoRemove;
    }

    /**
     * @param bool|null $imgIdentiteVersoRemove
     * @return Candidat
     */
    public function setImgIdentiteVersoRemove(?bool $imgIdentiteVersoRemove): Candidat
    {
        $this->imgIdentiteVersoRemove = $imgIdentiteVersoRemove;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImgIdentiteVersoFile(): ?File
    {
        return $this->imgIdentiteVersoFile;
    }

    /**
     * @param File|null $imgIdentiteVersoFile
     * @return Candidat
     */
    public function setImgIdentiteVersoFile(?File $imgIdentiteVersoFile): Candidat
    {
        $this->imgIdentiteVersoFile = $imgIdentiteVersoFile;

        if ($this->imgIdentiteVersoFile instanceof UploadedFile)
            $this->updated = new \DateTime('now');

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function __toString()
    {
        return $this->nomPrenom.' - '.$this->numIdentite;
    }


}
