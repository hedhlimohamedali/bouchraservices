<?php

namespace App\Entity\User;

use Doctrine\Common\Collections\ArrayCollection;

class UserSearch
{
    /** @var string|null */
    private $nom_prenom;

    /** @var string|null */
    private $username;

    /** @var string|null */
    private $email;

    /** @var bool|null */
    private $enabled;

    /**
     * UserSearch constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return null|string
     */
    public function getNomPrenom(): ?string
    {
        return $this->nom_prenom;
    }

    /**
     * @param null|string $nom_prenom
     * @return UserSearch
     */
    public function setNomPrenom(?string $nom_prenom): UserSearch
    {
        $this->nom_prenom = $nom_prenom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param null|string $username
     * @return UserSearch
     */
    public function setUsername(?string $username): UserSearch
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     * @return UserSearch
     */
    public function setEmail(?string $email): UserSearch
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return null|bool
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param null|bool $enabled
     * @return UserSearch
     */
    public function setEnabled(?bool $enabled): UserSearch
    {
        $this->enabled = $enabled;
        return $this;
    }

}
