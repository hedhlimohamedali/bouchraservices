<?php

namespace App\Entity\Referentiel\Search;


class ModeServiceSearch
{
    /** @var int|null */
    private $id;

    /** @var string|null */
    private $libelle;

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ModeServiceSearch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    /**
     * @param null|string $libelle
     * @return ModeServiceSearch
     */
    public function setLibelle(?string $libelle): ModeServiceSearch
    {
        $this->libelle = $libelle;
        return $this;
    }

}
