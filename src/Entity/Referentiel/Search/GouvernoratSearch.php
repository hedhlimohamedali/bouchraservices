<?php

namespace App\Entity\Referentiel\Search;


class GouvernoratSearch
{
    /** @var int|null */
    private $id;

    /** @var string|null */
    private $libelle;

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): GouvernoratSearch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    /**
     * @param null|string $libelle
     * @return GouvernoratSearch
     */
    public function setLibelle(?string $libelle): GouvernoratSearch
    {
        $this->libelle = $libelle;
        return $this;
    }

}
