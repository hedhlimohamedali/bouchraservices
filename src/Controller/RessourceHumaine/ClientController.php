<?php

namespace App\Controller\RessourceHumaine;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Vich\UploaderBundle\Handler\UploadHandler;

use App\Entity\RessourceHumaine\Client;
use App\Entity\RessourceHumaine\Search\ClientSearch;
use App\Form\RessourceHumaine\Search\ClientSearchType;
use App\Form\RessourceHumaine\ClientType;
use App\Repository\RessourceHumaine\ClientRepository;

/**
 * @Route("/ressourcehumaine/client")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/", name="ressourcehumaine_client_index", methods={"GET"})
     */
    public function indexClient(Request $request, PaginatorInterface $paginator, ClientRepository $clientRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ClientType::class,null);
        $search = new ClientSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(ClientSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");
        if ($items)
            if ($items == 'all')
                $clients = $clientRepository->getListClient($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $clients = $clientRepository->getListClient($search, $sort, $order, $page, $limit);
            else
                $clients = $clientRepository->getListClient($search, $sort, $order, "all", "all", false, explode("-", $items));
        
        if ($multi_delete) {
            foreach ($clients as $client)
                $em->remove($client);
            $em->flush();
        }
        

        $clients = $clientRepository->getListClient($search, $sort, $order, $page, $limit,true);
        $countItem = $clients["nbr_result"];
        $clients = $clientRepository->getListClient($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("RessourceHumaine/Client/liste.html.twig", ["clients" => $clients]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("RessourceHumaine/Client/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "clients" => $clients,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="ressourcehumaine_client_new", methods={"GET","POST"})
     */
    public function newClient(Request $request, EntityManagerInterface $em): Response
    {
        $client = new Client();
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($client, "setTranslatableLocale"))
                $client->setTranslatableLocale($request->getLocale());
            $em->persist($client);
            $em->flush();

            $content = $this->renderView("RessourceHumaine/Client/item.html.twig", ["client" => $client]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/view", name="ressourcehumaine_client_view", methods={"GET","POST"})
     */
    public function viewClient(Request $request, Client $client, EntityManagerInterface $em, UploadHandler $handler): Response
    {
        $content = $this->renderView("RessourceHumaine/Client/view.html.twig", ["client"=>$client]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/edit", name="ressourcehumaine_client_edit", methods={"GET","POST"})
     */
    public function editClient(Request $request, Client $client, EntityManagerInterface $em, UploadHandler $handler): Response
    {
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($client->getPieceJointRemove())
                $handler->remove($client, "pieceJointFile");

            if (method_exists($client, "setTranslatableLocale"))
                $client->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("RessourceHumaine/Client/item.html.twig", ["client" => $client]);
            return new Response($content);
        }

        $content = $this->renderView("RessourceHumaine/Client/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("ressourcehumaine_client_edit", [
                "id" => $client->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="ressourcehumaine_client_delete")
     */
    public function deleteClient(Client $client, EntityManagerInterface $em): Response
    {
        $em->remove($client);
        $em->flush();

        return new Response(1);
    }
}
