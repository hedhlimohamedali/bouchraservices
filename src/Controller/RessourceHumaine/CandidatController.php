<?php

namespace App\Controller\RessourceHumaine;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Vich\UploaderBundle\Handler\UploadHandler;

use App\Entity\RessourceHumaine\Candidat;
use App\Entity\RessourceHumaine\Search\CandidatSearch;
use App\Form\RessourceHumaine\Search\CandidatSearchType;
use App\Form\RessourceHumaine\CandidatType;
use App\Repository\RessourceHumaine\CandidatRepository;

/**
 * @Route("/ressourcehumaine/candidat")
 */
class CandidatController extends AbstractController
{
    /**
     * @Route("/", name="ressourcehumaine_candidat_index", methods={"GET"})
     */
    public function indexCandidat(Request $request, PaginatorInterface $paginator, CandidatRepository $candidatRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CandidatType::class,null);
        $search = new CandidatSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(CandidatSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");
        if ($items)
            if ($items == 'all')
                $candidats = $candidatRepository->getListCandidat($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $candidats = $candidatRepository->getListCandidat($search, $sort, $order, $page, $limit);
            else
                $candidats = $candidatRepository->getListCandidat($search, $sort, $order, "all", "all", false, explode("-", $items));
        
        if ($multi_delete) {
            foreach ($candidats as $candidat)
                $em->remove($candidat);
            $em->flush();
        }
        

        $candidats = $candidatRepository->getListCandidat($search, $sort, $order, $page, $limit,true);
        $countItem = $candidats["nbr_result"];
        $candidats = $candidatRepository->getListCandidat($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("RessourceHumaine/Candidat/liste.html.twig", ["candidats" => $candidats]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("RessourceHumaine/Candidat/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "candidats" => $candidats,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="ressourcehumaine_candidat_new", methods={"GET","POST"})
     */
    public function newCandidat(Request $request, EntityManagerInterface $em): Response
    {
        $candidat = new Candidat();
        $form = $this->createForm(CandidatType::class, $candidat);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($candidat, "setTranslatableLocale"))
                $candidat->setTranslatableLocale($request->getLocale());
            $em->persist($candidat);
            $em->flush();

            $content = $this->renderView("RessourceHumaine/Candidat/item.html.twig", ["candidat" => $candidat]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/view", name="ressourcehumaine_candidat_view", methods={"GET","POST"})
     */
    public function viewCandidat(Request $request, Candidat $candidat, EntityManagerInterface $em, UploadHandler $handler): Response
    {
        $content = $this->renderView("RessourceHumaine/Candidat/view.html.twig", ["candidat"=>$candidat]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/edit", name="ressourcehumaine_candidat_edit", methods={"GET","POST"})
     */
    public function editCandidat(Request $request, Candidat $candidat, EntityManagerInterface $em, UploadHandler $handler): Response
    {
        $form = $this->createForm(CandidatType::class, $candidat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($candidat->getImgIdentiteRectoRemove())
                $handler->remove($candidat, "imgIdentiteRectoFile");
            if ($candidat->getImgIdentiteVersoRemove())
                $handler->remove($candidat, "imgIdentiteVersoFile");

            if (method_exists($candidat, "setTranslatableLocale"))
                $candidat->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("RessourceHumaine/Candidat/item.html.twig", ["candidat" => $candidat]);
            return new Response($content);
        }

        $content = $this->renderView("RessourceHumaine/Candidat/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("ressourcehumaine_candidat_edit", [
                "id" => $candidat->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="ressourcehumaine_candidat_delete")
     */
    public function deleteCandidat(Candidat $candidat, EntityManagerInterface $em): Response
    {
        $em->remove($candidat);
        $em->flush();

        return new Response(1);
    }
}
