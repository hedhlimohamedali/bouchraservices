<?php

namespace App\Controller\RessourceHumaine;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Vich\UploaderBundle\Handler\UploadHandler;

use App\Entity\RessourceHumaine\Societe;
use App\Entity\RessourceHumaine\Search\SocieteSearch;
use App\Form\RessourceHumaine\Search\SocieteSearchType;
use App\Form\RessourceHumaine\SocieteType;
use App\Repository\RessourceHumaine\SocieteRepository;

/**
 * @Route("/ressourcehumaine/societe")
 */
class SocieteController extends AbstractController
{
    /**
     * @Route("/", name="ressourcehumaine_societe_index", methods={"GET"})
     */
    public function indexSociete(Request $request, PaginatorInterface $paginator, SocieteRepository $societeRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(SocieteType::class,null);
        $search = new SocieteSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(SocieteSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");
        if ($items)
            if ($items == 'all')
                $societes = $societeRepository->getListSociete($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $societes = $societeRepository->getListSociete($search, $sort, $order, $page, $limit);
            else
                $societes = $societeRepository->getListSociete($search, $sort, $order, "all", "all", false, explode("-", $items));
        
        if ($multi_delete) {
            foreach ($societes as $societe)
                $em->remove($societe);
            $em->flush();
        }
        
        $export_xsl = $request->query->getInt("export-xsl", 0);
        if ($export_xsl) {
            $data = [
            "Nom",
            "Code",
            "Activité",
            "Gérant",
            "Tél",
            "GSM",
            "Fax",
            "Email",
            "Gouvernorat",
            "Adresse",
            "Avis",
            "RIB",
            "Matricule Fiscale",
            "Registre Commerce",
            "Remarque",
            "Pièce Joint",
            ];
            /** @var Societe $societe */
            foreach ($societes as $societe) {
                $gouvernorat = "";
                if ($societe->getGouvernorat())
                    $gouvernorat = $societe->getGouvernorat()->__toString();

                    $data[] = [
                     $societe->getNom(),
                     $societe->getTel(),
                     $societe->getGsm(),
                     $societe->getFax(),
                     $societe->getEmail(),
                     $gouvernorat,
                ];
            }
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->fromArray($data, NULL, "A1");

            for ($i = "A"; $i !=  $sheet->getHighestColumn(); $i++)
                $sheet->getColumnDimension($i)->setAutoSize(TRUE);

            $sheet->getStyle("A1:Z1")->getFont()->setBold(true);

            $writer = new Xlsx($spreadsheet);
            $writer->save("export-pdf/Societes.xlsx");
            return new Response("/export-pdf/Societes.xlsx");
        }

        $societes = $societeRepository->getListSociete($search, $sort, $order, $page, $limit,true);
        $countItem = $societes["nbr_result"];
        $societes = $societeRepository->getListSociete($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("RessourceHumaine/Societe/liste.html.twig", ["societes" => $societes]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("RessourceHumaine/Societe/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "societes" => $societes,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="ressourcehumaine_societe_new", methods={"GET","POST"})
     */
    public function newSociete(Request $request, EntityManagerInterface $em): Response
    {
        $societe = new Societe();
        $form = $this->createForm(SocieteType::class, $societe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($societe, "setTranslatableLocale"))
                $societe->setTranslatableLocale($request->getLocale());
            $em->persist($societe);
            $em->flush();

            $content = $this->renderView("RessourceHumaine/Societe/item.html.twig", ["societe" => $societe]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/view", name="ressourcehumaine_societe_view", methods={"GET","POST"})
     */
    public function viewSociete(Request $request, Societe $societe, EntityManagerInterface $em, UploadHandler $handler): Response
    {
        $content = $this->renderView("RessourceHumaine/Societe/view.html.twig", ["societe"=>$societe]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/edit", name="ressourcehumaine_societe_edit", methods={"GET","POST"})
     */
    public function editSociete(Request $request, Societe $societe, EntityManagerInterface $em, UploadHandler $handler): Response
    {
        $form = $this->createForm(SocieteType::class, $societe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($societe->getPieceJointRemove())
                $handler->remove($societe, "pieceJointFile");

            if (method_exists($societe, "setTranslatableLocale"))
                $societe->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("RessourceHumaine/Societe/item.html.twig", ["societe" => $societe]);
            return new Response($content);
        }

        $content = $this->renderView("RessourceHumaine/Societe/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("ressourcehumaine_societe_edit", [
                "id" => $societe->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="ressourcehumaine_societe_delete")
     */
    public function deleteSociete(Societe $societe, EntityManagerInterface $em): Response
    {
        $em->remove($societe);
        $em->flush();

        return new Response(1);
    }
}
