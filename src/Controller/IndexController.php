<?php

namespace App\Controller;

use App\Service\Generale\Generale;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('Generale/index.html.twig');
    }

    /**
     * @Route("view-pdf/{file}", name="view_pdf")
     */
    public function viewPdf($file)
    {
        if (!file_exists("EtatWord/pdf/$file.pdf")) {
            $err = [];
            if (!is_writable(realpath('EtatWord')))
                $err[] = "Veuillez vérifier le droit d'accès du dossier EtatWord";
            if (is_null(shell_exec("libreoffice --version")))
                $err[] = "Veuillez vérifier l'installation de libreoffice";

            if (!empty($err)) {
                $this->addFlash('danger', implode('<br>', $err));
                return $this->render('/');
            }
        }
        
        $html = '<style>body{margin: 0px;}</style>
                <object type="application/pdf" style="width:100%;height:100vh">
                    <param name="src" value="{{ path }}" />
                    <p>Il semble que votre navigateur Web n\'est pas configuré pour afficher les fichiers PDF.
                     Pas de soucis, juste <a href="{{ path }}">cliquez ici pour télécharger le fichier PDF.</a>
                    </p>
                </object>';
        $loader = new \Twig\Loader\ArrayLoader([
            'viewPDF.html.twig' => $html,
        ]);
        $twig = new \Twig\Environment($loader);
        return new Response($twig->render('viewPDF.html.twig', ['path' => "/EtatWord/pdf/$file.pdf"]));
    }
    
    /**
     * @Route("/change_locale/{locale}", name="change_locale")
     */
    public
    function changeLocale($locale, Request $request)
    {
        $request->getSession()->set('_locale', $locale);
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/traduction", name="traduction")
     */
    public function traduction(Request $request)
    {
        if ($request->isMethod('POST'))
            file_put_contents($this->getParameter('kernel.project_dir') . '/translations/messages+intl-icu.ar.yaml', Yaml::dump($request->get('transAR')));
        $translations = Yaml::parse(file_get_contents($this->getParameter('kernel.project_dir') . '/translations/messages+intl-icu.ar.yaml'));
        return $this->render('traduction.html.twig', array('translations' => $translations));
    }
    
    /**
     * @Route("/get-fields-file-xsl", name="get_fields_file_xsl")
     */
    public function getFiledsFileXSL(Request $request, Generale $generale)
    {
        $entityName = $request->get('entity-name');
        $name = $_FILES['file-xsl']['name'];
        $file = $request->files->get('file-xsl');

        return $generale->getFiledsFileXSL($file, $entityName, $name);
    }

    /**
     * @Route("/import-data", name="import_data")
     */
    public function importData(Generale $generale)
    {
        if (!isset($_POST['cols-sheet']))
            return $this->redirect($_SERVER['HTTP_REFERER']);

        $entityName = $_POST['entity-name'];
        $colsSheet = $_POST['cols-sheet'];
        $file_path = $_POST['file-path'];

        $result = $generale->importData($entityName, $colsSheet, $file_path);

        if (is_array($result)) {
            $this->addFlash('danger', '<ul><li>' . implode('</li><li>', array_slice($result, 0, 10)) . '</li></ul>');
            return $this->redirect($_SERVER['HTTP_REFERER']);
        }
        $this->addFlash('success', 'Importation terminé avec succès');
        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * @Route("/git-pull", name="git_pull")
     */
    public function gitPull( TranslatorInterface $translator)
    {
        try {
            $cd = "cd " . $this->getParameter('kernel.project_dir');
            $reset = "git reset --hard";
            $pull = "git pull origin master";
            $db = "php bin\console doctrine:schema:update --force";
            $cache = "php bin\console cache:clear --env=prod";
            exec("$cd&&$reset&&$pull&&$db&&$cache", $output, $worked);

            if (isset($output[1]) and $output[1] == "Already up to date.")
                $this->addFlash('warning', $translator->trans("Pas des mise à jour pour le moment"));
            elseif($worked==0)
                $this->addFlash('success', "Votre mise a jour a été terminé avec succès<br><b>Traces :</b><ul><li>".implode('</li><li>',$output).'</li></ul>');
            else
                $this->addFlash('danger', $translator->trans("Une erreur est survenue lors de la mise à jour, veuillez contactez le support technique !.<br><b>Traces :</b><ul><li>".implode('</li><li>',$output).'</li></ul>'));

        } catch (\Exception $exception) {
            $this->addFlash('danger', $translator->trans("Une erreur est produite lors de la mise a jour"));
        }

        return $this->redirect('/');
    }

    function getVerPHP($base)
    {
        if ($dir = opendir($base))
            while ($entry = readdir($dir))
                if (is_dir($base . "/" . $entry) && !in_array($entry, array('.', '..')) and strpos($entry, "php7.") !== false)
                    return $entry;
    }
}
