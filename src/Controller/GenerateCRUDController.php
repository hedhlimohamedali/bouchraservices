<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GenerateCRUDController
 * @package App\Controller
 */
class GenerateCRUDController extends AbstractController
{
    /**
     * @param Request $request
     * @Route("/generate-crud", name="generate_crud")
     */
    public function generateCRUD(Request $request, EntityManagerInterface $em)
    {
        $_classNames = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        $classNames = [];
        foreach ($_classNames as $className)
            if (strpos($className, 'App\Entity') !== false)
                $classNames[] = $className;

        function nameGetterSetter($name)
        {
            $res = "";
            foreach (explode("_", $name) as $item)
                $res .= ucwords($item);
            return $res;
        }

        if ($request->get('className')) {
            $className = $request->get('className');
            $run = $request->isMethod('post');
            $translatable = false;
            $useArrayCollection = false;
            $uploadableField = false;
            $countItemInList = 0;
            $uses = [];
            $uses_search = [];
            $classes = [];
            $statiqueData = [];
            $_fieldMappings = $em->getClassMetadata($className);
            $name = substr($_fieldMappings->getName(), strrpos($_fieldMappings->getName(), '\\') + 1);
            $tableName = $_fieldMappings->getTableName();
            $namespace = $_fieldMappings->namespace;

            $logFieldMappings = [];
            if (file_exists("log-generat-crud/$tableName.php"))
                $logFieldMappings = include "log-generat-crud/$tableName.php";

            $jsonIni = [
                "X" => "",
                "fieldName" => "x",
                "label" => "",
                "list" => "",
                "search" => "",
                "searchInter" => "",
                "statiqueData" => "",
                "type" => "x",
                "translatable" => false,
                "scale" => "x",
                "precision" => "x",
                "relation" => "x",
                "namespace" => "",
                "class" => "",
                "nullable" => false,
                "_type" => "x",
                "required" => true
            ];

            $fieldTraiter = ['id', 'locale'];
            foreach ($_fieldMappings->fieldMappings as $fieldMapping) {
                $fieldName = $fieldMapping['fieldName'];
                if (!in_array($fieldName, $fieldTraiter)) {
                    $fieldTraiter[] = $fieldName;
                    $_json = $jsonIni;

                    foreach ($fieldMapping as $key => $mapping)
                        if (!in_array($key, ['options', 'columnName', 'declared', 'length', 'unique']))
                            $_json[$key] = $mapping;

                    if ($run) {
                        $_json['label'] = $request->get('label')[$fieldName];
                        $_json['_type'] = $_json['type'];

                        if ($_json['type'] == "decimal")
                            $_json['_type'] = "float";
                        if ($_json['type'] == "integer")
                            $_json['_type'] = "int";
                        if ($_json['type'] == "boolean")
                            $_json['_type'] = "bool";
                        if (in_array($_json['type'], ['date', 'datetime']))
                            $_json['_type'] = "\DateTime";

                        $_json['getter/setter'] = nameGetterSetter($fieldName);
                        $_json['getter'] = lcfirst($_json['getter/setter']);
                        $_json['list'] = isset($request->get('list')[$fieldName]);
                        if ($_json['list'])
                            $countItemInList++;
                        $_json['X'] = isset($request->get('X')[$fieldName]);
                        if (isset($request->get('col-md')[$fieldName]))
                            $_json['col-md'] = $request->get('col-md')[$fieldName];
                        $_json['search'] = isset($request->get('search')[$fieldName]);

                        if ($_json['search'] and isset($request->get('col-md-search')[$fieldName]))
                            $_json['col-md-search'] = $request->get('col-md-search')[$fieldName];

                        $_json['searchInter'] = isset($request->get('searchInter')[$fieldName]);
                        if ($request->get('statiqueData'))
                            $_json['statiqueData'] = $request->get('statiqueData')[$fieldName];
                        if ($_json['statiqueData'] != "")
                            $statiqueData[] = $_json['statiqueData'];

                        /*if ($_json['X'])*/
                        $fieldMappings[$fieldName] = $_json;
                    } else {
                        $lbl_fieldName = $fieldName;
                        $checked = 'checked';
                        $checked_list = 'checked';
                        $checked_search = 'checked';
                        if (isset($logFieldMappings[$fieldName])) {
                            $lbl_fieldName = $logFieldMappings[$fieldName]['label'];
                            $checked = $logFieldMappings[$fieldName]['X'] ? 'checked' : '';
                            $checked_list = $logFieldMappings[$fieldName]['list'] ? 'checked' : '';
                            $checked_search = $logFieldMappings[$fieldName]['search'] ? 'checked' : '';
                        }

                        $_json['label'] = "<input type='text' placeholder='$fieldName' class='form-control' name='label[$fieldName]' value='$lbl_fieldName'>";
                        $_json['X'] = "<input type='checkbox' class='X' id='X-$fieldName' data-field='$fieldName' $checked name='X[$fieldName]'>";
                        $_json['list'] = "<input type='checkbox' $checked_list name='list[$fieldName]'>";
                        $_json['search'] = "<input type='checkbox' $checked_search id='search-$fieldName' name='search[$fieldName]'>";
                        $_json['searchInter'] = "";
                        $_json['statiqueData'] = "<input type='text' class='form-control' name='statiqueData[$fieldName]'>";
                        $fieldMappings[$fieldName] = $_json;
                    }

                }
            }


            foreach ($_fieldMappings->reflClass->getProperties() as $property) {
                $_json = $jsonIni;
                $fieldName = $property->getName();
                $_json['fieldName'] = $fieldName;

                if (isset($fieldMappings[$fieldName]) and strpos($property->getDocComment(), 'name="') !== false) {
                    $pos1 = strpos($property->getDocComment(), 'name="') + 6;
                    $pos2 = strpos($property->getDocComment(), '"', $pos1);
                    $_fieldName = substr($property->getDocComment(), $pos1, $pos2 - $pos1);
                    if ($fieldMappings[$fieldName]['fieldName'] != $_fieldName)
                        $fieldMappings[$fieldName]['_fieldName'] = $_fieldName;
                }

                if (isset($fieldMappings[$fieldName]) and strpos($property->getDocComment(), 'nullable=true') !== false) {
                    $fieldMappings[$fieldName]['required'] = false;
                }

                if (strpos($property->getDocComment(), "@Vich\UploadableField") !== false) {
                    $uploadableField = true;
                    $fieldMappings[str_replace('File', '', $fieldName)]['type'] = "file";
                }

                if (strpos($property->getDocComment(), "@Gedmo\Translatable") !== false) {
                    $translatable = true;
                    $fieldMappings[$fieldName]['translatable'] = true;
                }

                if (strpos($property->getDocComment(), "@ORM\ManyToOne") !== false)
                    $_json['relation'] = "ManyToOne";

                if (strpos($property->getDocComment(), "@ORM\ManyToMany") !== false) {
                    $_json['relation'] = "ManyToMany";
                    $_json['_type'] = "ArrayCollection";
                    $useArrayCollection = true;
                }

                if (strpos($property->getDocComment(), "@ORM\OneToOne") !== false) {
                    $_json['relation'] = "ManyToOne";
                    //$_json['relation'] = "OneToOne";
                }

                $mappedBy = strpos($property->getDocComment(), "mappedBy") !== false;

                if ($_json['relation'] != "x" and !$mappedBy) {
                    $classes[] = "select";
                    $pos1 = strpos($property->getDocComment(), 'targetEntity="') + 14;
                    $pos2 = strpos($property->getDocComment(), '"', $pos1);
                    $_namespace = substr($property->getDocComment(), $pos1, $pos2 - $pos1);
                    $_json['namespace'] = $_namespace;
                    $_class = explode("\\", $_namespace);
                    $_json['class'] = end($_class);

                    if ($_json['relation'] == "ManyToOne")
                        $_json['_type'] = $_json['class'];

                    if ($run) {
                        $_json['getter/setter'] = nameGetterSetter($fieldName);
                        $_json['getter'] = lcfirst($_json['getter/setter']);
                        $_json['label'] = $request->get('label')[$fieldName];
                        $_json['list'] = isset($request->get('list')[$fieldName]);
                        if ($_json['list'])
                            $countItemInList++;
                        $_json['X'] = isset($request->get('X')[$fieldName]);
                        $_json['col-md'] = $request->get('col-md')[$fieldName];
                        $_json['search'] = isset($request->get('search')[$fieldName]);

                        if ($_json['search'] and isset($request->get('col-md-search')[$fieldName]))
                            $_json['col-md-search'] = $request->get('col-md-search')[$fieldName];

                        $_json['searchInter'] = isset($request->get('searchInter')[$fieldName]);
                        if ($request->get('statiqueData'))
                            $_json['statiqueData'] = $request->get('statiqueData')[$fieldName];
                        if ($_json['statiqueData'] != "")
                            $statiqueData[] = $_json['statiqueData'];

                        /*if ($_json['X'])*/
                        $fieldMappings[$fieldName] = $_json;
                    } else {
                        $lbl_fieldName = $fieldName;
                        $checked = 'checked';
                        $checked_list = 'checked';
                        $checked_search = 'checked';
                        if (isset($logFieldMappings[$fieldName])) {
                            $lbl_fieldName = $logFieldMappings[$fieldName]['label'];
                            $checked = $logFieldMappings[$fieldName]['X'] ? 'checked' : '';
                            $checked_list = $logFieldMappings[$fieldName]['list'] ? 'checked' : '';
                            $checked_search = $logFieldMappings[$fieldName]['search'] ? 'checked' : '';
                        }

                        $_json['label'] = "<input type='text' placeholder='$fieldName' class='form-control' name='label[$fieldName]' value='$lbl_fieldName'>";
                        $_json['list'] = "<input type='checkbox' $checked_list name='list[$fieldName]'>";
                        $_json['X'] = "<input type='checkbox' class='X' id='X-$fieldName' data-field='$fieldName' $checked name='X[$fieldName]'>";
                        $_json['search'] = "<input type='checkbox' id='search-$fieldName' $checked_search name='search[$fieldName]'>";
                        $_json['searchInter'] = "";
                        $_json['statiqueData'] = "<input type='text' class='form-control' name='statiqueData[$fieldName]'>";
                        $fieldMappings[$fieldName] = $_json;
                    }
                }
            }

            if (!$run)
                foreach ($fieldMappings as $key => $item) {
                    if (!in_array($item['type'], ['string', 'integer']))
                        $fieldMappings[$key]['statiqueData'] = "<input type='hidden' name='statiqueData[" . $item['fieldName'] . "]'>";
                    if (in_array($item['type'], ['text']))
                        $fieldMappings[$key]['list'] = "";
                    if (in_array($item['type'], ['file', 'text']))
                        $fieldMappings[$key]['search'] = "";

                    $checked_search_interval = 'checked';
                    if (isset($logFieldMappings[$key]))
                        $checked_search_interval = $logFieldMappings[$key]['searchInter'] ? 'checked' : '';

                    if (in_array($item['type'], ['datetime', 'date', 'integer', 'decimal']))
                        $fieldMappings[$key]['searchInter'] = "<input type='checkbox' id='searchInter-" . $item['fieldName'] . "' $checked_search_interval name='searchInter[" . $item['fieldName'] . "]'>";
                }

            foreach ($fieldMappings as $fieldMapping) {
                if ($fieldMapping['relation'] != "x" and $fieldMapping['search']) {
                    $uses_search[] = "use " . $fieldMapping['namespace'] . ";";
                    $use = "use Symfony\Bridge\Doctrine\Form\Type\EntityType;";
                    if (!in_array($use, $uses_search))
                        $uses_search[] = $use;
                }

                $use = "";
                switch ($fieldMapping['type']) {
                    case 'text':
                        $use = "use FOS\CKEditorBundle\Form\Type\CKEditorType;";
                        break;
                    case 'boolean':
                        $use = "use Symfony\Component\Form\Extension\Core\Type\CheckboxType;";
                        $classes[] = "styled";
                        break;
                    case 'decimale':

                        if ($fieldMapping['scale'] == 2)
                            $classes[] = "percent";
                        if ($fieldMapping['scale'] == 3)
                            $classes[] = "money";
                        break;
                    case 'datetime':
                        $use = "use Symfony\Component\Form\Extension\Core\Type\DateTimeType;";
                        break;
                    case 'date':
                        $use = "use Symfony\Component\Form\Extension\Core\Type\DateType;";
                        break;
                    case 'time':
                        $use = "use Symfony\Component\Form\Extension\Core\Type\TimeType;";
                        break;
                    case 'integer':
                        $use = "use Symfony\Component\Form\Extension\Core\Type\IntegerType;";
                        break;
                    case 'file':
                        $use = "use Symfony\Component\Form\Extension\Core\Type\FileType;";
                        $classes[] = "input-file";
                        break;
                }

                $uses[] = $use;
                if ($fieldMapping['search'])
                    $uses_search[] = $use;

                if ($fieldMapping['type'] == 'file')
                    $uses[] = "use Symfony\Component\Form\Extension\Core\Type\CheckboxType;";

                $use = "use Symfony\Component\Form\Extension\Core\Type\ChoiceType;";
                if ($fieldMapping['statiqueData'] != "") {
                    $uses[] = $use;
                    $classes[] = "select";
                    if ($fieldMapping['search'])
                        $uses_search[] = $use;
                }
            }

            if ($run) {
                $folder = str_replace("App\Entity\\", "", $namespace);
                $copyOld = $request->get('copy-old');
                $searching = !is_null($request->get('search'));
                $exportExcel = $request->get('export-excel');
                $knpPaginator = $request->get('knp_paginator');
                $copyTest = $request->get('copy-test');
                $uses_search = array_unique($uses_search);
                $uses = array_unique($uses);

                $ordre = 1;
                foreach ($request->get('col-md') as $key => $item) {
                    if (isset($fieldMappings[$key])) {
                        $fieldMappings[$key]['ordre'] = $ordre;
                        $fieldMappings[$key]['ordre-search'] = $ordre;
                        $ordre++;
                    }
                }

                if ($request->get('col-md-search')) {
                    $ordre = 1;
                    foreach ($request->get('col-md-search') as $key => $item) {
                        if (isset($fieldMappings[$key])) {
                            $fieldMappings[$key]['ordre-search'] = $ordre;
                            $ordre++;
                        }
                    }
                }

                uasort($fieldMappings, function ($a, $b) {
                    return isset($a['ordre']) and isset($b['ordre']) and $a['ordre'] > $b['ordre'];
                });

                $paths = [];
                function verifGeneration($result, &$paths)
                {
                    $paths[] = $result['path'];
                    if (isset($result['erreur'])) {
                        foreach ($paths as $path)
                            unlink($path);
                        dd($result['erreur']);
                    }
                }

                $route = "index";
                $path = "/";

                if ($request->get('function-getFields')) {
                    $foreign_fields = [];
                    $fields = [];

                    foreach ($fieldMappings as $fieldMapping) {
                        $_fieldName = isset($fieldMapping['_fieldName']) ? $fieldMapping['_fieldName'] : $fieldMapping['fieldName'];
                        if ($fieldMapping['namespace'] == "")
                            $fields[$_fieldName] = [
                                'libelle' => $fieldMapping['label'],
                                'type' => $fieldMapping['type'],
                                'required' => isset($fieldMapping['required']) ? $fieldMapping['required'] : true
                            ];
                        else
                            $foreign_fields[$_fieldName] = [
                                'class' => $fieldMapping['namespace'],
                                'key' => '',
                                'entity_fields' => []
                            ];

                    }

                    $entity_fields = [
                        'entity_name' => $tableName,
                        'entity_fields' => $fields
                    ];
                    if (!empty($foreign_fields))
                        $entity_fields['foreign_fields'] = $foreign_fields;

                    file_put_contents('entity_fields.php', "public function getFields$name(){\n return " . var_export($entity_fields, true) . "\n;}");

                    echo '<pre>';
                    print_r(file_get_contents('entity_fields.php'));
                    echo '</pre>';
                    die;
                }
                file_put_contents("log-generat-crud/$tableName.php", '<?php return ' . var_export($fieldMappings, true) . ';');

                foreach ($fieldMappings as $key => $fieldMapping)
                    if (!$fieldMapping['X'])
                        unset($fieldMappings[$key]);

                $singleItem = $request->get('single-item');
                $viewItem = $request->get('create-view-twig');
                if ($request->get('creat-controller')) {
                    if ($singleItem)
                        $result = $this->creatControllerSingleItem($name, $folder, $fieldMappings, $copyOld, $exportExcel, $statiqueData, $uploadableField, $translatable, $searching, $knpPaginator, $copyTest);
                    else
                        $result = $this->creatController($name, $folder, $fieldMappings, $copyOld, $exportExcel, $statiqueData, $uploadableField, $translatable, $searching, $knpPaginator, $copyTest, $viewItem);
                    verifGeneration($result, $paths);
                    $route = $result['route_name'];
                    $path = $result['url'];
                }

                if ($request->get('create-form-type')) {
                    $result = $this->createFormType($name, $folder, $fieldMappings, $copyOld, $uses, $copyTest);
                    verifGeneration($result, $paths);
                }

                if ($request->get('create-repository') and !$singleItem) {
                    $result = $this->createRepository($name, $folder, $fieldMappings, $copyOld, $searching, $copyTest);
                    verifGeneration($result, $paths);
                }

                if ($searching) {
                    if ($request->get('create-form-search-type') and !$singleItem) {
                        $result = $this->createFormSearchType($name, $folder, $fieldMappings, $copyOld, $uses_search, $copyTest);
                        verifGeneration($result, $paths);
                    }

                    if ($request->get('create-entity-search') and !$singleItem) {
                        $result = $this->createEntitySearch($name, $folder, $fieldMappings, $copyOld, $useArrayCollection, $copyTest);
                        verifGeneration($result, $paths);
                    }
                }

                if ($request->get('create-item-twig') and !$singleItem) {
                    $result = $this->createItemTwig($name, $folder, $fieldMappings, $copyOld, $copyTest, $knpPaginator, $viewItem);
                    verifGeneration($result, $paths);
                    if (!$knpPaginator) {
                        $result = $this->create_ItemTwig($name, $folder, $fieldMappings, $copyOld, $copyTest);
                        verifGeneration($result, $paths);
                    }
                }

                if ($request->get('create-liste-twig') and $knpPaginator and !$singleItem) {
                    $result = $this->createListeTwig($name, $folder, $countItemInList, $copyOld, $copyTest, $knpPaginator);
                    verifGeneration($result, $paths);
                }

                if ($request->get('create-form-twig')) {
                    $result = $this->createFormTwig($name, $folder, $fieldMappings, $copyOld, array_unique($classes), $knpPaginator, $copyTest, $singleItem);
                    verifGeneration($result, $paths);
                }

                if ($viewItem) {
                    $result = $this->createViewTwig($name, $folder, $fieldMappings, $copyOld, $copyTest);
                    verifGeneration($result, $paths);
                }

                uasort($fieldMappings, function ($a, $b) {
                    return $a['ordre-search'] > $b['ordre-search'];
                });

                if ($request->get('create-index-twig')) {
                    $widthModal = $request->get('width-modal');
                    if ($singleItem)
                        $result = $this->createIndexTwigSingleItem($name, $folder, $copyOld, $copyTest);
                    else
                        $result = $this->createIndexTwig($name, $folder, $fieldMappings, $copyOld, $exportExcel, $uploadableField, $searching, $widthModal, in_array("use FOS\CKEditorBundle\Form\Type\CKEditorType;", $uses), $knpPaginator, $copyTest, $viewItem);
                    verifGeneration($result, $paths);
                }

                return new Response("The generation of crud is successfully completed - Route Index : <a href='$path' target='_blank'>$route</a>");
            }

            foreach ($fieldMappings as &$fieldMapping)
                unset($fieldMapping['translatable'], $fieldMapping['scale'], $fieldMapping['precision'], $fieldMapping['namespace'], $fieldMapping['class']);

            foreach ($fieldMappings as &$item) {
                unset($item['fieldName']);
                unset($item['statiqueData']);
            }

            $JsonResponse = [
                'namespace' => $namespace,
                'name' => $name,
                'translatable' => $translatable,
                'uploadableField' => $uploadableField,
                'uses' => $uses,
                'statiqueData' => $statiqueData,
                'fieldMappings' => $fieldMappings,
                'logFieldMappings' => $logFieldMappings
            ];
            uasort($logFieldMappings, function ($a, $b) {
                return isset($a['ordre-search']) and isset($b['ordre-search']) and $a['ordre-search'] > $b['ordre-search'];
            });
            $JsonResponse['_logFieldMappings'] = $logFieldMappings;
            return new JsonResponse($JsonResponse);
        }

        return $this->render('generate_crud.html.twig', compact(['classNames']));
    }

    function creatControllerSingleItem($entity, $folder, $fields, $copyOld, $uploadableField, $translatable, $copyTest)
    {
        try {
            $namespace = "App\Controller\\$folder";
            $filename = $entity . "Controller";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\src\Controller\\$folder";
            $path = "$dirname\\$filename.php";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            $filenameEntity = "App\Entity\\$folder\\$entity";
            $filenameForm = "App\Form\\$folder\\$entity" . "Type";

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '<?php' . "\n");
            fwrite($fichier, '' . "\n");

            fwrite($fichier, 'namespace ' . $namespace . ';' . "\n");
            fwrite($fichier, '' . "\n");

            fwrite($fichier, 'use Doctrine\ORM\EntityManagerInterface;' . "\n");
            fwrite($fichier, 'use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;' . "\n");
            fwrite($fichier, 'use Symfony\Component\HttpFoundation\Request;' . "\n");
            fwrite($fichier, 'use Symfony\Component\HttpFoundation\Response;' . "\n");
            fwrite($fichier, 'use Symfony\Component\Routing\Annotation\Route;' . "\n");

            fwrite($fichier, '' . "\n");
            if ($uploadableField)
                fwrite($fichier, 'use Vich\UploaderBundle\Handler\UploadHandler;' . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'use ' . $filenameEntity . ';' . "\n");
            fwrite($fichier, 'use ' . $filenameForm . ';' . "\n");

            $entity_lower = strtolower($entity);
            $route = strtolower(str_replace("\\", "/", $folder)) . '/' . $entity_lower;
            $route_name = str_replace('\\', '_', strtolower($folder)) . '_' . $entity_lower;

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '/**' . "\n");
            fwrite($fichier, ' * @Route("/' . $route . '")' . "\n");
            fwrite($fichier, ' */' . "\n");
            fwrite($fichier, 'class ' . $entity . 'Controller extends AbstractController' . "\n");
            fwrite($fichier, '{' . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    /**' . "\n");
            fwrite($fichier, '     * @Route("/", name="' . $route_name . '_index", methods={"GET","POST"})' . "\n");
            fwrite($fichier, '     */' . "\n");
            fwrite($fichier, '    public function index' . $entity . '(Request $request, EntityManagerInterface $em' . ($uploadableField ? ', UploadHandler $handler' : '') . '): Response' . "\n");
            fwrite($fichier, '    {' . "\n");

            fwrite($fichier, '    $' . $entity_lower . ' = $em->getRepository(' . $entity . '::class)->findOneBy([]);' . "\n");
            fwrite($fichier, '    if(is_null($' . $entity_lower . ')){' . "\n");
            fwrite($fichier, '        $' . $entity_lower . '=new ' . $entity . '();' . "\n");
            fwrite($fichier, '        $em->persist($' . $entity_lower . ');' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '        $' . strtolower($field['statiqueData']) . '=$statiqueData->get' . $field['statiqueData'] . '();' . "\n");

            fwrite($fichier, '        $form = $this->createForm(' . $entity . 'Type::class, $' . $entity_lower . ');' . "\n");
            fwrite($fichier, '        $form->handleRequest($request);' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        if ($form->isSubmitted() && $form->isValid()) {' . "\n");

            foreach ($fields as $field)
                if ($field['type'] == 'file') {
                    fwrite($fichier, '            if ($' . $entity_lower . '->get' . $field['getter/setter'] . 'Remove())' . "\n");
                    fwrite($fichier, '                $handler->remove($' . $entity_lower . ', "' . $field['fieldName'] . 'File");' . "\n");
                }

            fwrite($fichier, '' . "\n");

            fwrite($fichier, '            if (method_exists($' . $entity_lower . ', "setTranslatableLocale"))' . "\n");
            fwrite($fichier, '                $' . $entity_lower . '->setTranslatableLocale($request->getLocale());' . "\n");

            fwrite($fichier, '            $em->flush();' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '            return new Response(1);' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        return $this->render("' . $folder . '/' . $entity . '/index.html.twig", [' . "\n");
            fwrite($fichier, '            "form" => $form->createView(),' . "\n");

            fwrite($fichier, '            "' . $entity_lower . '" => $' . $entity_lower . "\n");
            fwrite($fichier, '        ]);' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '}' . "\n");
            fclose($fichier);
            return [
                'route_name' => $route_name . '_index',
                'url' => $route,
                'path' => $path
            ];
        } catch (\Exception $exception) {
            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function creatController($entity, $folder, $fields, $copyOld, $exportExcel, $statiqueData, $uploadableField, $translatable, $searching, $knpPaginator, $copyTest, $view)
    {
        try {
            $namespace = "App\Controller\\$folder";
            $filename = $entity . "Controller";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\src\Controller\\$folder";
            $path = "$dirname\\$filename.php";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            $filenameEntity = "App\Entity\\$folder\\$entity";
            $filenameEntitySearch = "App\Entity\\$folder\Search\\$entity" . "Search";
            $filenameForm = "App\Form\\$folder\\$entity" . "Type";
            $filenameFormSearch = "App\Form\\$folder\Search\\$entity" . "SearchType";
            $filenameRepository = "App\Repository\\$folder\\$entity" . "Repository";

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '<?php' . "\n");
            fwrite($fichier, '' . "\n");

            fwrite($fichier, 'namespace ' . $namespace . ';' . "\n");
            fwrite($fichier, '' . "\n");

            fwrite($fichier, 'use Doctrine\ORM\EntityManagerInterface;' . "\n");
            fwrite($fichier, 'use Knp\Component\Pager\PaginatorInterface;' . "\n");
            fwrite($fichier, 'use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;' . "\n");
            fwrite($fichier, 'use Symfony\Component\HttpFoundation\JsonResponse;' . "\n");
            fwrite($fichier, 'use Symfony\Component\HttpFoundation\Request;' . "\n");
            fwrite($fichier, 'use Symfony\Component\HttpFoundation\Response;' . "\n");
            fwrite($fichier, 'use Symfony\Component\Routing\Annotation\Route;' . "\n");

            if ($exportExcel) {
                fwrite($fichier, 'use PhpOffice\PhpSpreadsheet\Spreadsheet;' . "\n");
                fwrite($fichier, 'use PhpOffice\PhpSpreadsheet\Writer\Xlsx;' . "\n");
            }


            fwrite($fichier, '' . "\n");
            if ($statiqueData)
                fwrite($fichier, 'use App\Service\Generale\StatiqueData;' . "\n");
            if ($uploadableField)
                fwrite($fichier, 'use Vich\UploaderBundle\Handler\UploadHandler;' . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'use ' . $filenameEntity . ';' . "\n");
            if ($searching) {
                fwrite($fichier, 'use ' . $filenameEntitySearch . ';' . "\n");
                fwrite($fichier, 'use ' . $filenameFormSearch . ';' . "\n");
            }
            fwrite($fichier, 'use ' . $filenameForm . ';' . "\n");
            fwrite($fichier, 'use ' . $filenameRepository . ';' . "\n");

            $entity_lower = strtolower($entity);
            $route = strtolower(str_replace("\\", "/", $folder)) . '/' . $entity_lower;
            $route_name = str_replace('\\', '_', strtolower($folder)) . '_' . $entity_lower;

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '/**' . "\n");
            fwrite($fichier, ' * @Route("/' . $route . '")' . "\n");
            fwrite($fichier, ' */' . "\n");
            fwrite($fichier, 'class ' . $entity . 'Controller extends AbstractController' . "\n");
            fwrite($fichier, '{' . "\n");
            fwrite($fichier, '    /**' . "\n");
            fwrite($fichier, '     * @Route("/", name="' . $route_name . '_index", methods={"GET"})' . "\n");
            fwrite($fichier, '     */' . "\n");
            fwrite($fichier, '    public function index' . $entity . '(Request $request' . ($knpPaginator ? ', PaginatorInterface $paginator' : '') . ', ' . $entity . 'Repository $' . $entity_lower . 'Repository' . ($statiqueData ? ',StatiqueData $statiqueData' : '') . ', EntityManagerInterface $em): Response' . "\n");
            fwrite($fichier, '    {' . "\n");

            $optionsForm = [];
            foreach ($fields as $field) {
                if ($field['statiqueData'] != "") {
                    fwrite($fichier, '        $' . strtolower($field['statiqueData']) . '=$statiqueData->get' . $field['statiqueData'] . '();' . "\n");
                    $optionsForm[] = '"' . strtolower($field['statiqueData']) . '"=>$' . strtolower($field['statiqueData']);
                }
            }
            if (!empty($optionsForm))
                $optionsForm = ',[' . implode(',', $optionsForm) . ']';
            else
                $optionsForm = "";

            fwrite($fichier, '        $form = $this->createForm(' . $entity . 'Type::class,null' . $optionsForm . ');' . "\n");

            if ($searching) {
                fwrite($fichier, '        $search = new ' . $entity . 'Search();' . "\n");
                fwrite($fichier, '        if (isset($_GET["id"]))' . "\n");
                fwrite($fichier, '            $search->setId($_GET["id"]);' . "\n");
                fwrite($fichier, '        ' . "\n");
                fwrite($fichier, '        $form_search = $this->createForm(' . $entity . 'SearchType::class, $search' . $optionsForm . ');' . "\n");
                fwrite($fichier, '        $form_search->handleRequest($request);' . "\n");
            }

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        $sort = $request->query->get("sort", "id");' . "\n");
            fwrite($fichier, '        $order = $request->query->get("order", "DESC");' . "\n");
            fwrite($fichier, '        $limit = $request->query->getInt("limit", 10);' . "\n");
            fwrite($fichier, '        $page = $request->query->getInt("page", 1);' . "\n");

            fwrite($fichier, '        $multi_delete = $request->query->getInt("multi-delete", 0);' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        $items = $request->get("items");' . "\n");
            fwrite($fichier, '        if ($items)' . "\n");
            fwrite($fichier, '            if ($items == \'all\')' . "\n");
            fwrite($fichier, '                $' . $entity_lower . 's = $' . $entity_lower . 'Repository->getList' . $entity . '(' . ($searching ? '$search, ' : '') . '$sort, $order, "all");' . "\n");
            fwrite($fichier, '            elseif ($items == \'filtre\')' . "\n");
            fwrite($fichier, '                $' . $entity_lower . 's = $' . $entity_lower . 'Repository->getList' . $entity . '(' . ($searching ? '$search, ' : '') . '$sort, $order, $page, $limit);' . "\n");
            fwrite($fichier, '            else' . "\n");
            fwrite($fichier, '                $' . $entity_lower . 's = $' . $entity_lower . 'Repository->getList' . $entity . '(' . ($searching ? '$search, ' : '') . '$sort, $order, "all", "all", false, explode("-", $items));' . "\n");
            fwrite($fichier, '        ' . "\n");
            fwrite($fichier, '        if ($multi_delete) {' . "\n");
            fwrite($fichier, '            foreach ($' . $entity_lower . 's as $' . $entity_lower . ')' . "\n");
            fwrite($fichier, '                $em->remove($' . $entity_lower . ');' . "\n");
            fwrite($fichier, '            $em->flush();' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '        ' . "\n");

            if ($exportExcel) {
                fwrite($fichier, '        $export_xsl = $request->query->getInt("export-xsl", 0);' . "\n");
                fwrite($fichier, '        if ($export_xsl) {' . "\n");
                fwrite($fichier, '            $data = [' . "\n");

                foreach ($fields as $field) {
                    fwrite($fichier, '            "' . $field['label'] . '",' . "\n");
                }
                fwrite($fichier, '            ];' . "\n");
                fwrite($fichier, '            /** @var ' . $entity . ' $' . $entity_lower . ' */' . "\n");
                fwrite($fichier, '            foreach ($' . $entity_lower . 's as $' . $entity_lower . ') {' . "\n");

                foreach ($fields as $field) {
                    if ($field['list']) {
                        if ($field['relation'] == "ManyToMany") {
                            fwrite($fichier, '                $' . $field['fieldName'] . ' = "";' . "\n");
                            fwrite($fichier, '                foreach ($' . $entity_lower . '->get' . $field['getter/setter'] . '() as $item)' . "\n");
                            fwrite($fichier, '                    $' . $field['fieldName'] . ' .= ($' . $field['fieldName'] . ' ? ", " : "") . $item->__toString();' . "\n");
                            fwrite($fichier, '' . "\n");
                        }
                    }
                }

                foreach ($fields as $field) {
                    if ($field['list']) {
                        if ($field['relation'] == "ManyToOne") {
                            fwrite($fichier, '                $' . $field['fieldName'] . ' = "";' . "\n");
                            fwrite($fichier, '                if ($' . $entity_lower . '->get' . $field['getter/setter'] . '())' . "\n");
                            fwrite($fichier, '                    $' . $field['fieldName'] . ' = $' . $entity_lower . '->get' . $field['getter/setter'] . '()->__toString();' . "\n");
                            fwrite($fichier, '' . "\n");
                        }
                    }
                }

                fwrite($fichier, '                    $data[] = [' . "\n");

                foreach ($fields as $field) {
                    if ($field['list']) {
                        if ($field['relation'] != "x") {
                            fwrite($fichier, '                     $' . $field['fieldName'] . ',' . "\n");
                        } elseif ($field['scale'] > 0) {
                            fwrite($fichier, '                     number_format($' . $entity_lower . '->get' . $field['getter/setter'] . '(), ' . $field['scale'] . ', ".", ""),' . "\n");
                        } elseif ($field['type'] != "file") {
                            $format = "";
                            if ($field['type'] == 'date')
                                $format = '->format("d/m/Y")';
                            if ($field['type'] == 'datetime')
                                $format = '->format("d/m/Y H:i")';
                            if ($field['type'] == 'boolean')
                                $format = '? "Oui" : "Non"';
                            fwrite($fichier, '                     $' . $entity_lower . '->get' . $field['getter/setter'] . '()' . $format . ',' . "\n");
                        }
                    }
                }

                fwrite($fichier, '                ];' . "\n");
                fwrite($fichier, '            }' . "\n");

                fwrite($fichier, '            $spreadsheet = new Spreadsheet();' . "\n");
                fwrite($fichier, '            $sheet = $spreadsheet->getActiveSheet();' . "\n");
                fwrite($fichier, '            $sheet->fromArray($data, NULL, "A1");' . "\n");
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '            for ($i = "A"; $i !=  $sheet->getHighestColumn(); $i++)' . "\n");
                fwrite($fichier, '                $sheet->getColumnDimension($i)->setAutoSize(TRUE);' . "\n");
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '            $sheet->getStyle("A1:Z1")->getFont()->setBold(true);' . "\n");
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '            $writer = new Xlsx($spreadsheet);' . "\n");
                fwrite($fichier, '            $writer->save("export-pdf/' . $entity . 's.xlsx");' . "\n");
                fwrite($fichier, '            return new Response("/export-pdf/' . $entity . 's.xlsx");' . "\n");

                fwrite($fichier, '        }' . "\n");
            }

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        $' . $entity_lower . 's = $' . $entity_lower . 'Repository->getList' . $entity . '(' . ($searching ? '$search, ' : '') . '$sort, $order, $page, $limit,true);' . "\n");
            fwrite($fichier, '        $countItem = $' . $entity_lower . 's["nbr_result"];' . "\n");

            if ($knpPaginator) {
                fwrite($fichier, '        $' . $entity_lower . 's = $' . $entity_lower . 'Repository->getList' . $entity . '(' . ($searching ? '$search, ' : '') . '$sort, $order, $page, $limit);' . "\n");
                fwrite($fichier, '        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);' . "\n");
            } else
                fwrite($fichier, '        $' . $entity_lower . 's = $_' . $entity_lower . 's;' . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        if ($request->isXmlHttpRequest()) {' . "\n");
            fwrite($fichier, '            $content = $this->renderView("' . $folder . '/' . $entity . '/liste.html.twig", ["' . $entity_lower . 's" => $' . $entity_lower . 's]);' . "\n");


            if ($knpPaginator)
                fwrite($fichier, '            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);' . "\n");

            fwrite($fichier, '' . "\n");

            fwrite($fichier, '            return new JsonResponse(compact(["content"' . ($knpPaginator ? ', "pagination"' : '') . ', "countItem"]));' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        return $this->render("' . $folder . '/' . $entity . '/index.html.twig", [' . "\n");
            fwrite($fichier, '            "form" => $form->createView(),' . "\n");

            if ($searching)
                fwrite($fichier, '            "form_search" => $form_search->createView(),' . "\n");

            fwrite($fichier, '            "' . $entity_lower . 's" => $' . $entity_lower . 's,' . "\n");
            fwrite($fichier, '            "itemsPagination" => $itemsPagination,' . "\n");
            fwrite($fichier, '            "countItem" => $countItem' . "\n");
            fwrite($fichier, '        ]);' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    /**' . "\n");
            fwrite($fichier, '     * @Route("/new", name="' . $route_name . '_new", methods={"GET","POST"})' . "\n");
            fwrite($fichier, '     */' . "\n");
            fwrite($fichier, '    public function new' . $entity . '(Request $request, EntityManagerInterface $em' . ($statiqueData ? ',StatiqueData $statiqueData' : '') . '): Response' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        $' . $entity_lower . ' = new ' . $entity . '();' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '        $' . strtolower($field['statiqueData']) . '=$statiqueData->get' . $field['statiqueData'] . '();' . "\n");

            fwrite($fichier, '        $form = $this->createForm(' . $entity . 'Type::class, $' . $entity_lower . $optionsForm . ');' . "\n");
            fwrite($fichier, '        $form->handleRequest($request);' . "\n");
            fwrite($fichier, '        if ($form->isSubmitted() && $form->isValid()) {' . "\n");

            fwrite($fichier, '            if (method_exists($' . $entity_lower . ', "setTranslatableLocale"))' . "\n");
            fwrite($fichier, '                $' . $entity_lower . '->setTranslatableLocale($request->getLocale());' . "\n");

            fwrite($fichier, '            $em->persist($' . $entity_lower . ');' . "\n");
            fwrite($fichier, '            $em->flush();' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '            $content = $this->renderView("' . $folder . '/' . $entity . '/' . /*(!$knpPaginator ? '_' : '') .*/
                'item.html.twig", ["' . $entity_lower . '" => $' . $entity_lower . ']);' . "\n");
            fwrite($fichier, '            return new Response($content);' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '    }' . "\n");

            if ($view) {
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '    /**' . "\n");
                fwrite($fichier, '     * @Route("/{id}/view", name="' . $route_name . '_view", methods={"GET","POST"})' . "\n");
                fwrite($fichier, '     */' . "\n");
                fwrite($fichier, '    public function view' . $entity . '(Request $request, ' . $entity . ' $' . $entity_lower . ', EntityManagerInterface $em' . ($uploadableField ? ', UploadHandler $handler' : '') . ($statiqueData ? ',StatiqueData $statiqueData' : '') . '): Response' . "\n");
                fwrite($fichier, '    {' . "\n");
                fwrite($fichier, '        $content = $this->renderView("' . $folder . '/' . $entity . '/view.html.twig", ["' . $entity_lower . '"=>$' . $entity_lower . ']);' . "\n");
                fwrite($fichier, '        return new Response($content);' . "\n");
                fwrite($fichier, '    }' . "\n");
            }

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    /**' . "\n");
            fwrite($fichier, '     * @Route("/{id}/edit", name="' . $route_name . '_edit", methods={"GET","POST"})' . "\n");
            fwrite($fichier, '     */' . "\n");
            fwrite($fichier, '    public function edit' . $entity . '(Request $request, ' . $entity . ' $' . $entity_lower . ', EntityManagerInterface $em' . ($uploadableField ? ', UploadHandler $handler' : '') . ($statiqueData ? ',StatiqueData $statiqueData' : '') . '): Response' . "\n");
            fwrite($fichier, '    {' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '        $' . strtolower($field['statiqueData']) . '=$statiqueData->get' . $field['statiqueData'] . '();' . "\n");

            fwrite($fichier, '        $form = $this->createForm(' . $entity . 'Type::class, $' . $entity_lower . $optionsForm . ');' . "\n");
            fwrite($fichier, '        $form->handleRequest($request);' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        if ($form->isSubmitted() && $form->isValid()) {' . "\n");

            foreach ($fields as $field)
                if ($field['type'] == 'file') {
                    fwrite($fichier, '            if ($' . $entity_lower . '->get' . $field['getter/setter'] . 'Remove())' . "\n");
                    fwrite($fichier, '                $handler->remove($' . $entity_lower . ', "' . $field['fieldName'] . 'File");' . "\n");
                }

            fwrite($fichier, '' . "\n");

            fwrite($fichier, '            if (method_exists($' . $entity_lower . ', "setTranslatableLocale"))' . "\n");
            fwrite($fichier, '                $' . $entity_lower . '->setTranslatableLocale($request->getLocale());' . "\n");

            fwrite($fichier, '            $em->flush();' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '            $content = $this->renderView("' . $folder . '/' . $entity . '/' . /*(!$knpPaginator ? '_' : '') .*/
                'item.html.twig", ["' . $entity_lower . '" => $' . $entity_lower . (!$knpPaginator ? ',"index"=>$request->get("index")' : '') . ']);' . "\n");
            fwrite($fichier, '            return new Response($content);' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        $content = $this->renderView("' . $folder . '/' . $entity . '/form.html.twig", [' . "\n");
            fwrite($fichier, '            "form" => $form->createView(),' . "\n");
            fwrite($fichier, '            "action" => $this->generateUrl("' . $route_name . '_edit", [' . "\n");
            fwrite($fichier, '                "id" => $' . $entity_lower . '->getId()' . "\n");
            fwrite($fichier, '            ])' . "\n");
            fwrite($fichier, '        ]);' . "\n");
            fwrite($fichier, '        return new Response($content);' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    /**' . "\n");
            fwrite($fichier, '     * @Route("/{id}/delete", name="' . $route_name . '_delete")' . "\n");
            fwrite($fichier, '     */' . "\n");
            fwrite($fichier, '    public function delete' . $entity . '(' . $entity . ' $' . $entity_lower . ', EntityManagerInterface $em): Response' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        $em->remove($' . $entity_lower . ');' . "\n");
            fwrite($fichier, '        $em->flush();' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        return new Response(1);' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '}' . "\n");
            fclose($fichier);
            return [
                'route_name' => $route_name . '_index',
                'url' => $route,
                'path' => $path
            ];
        } catch (\Exception $exception) {
            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createFormType($entity, $folder, $fields, $copyOld, $uses, $copyTest)
    {
        try {
            $namespace = "App\Form\\$folder";
            $filename = $entity . "Type";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\src\Form\\$folder";
            $path = "$dirname\\$filename.php";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            $filenameEntity = "App\Entity\\$folder\\$entity";

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '<?php' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'namespace ' . $namespace . ';' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'use ' . $filenameEntity . ';' . "\n");
            fwrite($fichier, 'use Symfony\Component\Form\AbstractType;' . "\n");
            fwrite($fichier, 'use Symfony\Component\Form\FormBuilderInterface;' . "\n");
            fwrite($fichier, 'use Symfony\Component\OptionsResolver\OptionsResolver;' . "\n");
            fwrite($fichier, '' . "\n");

            foreach ($uses as $use)
                fwrite($fichier, $use . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'class ' . $entity . 'Type extends AbstractType' . "\n");
            fwrite($fichier, '{' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '    private $' . strtolower($field['statiqueData']) . ';' . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    public function buildForm(FormBuilderInterface $builder, array $options)' . "\n");
            fwrite($fichier, '    {' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '        $this->' . strtolower($field['statiqueData']) . ' = $options["' . strtolower($field['statiqueData']) . '"];' . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        $builder' . "\n");

            foreach ($fields as $field) {
                $required = $field['required'] ? 'true' : 'false';
                switch ($field['type']) {
                    case "string":
                        if ($field['statiqueData'] != "") {
                            fwrite($fichier, '            ->add("' . $field['fieldName'] . '", ChoiceType::class, [' . "\n");
                            fwrite($fichier, '                "choices" => $this->get' . $field['statiqueData'] . '(),' . "\n");
                            fwrite($fichier, '                "required" => ' . $required . ',' . "\n");
                            fwrite($fichier, '                "attr" => [' . "\n");
                            fwrite($fichier, '                    "class" => "select-search"' . "\n");
                            fwrite($fichier, '                ]' . "\n");
                            fwrite($fichier, '            ])' . "\n");
                        } else
                            fwrite($fichier, '            ->add("' . $field['fieldName'] . '")' . "\n");
                        break;
                    case "text":
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", CKEditorType::class, [' . "\n");
                        fwrite($fichier, '                    "required" => ' . $required . ',' . "\n");
                        if (!$field['nullable']) {
                            fwrite($fichier, '                "attr" => [' . "\n");
                            fwrite($fichier, '                    "data-required"=>"true"' . "\n");
                            fwrite($fichier, '                ],' . "\n");
                        }

                        fwrite($fichier, '                "config" => [' . "\n");
                        fwrite($fichier, '                    "uiColor" => "#ffffff",' . "\n");
                        fwrite($fichier, '                ]])' . "\n");
                        break;
                    case "boolean":
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", CheckboxType::class, [' . "\n");
                        fwrite($fichier, '                "required" => false' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                        break;
                    case "datetime":
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", DateTimeType::class, [' . "\n");
                        fwrite($fichier, '                "required" => ' . $required . ',' . "\n");
                        fwrite($fichier, '                "widget" => "single_text",' . "\n");
                        fwrite($fichier, '                "format" => DateTimeType::HTML5_FORMAT,' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                        break;
                    case "date":
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", DateType::class, [' . "\n");
                        fwrite($fichier, '                "required" => ' . $required . ',' . "\n");
                        fwrite($fichier, '                "widget" => "single_text",' . "\n");
                        fwrite($fichier, '                "format" => DateType::HTML5_FORMAT,' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                        break;
                    case "time":
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", TimeType::class, [' . "\n");
                        fwrite($fichier, '                "required" => ' . $required . ',' . "\n");
                        fwrite($fichier, '                "widget" => "single_text",' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                        break;
                    case "decimal":
                        $class = "";
                        $maxlength = $field['precision'] + 1;
                        if ($field['scale'] == 3)
                            $class = "money";

                        if ($field['scale'] == 2)
                            $class = "percent";

                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", null, [' . "\n");
                        fwrite($fichier, '                "attr" => [' . "\n");
                        fwrite($fichier, '                    "class" => "' . $class . '",' . "\n");
                        fwrite($fichier, '                    "maxlength" => ' . $maxlength . "\n");
                        fwrite($fichier, '                ]' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                        break;
                    case "integer":
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", IntegerType::class,[' . "\n");
                        fwrite($fichier, '                  "required" => ' . $required . '' . "\n");
                        fwrite($fichier, '                                ])' . "\n");
                        break;
                    case "file":
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . 'File", FileType::class, [' . "\n");
                        fwrite($fichier, '                "required" => false,' . "\n");
                        fwrite($fichier, '                "attr" => [' . "\n");
                        fwrite($fichier, '                    "class" => "input-file",' . "\n");
                        fwrite($fichier, '                    "data-show-caption" => "false",' . "\n");
                        fwrite($fichier, '                    "data-show-upload" => "false"' . "\n");
                        fwrite($fichier, '                ]' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . 'Remove", CheckboxType::class, [' . "\n");
                        fwrite($fichier, '                "required" => false,' . "\n");
                        fwrite($fichier, '                "label" => false,' . "\n");
                        fwrite($fichier, '                "attr" => [' . "\n");
                        fwrite($fichier, '                    "hidden" => true' . "\n");
                        fwrite($fichier, '                ]' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                        break;
                }

                if ($field['relation'] != "x") {
                    fwrite($fichier, '            ->add("' . $field['fieldName'] . '", null, [' . "\n");
                    fwrite($fichier, '                "attr" => [' . "\n");
                    fwrite($fichier, '                    "class" => "select-search"' . "\n");
                    fwrite($fichier, '                ]' . "\n");
                    fwrite($fichier, '            ])' . "\n");
                }
            }

            fwrite($fichier, '            ;' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    public function configureOptions(OptionsResolver $resolver)' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        $resolver->setDefaults([' . "\n");
            fwrite($fichier, '            "data_class" => ' . $entity . '::class,' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '            "' . strtolower($field['statiqueData']) . '" => null,' . "\n");

            fwrite($fichier, '        ]);' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");


            foreach ($fields as $field)
                if ($field['statiqueData'] != "") {
                    fwrite($fichier, '    private function get' . $field['statiqueData'] . '()' . "\n");
                    fwrite($fichier, '    {' . "\n");
                    fwrite($fichier, '        $choices = $this->' . strtolower($field['statiqueData']) . ';' . "\n");
                    fwrite($fichier, '        $output = [];' . "\n");
                    fwrite($fichier, '        foreach ($choices as $key=>$choice) {' . "\n");
                    fwrite($fichier, '            $output[$choice] = $key;' . "\n");
                    fwrite($fichier, '        }' . "\n");
                    fwrite($fichier, '' . "\n");
                    fwrite($fichier, '        return $output;' . "\n");
                    fwrite($fichier, '    }' . "\n");
                }


            fwrite($fichier, '}' . "\n");
            fclose($fichier);

            return ['path' => $path];
        } catch (\Exception $exception) {
            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createRepository($entity, $folder, $fields, $copyOld, $searching, $copyTest)
    {
        try {
            $namespace = "App\Repository\\$folder";
            $filename = $entity . "Repository";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\src\Repository\\$folder";
            $path = "$dirname\\$filename.php";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            $filenameEntitySearch = "App\Entity\\$folder\Search\\$entity" . "Search";
            $filenameEntity = "App\Entity\\$folder\\$entity";
            $entity_lower = strtolower($entity);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '<?php' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'namespace ' . $namespace . ';' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'use ' . $filenameEntity . ';' . "\n");

            if ($searching)
                fwrite($fichier, 'use ' . $filenameEntitySearch . ';' . "\n");

            fwrite($fichier, 'use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;' . "\n");
            fwrite($fichier, 'use Doctrine\Persistence\ManagerRegistry;' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '/**' . "\n");
            fwrite($fichier, ' * @method ' . $entity . '|null find($id, $lockMode = null, $lockVersion = null)' . "\n");
            fwrite($fichier, ' * @method ' . $entity . '|null findOneBy(array $criteria, array $orderBy = null)' . "\n");
            fwrite($fichier, ' * @method ' . $entity . '[]    findAll()' . "\n");
            fwrite($fichier, ' * @method ' . $entity . '[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)' . "\n");
            fwrite($fichier, ' */' . "\n");
            fwrite($fichier, 'class ' . $entity . 'Repository extends ServiceEntityRepository' . "\n");
            fwrite($fichier, '{' . "\n");
            fwrite($fichier, '    public function __construct(ManagerRegistry $registry)' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        parent::__construct($registry, ' . $entity . '::class);' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    /**' . "\n");
            fwrite($fichier, '     * @return ' . $entity . '[] Returns an array of ' . $entity . ' objects' . "\n");
            fwrite($fichier, '     */' . "\n");
            fwrite($fichier, '    public function getList' . $entity . '(' . ($searching ? $entity . 'Search $search,' : '') . '$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        $query = $this->createQueryBuilder("' . $entity_lower . '");' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        if ($count)' . "\n");
            fwrite($fichier, '            $query->select("COUNT(' . $entity_lower . '.id) As nbr_result");' . "\n");
            fwrite($fichier, '' . "\n");

            fwrite($fichier, '        if (!empty($ids))' . "\n");
            fwrite($fichier, '            $query->andWhere($query->expr()->in("' . $entity_lower . '.id", $ids));' . "\n");
            fwrite($fichier, '' . "\n");

            if($searching){
                fwrite($fichier, '        if ($search->getId())' . "\n");
                fwrite($fichier, '            $query->andWhere("' . $entity_lower . '.id = :id")' . "\n");
                fwrite($fichier, '                ->setParameter("id", $search->getId());' . "\n");
                fwrite($fichier, '' . "\n");
            }

            foreach ($fields as $field) {
                if ($field['search']) {
                    if ($field['searchInter']) {
                        fwrite($fichier, '        if ($search->get' . $field['getter/setter'] . 'De())' . "\n");
                        fwrite($fichier, '            $query->andWhere("' . $entity_lower . '.' . $field['fieldName'] . ' >= :' . $field['fieldName'] . '_de")' . "\n");
                        fwrite($fichier, '                ->setParameter("' . $field['fieldName'] . '_de", $search->get' . $field['getter/setter'] . 'De());' . "\n");
                        fwrite($fichier, '' . "\n");
                        fwrite($fichier, '        if ($search->get' . $field['getter/setter'] . 'Au())' . "\n");
                        fwrite($fichier, '            $query->andWhere("' . $entity_lower . '.' . $field['fieldName'] . ' <= :' . $field['fieldName'] . '_au")' . "\n");
                        fwrite($fichier, '                ->setParameter("' . $field['fieldName'] . '_au", $search->get' . $field['getter/setter'] . 'Au());' . "\n");
                        fwrite($fichier, '' . "\n");
                    } else {
                        if ($field['type'] == 'string') {
                            fwrite($fichier, '        if ($search->get' . $field['getter/setter'] . '())' . "\n");
                            fwrite($fichier, '            $query->andWhere($query->expr()->like("UPPER(' . $entity_lower . '.' . $field['fieldName'] . ')", "UPPER(\'%".$search->get' . $field['getter/setter'] . '()."%\')"));' . "\n");
                            fwrite($fichier, '' . "\n");
                        } elseif ($field['relation'] == 'ManyToMany') {
                            $item = substr($field['fieldName'], 0, strlen($field['fieldName']) - 1);
                            fwrite($fichier, '        if ($search->get' . $field['getter/setter'] . '()->count() >0){' . "\n");
                            fwrite($fichier, '            $k=0;' . "\n");
                            fwrite($fichier, '            foreach ($search->get' . $field['getter/setter'] . '() as $' . $item . ') {' . "\n");
                            fwrite($fichier, '                $k++;' . "\n");
                            fwrite($fichier, '                $query->andWhere(":' . $item . '$k MEMBER OF ' . $entity_lower . '.' . $field['fieldName'] . '")' . "\n");
                            fwrite($fichier, '                    ->setParameter("' . $item . '$k", $' . $item . ');' . "\n");
                            fwrite($fichier, '            }' . "\n");
                            fwrite($fichier, '        }' . "\n");
                            fwrite($fichier, '' . "\n");
                        } else {
                            fwrite($fichier, '        if ($search->get' . $field['getter/setter'] . '())' . "\n");
                            fwrite($fichier, '            $query->andWhere("' . $entity_lower . '.' . $field['fieldName'] . ' = :' . $field['fieldName'] . '")' . "\n");
                            fwrite($fichier, '                ->setParameter("' . $field['fieldName'] . '", $search->get' . $field['getter/setter'] . '());' . "\n");
                            fwrite($fichier, '' . "\n");
                        }
                    }
                }
            }

            fwrite($fichier, '        if ($count)' . "\n");
            fwrite($fichier, '            return $query->getQuery()->getOneOrNullResult();' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        if (is_integer($page) and is_integer($limit))' . "\n");
            fwrite($fichier, '            $query->setMaxResults($limit)' . "\n");
            fwrite($fichier, '                ->setFirstResult(($page - 1) * $limit);' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        return $query' . "\n");
            fwrite($fichier, '            ->orderBy("' . $entity_lower . '.$sort", $order)' . "\n");
            fwrite($fichier, '            ->getQuery()' . "\n");
            fwrite($fichier, '            ->getResult();' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '}' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createFormSearchType($entity, $folder, $fields, $copyOld, $uses, $copyTest)
    {
        try {
            $namespace = "App\Form\\$folder\Search";
            $filename = $entity . "SearchType";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\src\Form\\$folder\Search";
            $path = "$dirname\\$filename.php";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            $filenameEntitySearch = "App\Entity\\$folder\Search\\$entity" . "Search";

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $fichier = fopen($path, 'c+b');


            fwrite($fichier, '<?php' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'namespace ' . $namespace . ';' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'use ' . $filenameEntitySearch . ';' . "\n");
            fwrite($fichier, 'use Symfony\Component\Form\AbstractType;' . "\n");
            fwrite($fichier, 'use Symfony\Component\Form\FormBuilderInterface;' . "\n");
            fwrite($fichier, 'use Symfony\Component\OptionsResolver\OptionsResolver;' . "\n");
            fwrite($fichier, 'use Symfony\Component\Form\Extension\Core\Type\HiddenType;' . "\n");
            fwrite($fichier, '' . "\n");

            foreach ($uses as $use)
                fwrite($fichier, $use . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'class ' . $entity . 'SearchType extends AbstractType' . "\n");
            fwrite($fichier, '{' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '    private $' . strtolower($field['statiqueData']) . ';' . "\n");

            fwrite($fichier, '    public function buildForm(FormBuilderInterface $builder, array $options)' . "\n");
            fwrite($fichier, '    {' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '        $this->' . strtolower($field['statiqueData']) . ' = $options["' . strtolower($field['statiqueData']) . '"];' . "\n");

            fwrite($fichier, '' . "\n");

            fwrite($fichier, '        $builder' . "\n");
            fwrite($fichier, '            ->add("id", HiddenType::class,[' . "\n");
            fwrite($fichier, '                "required" => false' . "\n");
            fwrite($fichier, '            ])' . "\n");

            foreach ($fields as $field) {
                if ($field['search']) {
                    $prefix = [""];
                    if ($field['searchInter'])
                        $prefix = ["_de", "_au"];
                    switch ($field['type']) {
                        case "string":
                            if ($field['statiqueData'] != "") {
                                fwrite($fichier, '            ->add("' . $field['fieldName'] . '", ChoiceType::class, [' . "\n");
                                fwrite($fichier, '                "choices" => $this->get' . $field['statiqueData'] . '(),' . "\n");
                                fwrite($fichier, '                "required" => false,' . "\n");
                                fwrite($fichier, '                "attr" => [' . "\n");
                                fwrite($fichier, '                    "class" => "select-search"' . "\n");
                                fwrite($fichier, '                ]' . "\n");
                                fwrite($fichier, '            ])' . "\n");
                            } else {
                                fwrite($fichier, '            ->add("' . $field['fieldName'] . '", null,[' . "\n");
                                fwrite($fichier, '                "required" => false' . "\n");
                                fwrite($fichier, '            ])' . "\n");
                            }
                            break;
                        case "boolean":
                            fwrite($fichier, '            ->add("' . $field['fieldName'] . '", CheckboxType::class, [' . "\n");
                            fwrite($fichier, '                "required" => false' . "\n");
                            fwrite($fichier, '            ])' . "\n");
                            break;
                        case "datetime":
                            foreach ($prefix as $prfx) {
                                fwrite($fichier, '            ->add("' . $field['fieldName'] . $prfx . '", DateTimeType::class, [' . "\n");
                                fwrite($fichier, '                "required" => false,' . "\n");
                                fwrite($fichier, '                "widget" => "single_text",' . "\n");
                                fwrite($fichier, '                "format" => DateTimeType::HTML5_FORMAT,' . "\n");
                                fwrite($fichier, '            ])' . "\n");
                            }
                            break;
                        case "date":
                            foreach ($prefix as $prfx) {
                                fwrite($fichier, '            ->add("' . $field['fieldName'] . $prfx . '", DateType::class, [' . "\n");
                                fwrite($fichier, '                "required" => false,' . "\n");
                                fwrite($fichier, '                "widget" => "single_text",' . "\n");
                                fwrite($fichier, '                "format" => DateType::HTML5_FORMAT,' . "\n");
                                fwrite($fichier, '            ])' . "\n");
                            }
                            break;
                        case "time":
                            foreach ($prefix as $prfx) {
                                fwrite($fichier, '            ->add("' . $field['fieldName'] . $prfx . '", TimeType::class, [' . "\n");
                                fwrite($fichier, '                "required" => false,' . "\n");
                                fwrite($fichier, '                "widget" => "single_text",' . "\n");
                                fwrite($fichier, '            ])' . "\n");
                            }
                            break;
                        case "decimal":
                            $class = "";
                            $maxlength = $field['precision'] + 1;
                            if ($field['scale'] == 3)
                                $class = "money";

                            if ($field['scale'] == 2)
                                $class = "percent";
                            foreach ($prefix as $prfx) {
                                fwrite($fichier, '            ->add("' . $field['fieldName'] . $prfx . '", null, [' . "\n");
                                fwrite($fichier, '                "required" => false,' . "\n");
                                fwrite($fichier, '                "attr" => [' . "\n");
                                fwrite($fichier, '                    "class" => "' . $class . '",' . "\n");
                                fwrite($fichier, '                    "maxlength" => ' . $maxlength . "\n");
                                fwrite($fichier, '                ]' . "\n");
                                fwrite($fichier, '            ])' . "\n");
                            }
                            break;
                        case "integer":
                            foreach ($prefix as $prfx) {
                                fwrite($fichier, '            ->add("' . $field['fieldName'] . $prfx . '", IntegerType::class,[' . "\n");
                                fwrite($fichier, '                "required" => false' . "\n");
                                fwrite($fichier, '            ])' . "\n");
                            }
                            break;
                    }

                    if ($field['relation'] == "ManyToOne") {
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", EntityType::class, [' . "\n");
                        fwrite($fichier, '                "class"=>' . $field['class'] . '::class,' . "\n");
                        fwrite($fichier, '                "required" => false,' . "\n");
                        fwrite($fichier, '                "attr" => [' . "\n");
                        fwrite($fichier, '                    "class" => "select-search"' . "\n");
                        fwrite($fichier, '                ]' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                    }
                    if ($field['relation'] == "ManyToMany") {
                        fwrite($fichier, '            ->add("' . $field['fieldName'] . '", EntityType::class, [' . "\n");
                        fwrite($fichier, '                "class"=>' . $field['class'] . '::class,' . "\n");
                        fwrite($fichier, '                "required" => false,' . "\n");
                        fwrite($fichier, '                "multiple"=>true,' . "\n");
                        fwrite($fichier, '                "attr" => [' . "\n");
                        fwrite($fichier, '                    "class" => "select-search"' . "\n");
                        fwrite($fichier, '                ]' . "\n");
                        fwrite($fichier, '            ])' . "\n");
                    }
                }

            }

            fwrite($fichier, '            ;' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    public function configureOptions(OptionsResolver $resolver)' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        $resolver->setDefaults([' . "\n");
            fwrite($fichier, '            "data_class" => ' . $entity . 'Search::class,' . "\n");
            fwrite($fichier, '            "method"=>"get",' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "")
                    fwrite($fichier, '            "' . strtolower($field['statiqueData']) . '" => null,' . "\n");

            fwrite($fichier, '            "csrf_protection"=>false' . "\n");
            fwrite($fichier, '        ]);' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    public function getBlockPrefix()' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        return "filtre";' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");

            foreach ($fields as $field)
                if ($field['statiqueData'] != "") {
                    fwrite($fichier, '    private function get' . $field['statiqueData'] . '()' . "\n");
                    fwrite($fichier, '    {' . "\n");
                    fwrite($fichier, '        $choices = $this->' . strtolower($field['statiqueData']) . ';' . "\n");
                    fwrite($fichier, '        $output = [];' . "\n");
                    fwrite($fichier, '        foreach ($choices as $key=>$choice) {' . "\n");
                    fwrite($fichier, '            $output[$choice] = $key;' . "\n");
                    fwrite($fichier, '        }' . "\n");
                    fwrite($fichier, '' . "\n");
                    fwrite($fichier, '        return $output;' . "\n");
                    fwrite($fichier, '    }' . "\n");
                }


            fwrite($fichier, '}' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createEntitySearch($entity, $folder, $fields, $copyOld, $useArrayCollection, $copyTest)
    {
        $uses = [];
        try {
            $namespace = "App\Entity\\$folder\Search";
            $filename = $entity . "Search";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\src\Entity\\$folder\Search";
            $path = "$dirname\\$filename.php";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '<?php' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'namespace ' . $namespace . ';' . "\n");
            fwrite($fichier, '' . "\n");

            if ($useArrayCollection)
                fwrite($fichier, 'use Doctrine\Common\Collections\ArrayCollection;' . "\n");

            foreach ($fields as $field)
                if ($field['relation'] == "ManyToOne")
                    if (!in_array($field['namespace'], $uses)) {
                        fwrite($fichier, 'use ' . $field['namespace'] . ';' . "\n");
                        $uses[] = $field['namespace'];
                    }


            fwrite($fichier, '' . "\n");
            fwrite($fichier, 'class ' . $entity . 'Search' . "\n");
            fwrite($fichier, '{' . "\n");

            fwrite($fichier, '    /** @var int|null */' . "\n");
            fwrite($fichier, '    private $id;' . "\n");
            fwrite($fichier, '' . "\n");

            foreach ($fields as $field) {
                if ($field['search']) {
                    $prefix = [""];
                    if ($field['searchInter'])
                        $prefix = ["_de", "_au"];
                    foreach ($prefix as $prfx) {
                        fwrite($fichier, '    /** @var ' . $field['_type'] . '|null */' . "\n");
                        fwrite($fichier, '    private $' . $field['fieldName'] . $prfx . ';' . "\n");
                        fwrite($fichier, '' . "\n");
                    }
                }
            }

            if ($useArrayCollection) {

                fwrite($fichier, '    /**' . "\n");
                fwrite($fichier, '     * ' . $entity . 'Search constructor.' . "\n");
                fwrite($fichier, '     */' . "\n");
                fwrite($fichier, '    public function __construct()' . "\n");
                fwrite($fichier, '    {' . "\n");

                foreach ($fields as $field) {
                    if ($field['search'] and $field['relation'] == 'ManyToMany') {
                        fwrite($fichier, '        $this->' . $field['fieldName'] . ' = new ArrayCollection();' . "\n");
                    }
                }

                fwrite($fichier, '    }' . "\n");
                fwrite($fichier, '' . "\n");
            }

            fwrite($fichier, '    /**' . "\n");
            fwrite($fichier, '     * @return null|int' . "\n");
            fwrite($fichier, '     */' . "\n");
            fwrite($fichier, '    public function getId(): ?int' . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        return $this->id;' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");

            fwrite($fichier, '    public function setId(?int $id): ' . $filename . "\n");
            fwrite($fichier, '    {' . "\n");
            fwrite($fichier, '        $this->id = $id;' . "\n");
            fwrite($fichier, '        return $this;' . "\n");
            fwrite($fichier, '    }' . "\n");
            fwrite($fichier, '' . "\n");

            foreach ($fields as $field) {
                if ($field['search']) {
                    $prefix = ["" => ""];
                    if ($field['searchInter'])
                        $prefix = ["_de" => "De", "_au" => "Au"];
                    foreach ($prefix as $key => $prfx) {
                        fwrite($fichier, '    /**' . "\n");
                        fwrite($fichier, '     * @return null|' . $field['_type'] . '' . "\n");
                        fwrite($fichier, '     */' . "\n");
                        fwrite($fichier, '    public function get' . $field['getter/setter'] . $prfx . '(): ?' . $field['_type'] . '' . "\n");
                        fwrite($fichier, '    {' . "\n");
                        fwrite($fichier, '        return $this->' . $field['fieldName'] . $key . ';' . "\n");
                        fwrite($fichier, '    }' . "\n");
                        fwrite($fichier, '' . "\n");
                        fwrite($fichier, '    /**' . "\n");
                        fwrite($fichier, '     * @param null|' . $field['_type'] . ' $' . $field['fieldName'] . "\n");
                        fwrite($fichier, '     * @return ' . $filename . "\n");
                        fwrite($fichier, '     */' . "\n");
                        fwrite($fichier, '    public function set' . $field['getter/setter'] . $prfx . '(?' . $field['_type'] . ' $' . $field['fieldName'] . '): ' . $filename . "\n");
                        fwrite($fichier, '    {' . "\n");
                        fwrite($fichier, '        $this->' . $field['fieldName'] . $key . ' = $' . $field['fieldName'] . ';' . "\n");
                        fwrite($fichier, '        return $this;' . "\n");
                        fwrite($fichier, '    }' . "\n");
                        fwrite($fichier, '' . "\n");
                    }
                }
            }

            fwrite($fichier, '}' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch
        (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createViewTwig($entity, $folder, $fields, $copyOld, $copyTest)
    {
        try {
            $filename = "view.html";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\\templates\\$folder\\$entity";
            $path = "$dirname\\$filename.twig";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);
            $entity_lower = strtolower($entity);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '<div class="main-body">
    <div class="row gutters-sm">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-body">
                    ' . "\n");

            foreach ($fields as $key=>$field) {
                $lbl = "";
                if ($field['translatable'])
                    $lbl = ' ~ " (" ~ app.request.locale|upper ~ ")"';

                if ($field['type'] == 'boolean')
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . '|viewBooleanValue|raw }}';
                elseif ($field['type'] == 'date')
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . '|date("d/m/Y") }}';
                elseif ($field['type'] == 'datetime')
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . '|date("d/m/Y H:i") }}';
                elseif ($field['type'] == 'time')
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . '|date("H:i") }}';
                elseif ($field['type'] == 'text')
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . '|raw }}';
                elseif ($field['class'] == 'money')
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . '|number_format(3,".","") }}';
                elseif ($field['class'] == 'percent')
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . '|number_format(2,".","") }}';
                elseif ($field['type'] == 'file')
                    $value ='                            {% if fileIsImage(vich_uploader_asset(' . $entity_lower . ', "' . $field['fieldName'] . 'File")|slice(1)) %}
                                <img src="{{ vich_uploader_asset(' . $entity_lower . ', "' . $field['fieldName'] . 'File")|default("https://via.placeholder.com/100x100?text="~' . $entity_lower . ') }}"
                                     height="100">
                            {% else %}
                                <a href="{{ vich_uploader_asset(' . $entity_lower . ', "' . $field['fieldName'] . 'File") }}"
                                   target="_blank">{{ vich_uploader_asset(' . $entity_lower . ', "' . $field['fieldName'] . 'File")|slice(1) }}</a>
                            {% endif %}';
                else
                    $value = '{{ ' . $entity_lower . '.' . $field['fieldName'] . ' }}';

                fwrite($fichier, '                    <div class="row">
                        <div class="col-sm-3">
                            <h6 class="mb-0">{{ "' . $field['label'] . '"|trans' . $lbl . ' }}</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            ' . $value . '
                        </div>
                    </div>' . "\n");
                if ($key !== array_key_last($fields))
                    fwrite($fichier, '                    <hr>' . "\n");
            }

            fwrite($fichier, '
                </div>
            </div>
        </div>
    </div>
</div>' . "\n");
            fwrite($fichier, '' . "\n");

            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createFormTwig($entity, $folder, $fields, $copyOld, $classes, $knpPaginator, $copyTest, $singleItem)
    {
        try {
            $filename = "form.html";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\\templates\\$folder\\$entity";
            $path = "$dirname\\$filename.twig";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);
            $entity_lower = strtolower($entity);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '{% set mode = form.vars.value is not null ? "edit" : "add" %}' . "\n");
            fwrite($fichier, '{{ form_start(form,{attr : { action : action } } ) }}' . "\n");
            fwrite($fichier, '{% if mode == "edit" %}' . "\n");
            fwrite($fichier, '    <input type="hidden" id="id" value="{{ form.vars.value.id }}">' . "\n");
            if (!$knpPaginator)
                fwrite($fichier, '    <input type="hidden" name="index">' . "\n");
            fwrite($fichier, '{% endif %}' . "\n");
            fwrite($fichier, '<div class="row">' . "\n");

            foreach ($fields as $field) {
                $lbl = "";
                if ($field['translatable'])
                    $lbl = ' ~ " (" ~ app.request.locale|upper ~ ")"';

                if ($field['type'] == 'text')
                    fwrite($fichier, '    {{ form_input("col-md-' . $field['col-md'] . '","' . $field['label'] . '"|trans' . $lbl . ',form.' . $field['fieldName'] . ',null,null,mode=="edit") }}' . "\n");
                elseif ($field['type'] == 'boolean')
                    fwrite($fichier, '    {{ form_checkbox("col-md-' . $field['col-md'] . ' m-t-25","' . $field['label'] . '"|trans,form.' . $field['fieldName'] . ') }}' . "\n");
                elseif ($field['type'] == 'file') {
                    fwrite($fichier, '    {{ form_input("col-md-' . $field['col-md'] . '","' . $field['label'] . '"|trans,form.' . $field['fieldName'] . 'File) }}' . "\n");
                    fwrite($fichier, '    {{ form_widget(form.' . $field['fieldName'] . 'Remove) }}' . "\n");
                } else
                    fwrite($fichier, '    {{ form_input("col-md-' . $field['col-md'] . '","' . $field['label'] . '"|trans' . $lbl . ',form.' . $field['fieldName'] . ') }}' . "\n");
            }

            fwrite($fichier, '</div>' . "\n");
            fwrite($fichier, '<hr>' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '<div class="text-right">' . "\n");
            fwrite($fichier, '    <button type="submit" class="btn btn-primary"><i' . "\n");
            fwrite($fichier, '                class="fa fa-{{ (mode == "edit" ? "edit" : "save") }}  position-left"></i> {{ (mode == "edit" ? "Modifier" : "Enregistrer")|trans }}' . "\n");
            fwrite($fichier, '    </button>' . "\n");
            fwrite($fichier, '</div>' . "\n");
            fwrite($fichier, '{{ form_end(form) }}' . "\n");
            fwrite($fichier, '{% if mode == "edit" %}' . "\n");
            fwrite($fichier, '    <script>' . "\n");

            if (in_array('select', $classes)) {
                fwrite($fichier, '        $("#modal-edit-' . $entity_lower . ' .select-search").select2();' . "\n");
                fwrite($fichier, '        $("#modal-edit-' . $entity_lower . ' .select").select2({minimumResultsForSearch: Infinity});' . "\n");
            }
            if (in_array('money', $classes))
                fwrite($fichier, '        $("#modal-edit-' . $entity_lower . ' .money").number(true, 3, ".", "");' . "\n");

            if (in_array('percent', $classes))
                fwrite($fichier, '        $("#modal-edit-' . $entity_lower . ' .percent").number(true, 2, ".", "");' . "\n");

            if (in_array('styled', $classes))
                fwrite($fichier, '        $("#modal-edit-' . $entity_lower . ' .styled").uniform();' . "\n");

            if (in_array('input-file', $classes)) {
                fwrite($fichier, '        $("#modal-edit-' . $entity_lower . ' .input-file").each(function () {' . "\n");
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '            let img = "";' . "\n");

                foreach ($fields as $field) {
                    if ($field['type'] == 'file') {
                        fwrite($fichier, '            if ($(this).attr("name") == "' . $entity_lower . '[' . $field['fieldName'] . 'File]")' . "\n");
                        fwrite($fichier, '                img = "{{ vich_uploader_asset(form.vars.value, "' . $field['fieldName'] . 'File") }}";' . "\n");
                        fwrite($fichier, '' . "\n");
                    }
                }

                fwrite($fichier, '            $(this).fileinput({' . "\n");
                fwrite($fichier, '                browseLabel: language.parcourir,' . "\n");
                fwrite($fichier, '                browseIcon: "<i class=\'icon-file-plus\'></i>",' . "\n");
                fwrite($fichier, '                uploadIcon: "<i class=\'icon-file-upload2\'></i>",' . "\n");
                fwrite($fichier, '                removeIcon: "<i class=\'icon-cross3\'></i>",' . "\n");
                fwrite($fichier, '                layoutTemplates: {' . "\n");
                fwrite($fichier, '                    icon: "<i class=\'icon-file-check\'></i>",' . "\n");
                fwrite($fichier, '                    modal: modalTemplate' . "\n");
                fwrite($fichier, '                },' . "\n");
                fwrite($fichier, '                initialPreview: [' . "\n");
                fwrite($fichier, '                    img' . "\n");
                fwrite($fichier, '                ],' . "\n");
                fwrite($fichier, '                initialPreviewAsData: true,' . "\n");
                fwrite($fichier, '                overwriteInitial: true,' . "\n");
                fwrite($fichier, '                previewZoomButtonClasses: previewZoomButtonClasses,' . "\n");
                fwrite($fichier, '                previewZoomButtonIcons: previewZoomButtonIcons,' . "\n");
                fwrite($fichier, '                fileActionSettings: fileActionSettings' . "\n");
                fwrite($fichier, '            });' . "\n");
                fwrite($fichier, '        })' . "\n");
            }

            fwrite($fichier, '' . "\n");

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    </script>' . "\n");
            fwrite($fichier, '{% endif %}' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createIndexTwig($entity, $folder, $fields, $copyOld, $exportExcel, $uploadableField, $searching, $widthModal, $CKEDITOR, $knpPaginator, $copyTest, $viewItem)
    {
        try {
            $_folder = str_replace("\\", "/", $folder);
            $filename = "index.html";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\\templates\\$folder\\$entity";
            $path = "$dirname\\$filename.twig";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $entity_lower = strtolower($entity);
            $route_name = str_replace('\\', '_', strtolower($folder)) . '_' . $entity_lower;

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '{% extends "base.html.twig" %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '{% block title %}{{ "' . $entity . '"|trans }}{% endblock %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '{% block stylesheets %}' . "\n");
            fwrite($fichier, '    {{ parent() }}' . "\n");
            fwrite($fichier, '    <link href="{{ asset("assets/css/style-crud.css") }}" rel="stylesheet" type="text/css">' . "\n");

            if (!$knpPaginator)
                fwrite($fichier, '    <link href="{{ asset("assets/css/bootstrap-table.min.css") }}" rel="stylesheet" type="text/css">' . "\n");

            fwrite($fichier, '{% endblock %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '{% block body %}' . "\n");
            fwrite($fichier, '    <style>' . "\n");
            fwrite($fichier, '        div.icon-spinner3 {' . "\n");
            fwrite($fichier, '            font-size: 100%' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        .m-t-25 {' . "\n");
            fwrite($fichier, '            margin-top: 25px;' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '' . "\n");

            if ($widthModal) {
                fwrite($fichier, '        .modal-lg {' . "\n");
                fwrite($fichier, '            width: ' . $widthModal . '%' . "\n");
                fwrite($fichier, '        }' . "\n");
            }

            fwrite($fichier, '    </style>' . "\n");
            fwrite($fichier, '    <div id="message-alert"></div>' . "\n");
            fwrite($fichier, '    <div id="modal-add-' . $entity_lower . '" class="modal fade" tabindex="-1">' . "\n");
            fwrite($fichier, '        <div id="modal-message-alert"></div>' . "\n");
            fwrite($fichier, '        <div class="modal-dialog modal-lg">' . "\n");
            fwrite($fichier, '            <div class="modal-content">' . "\n");
            fwrite($fichier, '                <div class="modal-header bg-primary">' . "\n");
            fwrite($fichier, '                    <h6 class="modal-title">{{ "Ajout d\'un nouvel élément"|trans }}</h6>' . "\n");
            fwrite($fichier, '                    <button type="button" class="close" data-dismiss="modal">&times;</button>' . "\n");
            fwrite($fichier, '                </div>' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '                <div class="modal-body" style="overflow:hidden;">' . "\n");
            fwrite($fichier, '                    {{ include("' . $_folder . '/' . $entity . '/form.html.twig",{action:path("' . $route_name . '_new")}) }}' . "\n");
            fwrite($fichier, '                </div>' . "\n");
            fwrite($fichier, '            </div>' . "\n");
            fwrite($fichier, '        </div>' . "\n");
            fwrite($fichier, '    </div>' . "\n");

            fwrite($fichier, '    <div id="modal-edit-' . $entity_lower . '" class="modal fade" tabindex="-1">' . "\n");
            fwrite($fichier, '        <div id="modal-message-alert"></div>' . "\n");
            fwrite($fichier, '        <div class="modal-dialog modal-lg">' . "\n");
            fwrite($fichier, '            <div class="modal-content">' . "\n");
            fwrite($fichier, '                <div class="modal-header bg-primary">' . "\n");
            fwrite($fichier, '                    <h6 class="modal-title">{{ "Modifier élément"|trans }}</h6>' . "\n");
            fwrite($fichier, '                    <button type="button" class="close" data-dismiss="modal">&times;</button>' . "\n");
            fwrite($fichier, '                </div>' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '                <div class="modal-body" style="overflow:hidden;">' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '                </div>' . "\n");
            fwrite($fichier, '            </div>' . "\n");
            fwrite($fichier, '        </div>' . "\n");
            fwrite($fichier, '    </div>' . "\n");

            if ($viewItem) {
                fwrite($fichier, '    <div id="modal-view-' . $entity_lower . '" class="modal fade" tabindex="-1">' . "\n");
                fwrite($fichier, '        <div id="modal-message-alert"></div>' . "\n");
                fwrite($fichier, '        <div class="modal-dialog modal-md">' . "\n");
                fwrite($fichier, '            <div class="modal-content">' . "\n");
                fwrite($fichier, '                <div class="modal-header bg-primary">' . "\n");
                fwrite($fichier, '                    <h6 class="modal-title">{{ "Consulter élément"|trans }}</h6>' . "\n");
                fwrite($fichier, '                    <button type="button" class="close" data-dismiss="modal">&times;</button>' . "\n");
                fwrite($fichier, '                </div>' . "\n");
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '                <div class="modal-body" style="overflow:hidden;">' . "\n");
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '                </div>' . "\n");
                fwrite($fichier, '            </div>' . "\n");
                fwrite($fichier, '        </div>' . "\n");
                fwrite($fichier, '    </div>' . "\n");
            }

            fwrite($fichier, '    <div class="panel panel-flat">' . "\n");
            fwrite($fichier, '        <div class="panel-heading">' . "\n");
            fwrite($fichier, '            <h5 class="panel-title">{{ "' . $entity . 's"|trans }} (<span' . "\n");

            if ($knpPaginator)
                fwrite($fichier, '                        id="count-item-' . $entity_lower . '">{{ countItem }}</span> {{ "éléments"|trans }})<a' . "\n");
            else
                fwrite($fichier, '                        id="count-item-' . $entity_lower . '">{{ ' . $entity_lower . 's|length }}</span> {{ "éléments"|trans }})<a' . "\n");

            fwrite($fichier, '                        class="heading-elements-toggle"><i class="icon-more"></i></a></h5>' . "\n");
            fwrite($fichier, '            <div class="heading-elements">' . "\n");
            fwrite($fichier, '                <a href="#modal-add-' . $entity_lower . '" data-toggle="modal" class="btn border-primary btn-xs text-slate-800 btn-flat"><i' . "\n");
            fwrite($fichier, '                            class="icon-plus-circle2 position-left"></i> {{ "Nouvel  Élément"|trans }}</a>' . "\n");

            if ($searching) {
                fwrite($fichier, '                {% if not app.request.get("id") %}' . "\n");
                fwrite($fichier, '                    <a data-toggle="collapse" data-target="#form-filtre"' . "\n");
                fwrite($fichier, '                       class="btn border-primary btn-xs text-slate-800 btn-flat"><i' . "\n");
                fwrite($fichier, '                                class="icon-search4 position-left"></i> {{ "Recherche"|trans }}</a>' . "\n");
                fwrite($fichier, '                {% endif %}' . "\n");
            }

            fwrite($fichier, '            </div>' . "\n");
            fwrite($fichier, '        </div>' . "\n");
            fwrite($fichier, '' . "\n");

            if ($searching) {
                fwrite($fichier, '        {% if not app.request.get("id") %}' . "\n");
                fwrite($fichier, '        <div class="panel panel-flat border-top-success collapse" id="form-filtre">' . "\n");
                fwrite($fichier, '            <div class="panel-heading">' . "\n");
                fwrite($fichier, '                <h6 class="panel-title">{{ "Formulaire de recherche"|trans }}</h6>' . "\n");
                fwrite($fichier, '            </div>' . "\n");
                fwrite($fichier, '' . "\n");
                fwrite($fichier, '            <div class="panel-body">' . "\n");
                fwrite($fichier, '                {{ form_start(form_search) }}' . "\n");
                fwrite($fichier, '                <div class="row">' . "\n");

                foreach ($fields as $field) {
                    if ($field['search']) {
                        $prefix = ["" => ""];
                        if ($field['searchInter'])
                            $prefix = [" De" => "_de", " Au" => "_au"];
                        foreach ($prefix as $key => $prfx)
                            if ($field['type'] == 'boolean')
                                fwrite($fichier, '                    {{ form_checkbox("col-md-' . $field['col-md-search'] . ' m-t-25","' . $field['label'] . '"|trans,form_search.' . $field['fieldName'] . ') }}' . "\n");
                            else
                                fwrite($fichier, '                    {{ form_input("col-md-' . $field['col-md-search'] . '","' . $field['label'] . $key . '"|trans,form_search.' . $field['fieldName'] . $prfx . ') }}' . "\n");
                    }
                }

                fwrite($fichier, '                </div>' . "\n");
                fwrite($fichier, '                <hr>' . "\n");
                fwrite($fichier, '                <div class="text-right">' . "\n");
                fwrite($fichier, '                    <button type="submit" class="btn btn-primary"><i' . "\n");
                fwrite($fichier, '                                class="icon-search4  position-left"></i> {{ "Recherche"|trans }} </button>' . "\n");
                fwrite($fichier, '                    <button type="submit" class="btn btn-default"' . "\n");
                fwrite($fichier, '                            onclick="resetForm($(this).closest(\'form\'));"><i' . "\n");
                fwrite($fichier, '                                class="icon-filter4  position-left"></i> {{ "Réinitialiser"|trans }}  </button>' . "\n");
                fwrite($fichier, '                </div>' . "\n");
                fwrite($fichier, '                {{ form_end(form_search) }}' . "\n");
                fwrite($fichier, '            </div>' . "\n");
                fwrite($fichier, '        </div>' . "\n");
                fwrite($fichier, '        {% endif %}' . "\n");
                fwrite($fichier, '' . "\n");
            }

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        <div class="panel-body table-responsive">' . "\n");

            if ($knpPaginator) {
                fwrite($fichier, '            <div id="show-page" style="display: inline-flex;float: right;    margin-right: 10px;">' . "\n");

                fwrite($fichier, '                <div class="btn-group">' . "\n");
                fwrite($fichier, '                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cross2"></i> {{ "Supprimer tous les éléments"|trans }}<span class="caret"></span></button>' . "\n");
                fwrite($fichier, '                    <ul class="dropdown-menu dropdown-menu-right">' . "\n");
                fwrite($fichier, '                        <li><a href="javascript:void(0);" class="btn-multi-delete" data-items="checked"><i class="icon-checkbox-checked"></i> Cochés</a></li>' . "\n");
                fwrite($fichier, '                        <li><a href="javascript:void(0);" class="btn-multi-delete" data-items="filter"><i class="icon-filter4"></i> Dans le filtre</a></li>' . "\n");
                fwrite($fichier, '                        <li><a href="javascript:void(0);" class="btn-multi-delete" data-items="database"><i class="icon-database2"></i> Dans la Base</a></li>' . "\n");
                fwrite($fichier, '                    </ul>' . "\n");
                fwrite($fichier, '                </div>' . "\n");
                fwrite($fichier, '                ' . "\n");

                if ($exportExcel) {
                    fwrite($fichier, '                <div class="btn-group">' . "\n");
                    fwrite($fichier, '                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-file-excel"></i> {{ "Export excel de tous les éléments"|trans }}<span class="caret"></span></button>' . "\n");
                    fwrite($fichier, '                    <ul class="dropdown-menu dropdown-menu-right">' . "\n");
                    fwrite($fichier, '                        <li><a href="javascript:void(0);" class="btn-export-xsl" data-items="checked"><i class="icon-checkbox-checked"></i> Cochés</a></li>' . "\n");
                    fwrite($fichier, '                        <li><a href="javascript:void(0);" class="btn-export-xsl" data-items="filter"><i class="icon-filter4"></i> Dans le filtre</a></li>' . "\n");
                    fwrite($fichier, '                        <li><a href="javascript:void(0);" class="btn-export-xsl" data-items="database"><i class="icon-database2"></i> Dans la Base</a></li>' . "\n");
                    fwrite($fichier, '                    </ul>' . "\n");
                    fwrite($fichier, '                </div>' . "\n");
                    fwrite($fichier, '                ' . "\n");
                    fwrite($fichier, '                {#<a href="javascript:void(0);" type="button" class="btn btn-default btn-raised legitRipple ml-5" onclick=\'$("#file-xsl").click();$("#entity-name").val("' . $entity . '");\'><i class="icon-import position-left"></i> {{ "Import Excel"|trans }}</a>#}' . "\n");
                }

                fwrite($fichier, '                <span style="margin: 10px;">{{ "Afficher"|trans }}</span>' . "\n");
                fwrite($fichier, '                <select name="limit" class="select">' . "\n");
                fwrite($fichier, '                    <option value="10" selected>10</option>' . "\n");
                fwrite($fichier, '                    <option value="25">25</option>' . "\n");
                fwrite($fichier, '                    <option value="50">50</option>' . "\n");
                fwrite($fichier, '                    <option value="100">100</option>' . "\n");
                fwrite($fichier, '                    <option value="200">200</option>' . "\n");
                fwrite($fichier, '                    <option value="9999999999">{{ "Tous"|trans }}</option>' . "\n");
                fwrite($fichier, '                </select>' . "\n");
                fwrite($fichier, '                <span style="margin-right: 20px;margin-left: 10px;margin-top: 7px;">{{ "éléments"|trans }}</span>' . "\n");

                fwrite($fichier, '            </div>' . "\n");
            }

            fwrite($fichier, '' . "\n");

            $dataSet = "";
            if (!$knpPaginator)
                $dataSet = 'data-toggle="table" data-search="true" data-pagination="true"';

            fwrite($fichier, '            <table class="table table-striped" id="table-liste-' . $entity_lower . '" ' . $dataSet . '>' . "\n");
            fwrite($fichier, '                <thead>' . "\n");
            fwrite($fichier, '                <tr>' . "\n");
            fwrite($fichier, '                    <td>' . "\n");
            fwrite($fichier, '                        <div class="checkbox"><label><input type="checkbox" class="styled" onchange="$(\'#table-liste-' . $entity_lower . ' .chk-item\').prop(\'checked\',$(this).is(\':checked\')).uniform();"></label></div>' . "\n");
            fwrite($fichier, '                    </td>' . "\n");

            if (!$knpPaginator)
                fwrite($fichier, '                    <th data-field="id"></th>' . "\n");

            foreach ($fields as $field)
                if ($field['list']) {

                    $sort = "";
                    if ($knpPaginator) {
                        if (in_array($field['type'], ['datetime', 'date', 'integer', 'decimal', 'string', 'boolean']))
                            $sort = ' data-sort="' . $field['fieldName'] . '"';
                    } else
                        $sort = ' data-field="' . $field['fieldName'] . '"';

                    fwrite($fichier, '                    <th' . $sort . '>{{ "' . $field['label'] . '"|trans }}</th>' . "\n");
                }

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '                    <th class="action"' . ($knpPaginator ? '' : ' data-field="action"') . '>{{ "Actions"|trans }}</th>' . "\n");
            fwrite($fichier, '                </tr>' . "\n");
            fwrite($fichier, '                </thead>' . "\n");
            fwrite($fichier, '                <tbody id="table_div">' . "\n");

            if ($knpPaginator)
                fwrite($fichier, '                {{ include("' . $_folder . '/' . $entity . '/liste.html.twig",{"' . $entity_lower . 's":' . $entity_lower . 's}) }}' . "\n");

            fwrite($fichier, '                </tbody>' . "\n");
            fwrite($fichier, '            </table>' . "\n");
            fwrite($fichier, '        </div>' . "\n");

            if ($knpPaginator) {
                fwrite($fichier, '        <div class="pagination">' . "\n");
                fwrite($fichier, '            {{ include("Generale/knp_paginator/pagination_render.html.twig",{"items":itemsPagination}) }}' . "\n");
                fwrite($fichier, '        </div>' . "\n");
            }

            fwrite($fichier, '    </div>' . "\n");
            fwrite($fichier, '{% endblock %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '{% block javascripts %}' . "\n");
            fwrite($fichier, '    {{ parent() }}' . "\n");

            if ($uploadableField) {
                fwrite($fichier, '    <script type="text/javascript" src="{{ asset("assets/js/plugins/uploaders/fileinput/fileinput.min.js") }}"></script>' . "\n");
                fwrite($fichier, '    <script type="text/javascript" src="{{ asset("assets/js/init_uploader_bootstrap.js") }}"></script>' . "\n");
            }

            if (!$knpPaginator) {
                //fwrite($fichier, '    <script type="text/javascript" src="{{ asset("assets/js/bootstrap-table.min.js") }}"></script>' . "\n");
                //fwrite($fichier, '    <script type="text/javascript" src="{{ asset("assets/js/bootstrap-table-fr-FR.min.js") }}"></script>' . "\n");
                fwrite($fichier, '    <script>$( document ).ready(function() {$("#table-liste-' . $entity_lower . '").bootstrapTable("hideColumn", "id");});</script>' . "\n");

                fwrite($fichier, '' . "\n");
                fwrite($fichier, '    <script>' . "\n");
                fwrite($fichier, '        setTimeout(function(){' . "\n");

                fwrite($fichier, '            {% for ' . $entity_lower . ' in ' . $entity_lower . 's %}' . "\n");
                fwrite($fichier, '                {{ include("' . $_folder . '/' . $entity . '/item.html.twig",{"' . $entity_lower . '":' . $entity_lower . ',"list":true}) }}' . "\n");
                fwrite($fichier, '            {% endfor %}' . "\n");

                fwrite($fichier, '        }, 500);' . "\n");
                fwrite($fichier, '    </script>' . "\n");
                fwrite($fichier, '' . "\n");
            }

            if ($exportExcel)
                fwrite($fichier, '    <script type="text/javascript" src="{{ asset("assets/js/excelexportjs.js") }}"></script>' . "\n");

            fwrite($fichier, '    {{ include("Generale/jquery_crud.html.twig",{"entity":"' . $entity_lower . '"' . ($knpPaginator ? ',"pagination":true' : ',"bootstrapTable":true') . ($searching ? ',"searching":true' : '') . ($exportExcel ? ',"exportExcel":true' : '') . ($CKEDITOR ? ',"CKEDITOR":true' : '') . '}) }}' . "\n");

            fwrite($fichier, '{% endblock %}' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createIndexTwigSingleItem($entity, $folder, $copyOld, $copyTest)
    {
        try {
            $_folder = str_replace("\\", "/", $folder);
            $filename = "index.html";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\\templates\\$folder\\$entity";
            $path = "$dirname\\$filename.twig";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.php", "$filename.old$i.php", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $entity_lower = strtolower($entity);
            $route_name = str_replace('\\', '_', strtolower($folder)) . '_' . $entity_lower;

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '{% extends "base.html.twig" %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '{% block title %}{{ "' . $entity . '"|trans }}{% endblock %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '{% block stylesheets %}' . "\n");
            fwrite($fichier, '    {{ parent() }}' . "\n");
            fwrite($fichier, '    <link href="{{ asset("assets/css/style-crud.css") }}" rel="stylesheet" type="text/css">' . "\n");

            fwrite($fichier, '{% endblock %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '{% block body %}' . "\n");
            fwrite($fichier, '    <style>' . "\n");
            fwrite($fichier, '        div.icon-spinner3 {' . "\n");
            fwrite($fichier, '            font-size: 100%' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '        .m-t-25 {' . "\n");
            fwrite($fichier, '            margin-top: 25px;' . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '' . "\n");

            fwrite($fichier, '    </style>' . "\n");
            fwrite($fichier, '    <div id="message-alert"></div>' . "\n");

            fwrite($fichier, '    <div class="col-md-12">' . "\n");
            fwrite($fichier, '        <div class="panel panel-flat" id="panel-form">' . "\n");
            fwrite($fichier, '            <div class="panel-heading">' . "\n");
            fwrite($fichier, '                <h5 class="panel-title">{{ "' . $entity . 's"|trans }}' . "\n");

            fwrite($fichier, '                        <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>' . "\n");
            fwrite($fichier, '                <div class="heading-elements">' . "\n");
            fwrite($fichier, '                    <ul class="icons-list">' . "\n");
            fwrite($fichier, '                        <li><a data-action="collapse"></a></li>' . "\n");
            fwrite($fichier, '                        <li><a data-action="reload"></a></li>' . "\n");
            fwrite($fichier, '                        <li><a data-action="close"></a></li>' . "\n");
            fwrite($fichier, '                    </ul>' . "\n");
            fwrite($fichier, '                </div>' . "\n");
            fwrite($fichier, '            </div>' . "\n");
            fwrite($fichier, '            <div class="panel-body">' . "\n");
            fwrite($fichier, '                {{ include("' . $_folder . '/' . $entity . '/form.html.twig",{action:path("' . $route_name . '_index")}) }}' . "\n");
            fwrite($fichier, '            </div>' . "\n");
            fwrite($fichier, '        </div>' . "\n");
            fwrite($fichier, '    </div>' . "\n");
            fwrite($fichier, '{% endblock %}' . "\n");

            fwrite($fichier, '' . "\n");

            fwrite($fichier, '{% block javascripts %}' . "\n");
            fwrite($fichier, '    {{ parent() }}' . "\n");
            fwrite($fichier, '    <script>' . "\n");
            fwrite($fichier, '    	$(document).on("submit", "#panel-form form", function (e) {' . "\n");
            fwrite($fichier, '    		e.preventDefault();' . "\n");
            fwrite($fichier, '    ' . "\n");
            fwrite($fichier, '    		$("#panel-form .panel-body").LoadingOverlay("show", {' . "\n");
            fwrite($fichier, '    			image: "",' . "\n");
            fwrite($fichier, '    			fontawesome: "icon-spinner3 spinner"' . "\n");
            fwrite($fichier, '    		});' . "\n");
            fwrite($fichier, '    ' . "\n");
            fwrite($fichier, '    		$.ajax({' . "\n");
            fwrite($fichier, '    			type: "POST",' . "\n");
            fwrite($fichier, '    			url: $(this).attr("action"),' . "\n");
            fwrite($fichier, '    			data: new FormData(this),' . "\n");
            fwrite($fichier, '    			contentType: false,' . "\n");
            fwrite($fichier, '    			cache: false,' . "\n");
            fwrite($fichier, '    			processData: false,' . "\n");
            fwrite($fichier, '    			complete: function () {' . "\n");
            fwrite($fichier, '    				$("#panel-form .panel-body").LoadingOverlay("hide", true);' . "\n");
            fwrite($fichier, '    			},' . "\n");
            fwrite($fichier, '    			success: function () {' . "\n");
            fwrite($fichier, '    				toastr["success"]("Opération terminée avec succes");' . "\n");
            fwrite($fichier, '    			},' . "\n");
            fwrite($fichier, '    			error: function (jqXHR) {' . "\n");
            fwrite($fichier, '    				my_alert(jqXHR.responseText, "danger", "#message-alert");' . "\n");
            fwrite($fichier, '    			}' . "\n");
            fwrite($fichier, '    		});' . "\n");
            fwrite($fichier, '    ' . "\n");
            fwrite($fichier, '    	});' . "\n");
            fwrite($fichier, '    </script>' . "\n");
            fwrite($fichier, '{% endblock %}' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createItemTwig($entity, $folder, $fields, $copyOld, $copyTest, $knpPaginator, $viewItem)
    {
        try {
            $filename = "item.html";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\\templates\\$folder\\$entity";
            $path = "$dirname\\$filename.twig";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.twig", "$filename.old$i.twig", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $entity_lower = strtolower($entity);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '<tr id="{{ ' . $entity_lower . '.id }}">' . "\n");
            fwrite($fichier, '    <td>' . "\n");
            fwrite($fichier, '        <div class="checkbox"><label><input type="checkbox" value="{{ ' . $entity_lower . '.id }}" class="styled chk-item"></label></div>' . "\n");
            fwrite($fichier, '    </td>' . "\n");

            if (!$knpPaginator)
                fwrite($fichier, '    <td>{{ ' . $entity_lower . '.id }}</td>' . "\n");

            foreach ($fields as $field) {
                if ($field['list']) {
                    if ($field['type'] == "file") {
                        fwrite($fichier, '    <td><img src="{{ vich_uploader_asset(' . $entity_lower . ', "' . $field['fieldName'] . 'File")|default("https://via.placeholder.com/100x100?text="~' . $entity_lower . ') }}" height="100"></td>' . "\n");
                    } elseif ($field['relation'] == "ManyToMany") {
                        fwrite($fichier, '    <td>' . "\n");
                        fwrite($fichier, '        {% for item in ' . $entity_lower . '.' . $field['fieldName'] . ' %}' . "\n");
                        fwrite($fichier, '            {{ item }}{% if not loop.last %}, {% endif %}' . "\n");
                        fwrite($fichier, '        {% endfor %}' . "\n");
                        fwrite($fichier, '    </td>' . "\n");
                    } else {
                        $format = '';
                        $class = '';
                        switch ($field['type']) {
                            case "boolean":
                                $format = '|viewBooleanValue|raw';
                                break;
                            case "datetime":
                                $format = '|date("d/m/Y H:i")';
                                break;
                            case "date":
                                $format = '|date("d/m/Y")';
                                break;
                            case "time":
                                $format = '|date("H:i")';
                                break;
                            case "array":
                                $format = '|join(", ")';
                                break;
                            case "decimal":
                                $format = '|number_format(3,".","")';
                                $class = ' class="text-right"';
                                break;

                        }
                        fwrite($fichier, '    <td' . $class . '>{{ ' . $entity_lower . '.' . $field['getter'] . $format . ' }}</td>' . "\n");
                    }
                }
            }

            $route_name = str_replace('\\', '_', strtolower($folder)) . '_' . $entity_lower;

            fwrite($fichier, '' . "\n");
            fwrite($fichier, '    <td id="{{ ' . $entity_lower . '.id }}">' . "\n");

//            fwrite($fichier, '            <a href="javascript:void(0);" data-id="{{ ' . $entity_lower . '.id }}" data-action="{{ path("' . $route_name . '_delete", {"id": ' . $entity_lower . '.id}) }}"' . "\n");
//            fwrite($fichier, '               class="btn-delete-' . $entity_lower . '"><i class="icon-trash"></i></a>' . "\n");
//            fwrite($fichier, '            <a href="#modal-edit-' . $entity_lower . '" data-toggle="modal" data-action="{{ path("' . $route_name . '_edit", {"id": ' . $entity_lower . '.id}) }}"' . "\n");
//            fwrite($fichier, '               class="btn-edit-' . $entity_lower . '"><i class="icon-pencil"></i></a>' . "\n");
//            fwrite($fichier, '            <a href="#modal-edit-' . $entity_lower . '" data-toggle="modal" data-action="{{ path("' . $route_name . '_view", {"id": ' . $entity_lower . '.id}) }}"' . "\n");
//            fwrite($fichier, '               class="btn-edit-' . $entity_lower . '"><i class="icon-eye4"></i></a>' . "\n");

            $li_view = '';
            if ($viewItem)
                $li_view = '<li>{{ macro_bouttonModal(null,"modal-view-' . $entity_lower . '",path("' . $route_name . '_view",{"id":' . $entity_lower . '.id}),"icon-eye4","btn-view-' . $entity_lower . '","Consulter") }}</li>';

            fwrite($fichier, '        <ul class="icons-list">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-menu9"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    ' . $li_view . '
                    <li>{{ macro_bouttonModal(null,"modal-edit-' . $entity_lower . '",path("' . $route_name . '_edit",{"id":' . $entity_lower . '.id}),"icon-pencil","btn-edit-' . $entity_lower . '","Modifier") }}</li>
                    <li>{{ macro_bouttonModal(' . $entity_lower . '.id,null,path("' . $route_name . '_delete",{"id":' . $entity_lower . '.id}),"icon-trash","btn-delete-' . $entity_lower . '","Supprimer") }}</li>
                </ul>
            </li>
        </ul>' . "\n");


            fwrite($fichier, '    </td>' . "\n");
            fwrite($fichier, '</tr>' . "\n");

            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function create_ItemTwig($entity, $folder, $fields, $copyOld, $copyTest)
    {
        try {
            $filename = "item.html";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\\templates\\$folder\\$entity";
            $path = "$dirname\\$filename.twig";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.twig", "$filename.old$i.twig", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $entity_lower = strtolower($entity);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '{% if list is not defined %}<script>{% endif %}' . "\n");
            fwrite($fichier, '    $("#table-liste-' . $entity_lower . '").bootstrapTable("{{ index is defined ? "updateRow" : "insertRow" }}", {' . "\n");
            fwrite($fichier, '        index: {{ index is defined ? index : 0 }},' . "\n");
            fwrite($fichier, '        row: {' . "\n");

            fwrite($fichier, '        id: ' . "'" . '{{ ' . $entity_lower . '.id }}' . "'," . "\n");
            foreach ($fields as $field) {
                if ($field['list']) {
                    if ($field['type'] == "file") {
                        fwrite($fichier, '        ' . $field['fieldName'] . ': ' . "'" . '<img src="{{ vich_uploader_asset(' . $entity_lower . ', "' . $field['fieldName'] . 'File")|default("https://via.placeholder.com/100x100?text="~' . $entity_lower . ') }}" height="100">' . "'," . "\n");
                    } elseif ($field['relation'] == "ManyToMany") {
                        fwrite($fichier, '        ' . $field['fieldName'] . ': ' . "'" . '{% for item in ' . $entity_lower . '.' . $field['fieldName'] . ' %}{{ item }}{% if not loop.last %}, {% endif %}{% endfor %}' . "'," . "\n");
                    } else {
                        $format = '';
                        switch ($field['type']) {
                            case "boolean":
                                $format = '|viewBooleanValue|raw';
                                break;
                            case "datetime":
                                $format = '|date("d/m/Y H:i")';
                                break;
                            case "date":
                                $format = '|date("d/m/Y")';
                                break;
                            case "time":
                                $format = '|date("H:i")';
                                break;
                            case "array":
                                $format = '|join(", ")';
                                break;
                            case "decimal":
                                $format = '|number_format(3,".","")';
                                break;

                        }
                        fwrite($fichier, '        ' . $field['fieldName'] . ': ' . "'" . '{{ ' . $entity_lower . '.' . $field['getter'] . $format . ' }}' . "'," . "\n");
                    }
                }
            }

            $route_name = str_replace('\\', '_', strtolower($folder)) . '_' . $entity_lower;

            fwrite($fichier, '        action: "\n" +' . "\n");
            fwrite($fichier, '            ' . "'" . '<a href="javascript:void(0);" data-id="{{ ' . $entity_lower . '.id }}" data-action="{{ path("' . $route_name . '_delete", {"id": ' . $entity_lower . '.id}) }}"' . "'+" . "\n");
            fwrite($fichier, '               ' . "'" . 'class="btn-delete-' . $entity_lower . '"><i class="icon-trash"></i></a>' . "'+" . "\n");
            fwrite($fichier, '            ' . "'" . '<a href="#modal-edit-' . $entity_lower . '" data-toggle="modal" data-action="{{ path("' . $route_name . '_edit", {"id": ' . $entity_lower . '.id}) }}"' . "'+" . "\n");
            fwrite($fichier, '               ' . "'" . 'class="btn-edit-' . $entity_lower . '"><i class="icon-pencil"></i></a>' . "'" . "\n");
            fwrite($fichier, '        }' . "\n");
            fwrite($fichier, '    })' . "\n");
            fwrite($fichier, '{% if list is not defined %}</script>{% endif %}' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {

            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }

    function createListeTwig($entity, $folder, $countItemInList, $copyOld, $copyTest, $knpPaginator)
    {
        try {
            $_folder = str_replace("\\", "/", $folder);
            $filename = "liste.html";
            $project_dir = $this->getParameter('kernel.project_dir');
            if ($copyTest)
                $project_dir .= '\\TestCRUD';
            $dirname = "$project_dir\\templates\\$folder\\$entity";

            $path = "$dirname\\$filename.twig";

            if ($copyOld) {
                $i = 0;
                $old_path = $path;
                $rename = false;
                while (file_exists($old_path)) {
                    $old_path = str_replace("$filename.twig", "$filename.old$i.twig", $path);
                    $i++;
                    $rename = true;
                }
                if ($rename)
                    rename($path, $old_path);
            } elseif (file_exists($path))
                unlink($path);

            if (!is_dir($dirname))
                mkdir($dirname, 0777, true);

            $entity_lower = strtolower($entity);

            $fichier = fopen($path, 'c+b');
            fwrite($fichier, '{% for ' . $entity_lower . ' in ' . $entity_lower . 's %}' . "\n");
            fwrite($fichier, '    {{ include("' . $_folder . '/' . $entity . '/item.html.twig",{"' . $entity_lower . '":' . $entity_lower . '}) }}' . "\n");

            if ($knpPaginator) {
                fwrite($fichier, '{% else %}' . "\n");
                fwrite($fichier, '    <tr id="zero-item-' . $entity_lower . '">' . "\n");
                fwrite($fichier, '        <td class="text-center" colspan="' . ($countItemInList + 1) . '">{{ "Aucun enregistrement trouvé"|trans }}</td>' . "\n");
                fwrite($fichier, '    </tr>' . "\n");
            }

            fwrite($fichier, '{% endfor %}' . "\n");
            fwrite($fichier, '' . "\n");
            fwrite($fichier, '<script>' . "\n");
            fwrite($fichier, '    {% if app.request.xmlHttpRequest %}' . "\n");
            fwrite($fichier, '        $(".chk-item").uniform();' . "\n");
            fwrite($fichier, '    {% endif %}' . "\n");
            fwrite($fichier, '    $("#show-page").toggle({{ ' . $entity_lower . 's|length > 0 ? "true" : "false" }});' . "\n");
            fwrite($fichier, '</script>' . "\n");
            fclose($fichier);
            return ['path' => $path];
        } catch (\Exception $exception) {
            return ['path' => $path, 'erreur' => $exception->getMessage() . " - File: " . $exception->getFile() . " - Line: " . $exception->getLine()];
        }
    }
}
