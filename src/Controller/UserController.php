<?php

namespace App\Controller;

use App\Entity\User\User;
use App\Entity\User\UserSearch;
use App\Form\User\UserSearchType;
use App\Form\User\UserType;
use App\Repository\User\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_user_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator, UserRepository $userRepository): Response
    {
        $form = $this->createForm(UserType::class, null, ['mode' => 'add']);
        $search = new UserSearch();
        $form_search = $this->createForm(UserSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);

        $_users = $userRepository->getListUser($search, $sort, $order);
        $users = $paginator->paginate($_users, $request->query->getInt("page", 1), $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("User/liste.html.twig", ["users" => $users]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $users]);
            $countItem = count($_users);

            return new JsonResponse(compact(["content", "pagination", "data", "countItem"]));
        }

        return $this->render("User/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "users" => $users
        ]);
    }

    /**
     * @Route("/new", name="user_user_new", methods={"GET","POST"})
     */
    public function new(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['mode' => 'add']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($em->getRepository(User::class)->findOneByUsername($user->getUsername()))
                throw new \Exception("Username exist déja!");

            if ($em->getRepository(User::class)->findOneByEmail($user->getEmail()))
                throw new \Exception("Email exist déja!");

            if (method_exists($user, "setTranslatableLocale"))
                $user->setTranslatableLocale($request->getLocale());

            $passEncoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($passEncoded);

            $em->persist($user);
            $em->flush();

            $content = $this->renderView("User/item.html.twig", ["user" => $user]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/edit", name="user_user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($user, "setTranslatableLocale"))
                $user->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("User/item.html.twig", ["user" => $user]);
            return new Response($content);
        }

        $content = $this->renderView("User/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("user_user_edit", [
                "id" => $user->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="user_user_delete")
     */
    public function delete(User $user, EntityManagerInterface $em): Response
    {
        $em->remove($user);
        $em->flush();

        return new Response(1);
    }
}
