<?php

namespace App\Controller\Referentiel;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


use App\Entity\Referentiel\ModeService;
use App\Entity\Referentiel\Search\ModeServiceSearch;
use App\Form\Referentiel\Search\ModeServiceSearchType;
use App\Form\Referentiel\ModeServiceType;
use App\Repository\Referentiel\ModeServiceRepository;

/**
 * @Route("/referentiel/modeservice")
 */
class ModeServiceController extends AbstractController
{
    /**
     * @Route("/", name="referentiel_modeservice_index", methods={"GET"})
     */
    public function indexModeService(Request $request, PaginatorInterface $paginator, ModeServiceRepository $modeserviceRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ModeServiceType::class,null);
        $search = new ModeServiceSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(ModeServiceSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");

        if ($items)
            if ($items == 'all')
                $modeservices = $modeserviceRepository->getListModeService($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $modeservices = $modeserviceRepository->getListModeService($search, $sort, $order, $page, $limit);
            else
                $modeservices = $modeserviceRepository->getListModeService($search, $sort, $order, "all", "all", false, explode("-", $items));

        if ($multi_delete) {
            foreach ($modeservices as $modeservice)
                $em->remove($modeservice);
            $em->flush();
        }

        $modeservices = $modeserviceRepository->getListModeService($search, $sort, $order, $page, $limit,true);

        $countItem = $modeservices["nbr_result"];
        $modeservices = $modeserviceRepository->getListModeService($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("Referentiel/ModeService/liste.html.twig", ["modeservices" => $modeservices]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("Referentiel/ModeService/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "modeservices" => $modeservices,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="referentiel_modeservice_new", methods={"GET","POST"})
     */
    public function newModeService(Request $request, EntityManagerInterface $em): Response
    {
        $modeservice = new ModeService();
        $form = $this->createForm(ModeServiceType::class, $modeservice);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($modeservice, "setTranslatableLocale"))
                $modeservice->setTranslatableLocale($request->getLocale());
            $em->persist($modeservice);
            $em->flush();

            $content = $this->renderView("Referentiel/ModeService/item.html.twig", ["modeservice" => $modeservice]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/edit", name="referentiel_modeservice_edit", methods={"GET","POST"})
     */
    public function editModeService(Request $request, ModeService $modeservice, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ModeServiceType::class, $modeservice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($modeservice, "setTranslatableLocale"))
                $modeservice->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("Referentiel/ModeService/item.html.twig", ["modeservice" => $modeservice]);
            return new Response($content);
        }

        $content = $this->renderView("Referentiel/ModeService/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("referentiel_modeservice_edit", [
                "id" => $modeservice->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="referentiel_modeservice_delete")
     */
    public function deleteModeService(ModeService $modeservice, EntityManagerInterface $em): Response
    {
        $em->remove($modeservice);
        $em->flush();

        return new Response(1);
    }
}
