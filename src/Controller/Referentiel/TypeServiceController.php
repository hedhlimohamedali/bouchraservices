<?php

namespace App\Controller\Referentiel;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


use App\Entity\Referentiel\TypeService;
use App\Entity\Referentiel\Search\TypeServiceSearch;
use App\Form\Referentiel\Search\TypeServiceSearchType;
use App\Form\Referentiel\TypeServiceType;
use App\Repository\Referentiel\TypeServiceRepository;

/**
 * @Route("/referentiel/typeservice")
 */
class TypeServiceController extends AbstractController
{
    /**
     * @Route("/", name="referentiel_typeservice_index", methods={"GET"})
     */
    public function indexTypeService(Request $request, PaginatorInterface $paginator, TypeServiceRepository $typeserviceRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(TypeServiceType::class,null);
        $search = new TypeServiceSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(TypeServiceSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");
        if ($items)
            if ($items == 'all')
                $typeservices = $typeserviceRepository->getListTypeService($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $typeservices = $typeserviceRepository->getListTypeService($search, $sort, $order, $page, $limit);
            else
                $typeservices = $typeserviceRepository->getListTypeService($search, $sort, $order, "all", "all", false, explode("-", $items));
        
        if ($multi_delete) {
            foreach ($typeservices as $typeservice)
                $em->remove($typeservice);
            $em->flush();
        }
        

        $typeservices = $typeserviceRepository->getListTypeService($search, $sort, $order, $page, $limit,true);
        $countItem = $typeservices["nbr_result"];
        $typeservices = $typeserviceRepository->getListTypeService($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("Referentiel/TypeService/liste.html.twig", ["typeservices" => $typeservices]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("Referentiel/TypeService/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "typeservices" => $typeservices,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="referentiel_typeservice_new", methods={"GET","POST"})
     */
    public function newTypeService(Request $request, EntityManagerInterface $em): Response
    {
        $typeservice = new TypeService();
        $form = $this->createForm(TypeServiceType::class, $typeservice);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($typeservice, "setTranslatableLocale"))
                $typeservice->setTranslatableLocale($request->getLocale());
            $em->persist($typeservice);
            $em->flush();

            $content = $this->renderView("Referentiel/TypeService/item.html.twig", ["typeservice" => $typeservice]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/edit", name="referentiel_typeservice_edit", methods={"GET","POST"})
     */
    public function editTypeService(Request $request, TypeService $typeservice, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(TypeServiceType::class, $typeservice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($typeservice, "setTranslatableLocale"))
                $typeservice->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("Referentiel/TypeService/item.html.twig", ["typeservice" => $typeservice]);
            return new Response($content);
        }

        $content = $this->renderView("Referentiel/TypeService/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("referentiel_typeservice_edit", [
                "id" => $typeservice->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="referentiel_typeservice_delete")
     */
    public function deleteTypeService(TypeService $typeservice, EntityManagerInterface $em): Response
    {
        $em->remove($typeservice);
        $em->flush();

        return new Response(1);
    }
}
