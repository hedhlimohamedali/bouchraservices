<?php

namespace App\Controller\Referentiel;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


use App\Entity\Referentiel\Gouvernorat;
use App\Entity\Referentiel\Search\GouvernoratSearch;
use App\Form\Referentiel\Search\GouvernoratSearchType;
use App\Form\Referentiel\GouvernoratType;
use App\Repository\Referentiel\GouvernoratRepository;

/**
 * @Route("/referentiel/gouvernorat")
 */
class GouvernoratController extends AbstractController
{
    /**
     * @Route("/", name="referentiel_gouvernorat_index", methods={"GET"})
     */
    public function indexGouvernorat(Request $request, PaginatorInterface $paginator, GouvernoratRepository $gouvernoratRepository): Response
    {
        $form = $this->createForm(GouvernoratType::class,null);
        $search = new GouvernoratSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(GouvernoratSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");
        if ($items)
            if ($items == 'all')
                $gouvernorats = $gouvernoratRepository->getListGouvernorat($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $gouvernorats = $gouvernoratRepository->getListGouvernorat($search, $sort, $order, $page, $limit);
            else
                $gouvernorats = $gouvernoratRepository->getListGouvernorat($search, $sort, $order, "all", "all", false, explode("-", $items));
        
        if ($multi_delete) {
            foreach ($gouvernorats as $gouvernorat)
                $em->remove($gouvernorat);
            $em->flush();
        }
        
        $export_xsl = $request->query->getInt("export-xsl", 0);
        if ($export_xsl) {
            $data = [
            "Désignation",
            ];
            /** @var Gouvernorat $gouvernorat */
            foreach ($gouvernorats as $gouvernorat) {
                    $data[] = [
                     $gouvernorat->getLibelle(),
                ];
            }
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->fromArray($data, NULL, "A1");

            for ($i = "A"; $i !=  $sheet->getHighestColumn(); $i++)
                $sheet->getColumnDimension($i)->setAutoSize(TRUE);

            $sheet->getStyle("A1:Z1")->getFont()->setBold(true);

            $writer = new Xlsx($spreadsheet);
            $writer->save("export-pdf/Gouvernorats.xlsx");
            return new Response("/export-pdf/Gouvernorats.xlsx");
        }

        $gouvernorats = $gouvernoratRepository->getListGouvernorat($search, $sort, $order, $page, $limit,true);
        $countItem = $gouvernorats["nbr_result"];
        $gouvernorats = $gouvernoratRepository->getListGouvernorat($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("Referentiel/Gouvernorat/liste.html.twig", ["gouvernorats" => $gouvernorats]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("Referentiel/Gouvernorat/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "gouvernorats" => $gouvernorats,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="referentiel_gouvernorat_new", methods={"GET","POST"})
     */
    public function newGouvernorat(Request $request, EntityManagerInterface $em): Response
    {
        $gouvernorat = new Gouvernorat();
        $form = $this->createForm(GouvernoratType::class, $gouvernorat);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($gouvernorat, "setTranslatableLocale"))
                $gouvernorat->setTranslatableLocale($request->getLocale());
            $em->persist($gouvernorat);
            $em->flush();

            $content = $this->renderView("Referentiel/Gouvernorat/item.html.twig", ["gouvernorat" => $gouvernorat]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/edit", name="referentiel_gouvernorat_edit", methods={"GET","POST"})
     */
    public function editGouvernorat(Request $request, Gouvernorat $gouvernorat, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(GouvernoratType::class, $gouvernorat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($gouvernorat, "setTranslatableLocale"))
                $gouvernorat->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("Referentiel/Gouvernorat/item.html.twig", ["gouvernorat" => $gouvernorat]);
            return new Response($content);
        }

        $content = $this->renderView("Referentiel/Gouvernorat/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("referentiel_gouvernorat_edit", [
                "id" => $gouvernorat->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="referentiel_gouvernorat_delete")
     */
    public function deleteGouvernorat(Gouvernorat $gouvernorat, EntityManagerInterface $em): Response
    {
        $em->remove($gouvernorat);
        $em->flush();

        return new Response(1);
    }
}
