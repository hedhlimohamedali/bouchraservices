<?php

namespace App\Controller\Referentiel;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


use App\Entity\Referentiel\Repos;
use App\Entity\Referentiel\Search\ReposSearch;
use App\Form\Referentiel\Search\ReposSearchType;
use App\Form\Referentiel\ReposType;
use App\Repository\Referentiel\ReposRepository;

/**
 * @Route("/referentiel/repos")
 */
class ReposController extends AbstractController
{
    /**
     * @Route("/", name="referentiel_repos_index", methods={"GET"})
     */
    public function indexRepos(Request $request, PaginatorInterface $paginator, ReposRepository $reposRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ReposType::class,null);
        $search = new ReposSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(ReposSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");
        if ($items)
            if ($items == 'all')
                $reposs = $reposRepository->getListRepos($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $reposs = $reposRepository->getListRepos($search, $sort, $order, $page, $limit);
            else
                $reposs = $reposRepository->getListRepos($search, $sort, $order, "all", "all", false, explode("-", $items));
        
        if ($multi_delete) {
            foreach ($reposs as $repos)
                $em->remove($repos);
            $em->flush();
        }
        

        $reposs = $reposRepository->getListRepos($search, $sort, $order, $page, $limit,true);
        $countItem = $reposs["nbr_result"];
        $reposs = $reposRepository->getListRepos($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("Referentiel/Repos/liste.html.twig", ["reposs" => $reposs]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("Referentiel/Repos/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "reposs" => $reposs,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="referentiel_repos_new", methods={"GET","POST"})
     */
    public function newRepos(Request $request, EntityManagerInterface $em): Response
    {
        $repos = new Repos();
        $form = $this->createForm(ReposType::class, $repos);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($repos, "setTranslatableLocale"))
                $repos->setTranslatableLocale($request->getLocale());
            $em->persist($repos);
            $em->flush();

            $content = $this->renderView("Referentiel/Repos/item.html.twig", ["repos" => $repos]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/edit", name="referentiel_repos_edit", methods={"GET","POST"})
     */
    public function editRepos(Request $request, Repos $repos, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ReposType::class, $repos);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($repos, "setTranslatableLocale"))
                $repos->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("Referentiel/Repos/item.html.twig", ["repos" => $repos]);
            return new Response($content);
        }

        $content = $this->renderView("Referentiel/Repos/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("referentiel_repos_edit", [
                "id" => $repos->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="referentiel_repos_delete")
     */
    public function deleteRepos(Repos $repos, EntityManagerInterface $em): Response
    {
        $em->remove($repos);
        $em->flush();

        return new Response(1);
    }
}
