<?php

namespace App\Controller\Referentiel;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


use App\Entity\Referentiel\Archive;
use App\Entity\Referentiel\Search\ArchiveSearch;
use App\Form\Referentiel\Search\ArchiveSearchType;
use App\Form\Referentiel\ArchiveType;
use App\Repository\Referentiel\ArchiveRepository;

/**
 * @Route("/referentiel/archive")
 */
class ArchiveController extends AbstractController
{
    /**
     * @Route("/", name="referentiel_archive_index", methods={"GET"})
     */
    public function indexArchive(Request $request, PaginatorInterface $paginator, ArchiveRepository $archiveRepository, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ArchiveType::class,null);
        $search = new ArchiveSearch();
        if (isset($_GET["id"]))
            $search->setId($_GET["id"]);
        
        $form_search = $this->createForm(ArchiveSearchType::class, $search);
        $form_search->handleRequest($request);

        $sort = $request->query->get("sort", "id");
        $order = $request->query->get("order", "DESC");
        $limit = $request->query->getInt("limit", 10);
        $page = $request->query->getInt("page", 1);
        $multi_delete = $request->query->getInt("multi-delete", 0);

        $items = $request->get("items");
        if ($items)
            if ($items == 'all')
                $archives = $archiveRepository->getListArchive($search, $sort, $order, "all");
            elseif ($items == 'filtre')
                $archives = $archiveRepository->getListArchive($search, $sort, $order, $page, $limit);
            else
                $archives = $archiveRepository->getListArchive($search, $sort, $order, "all", "all", false, explode("-", $items));
        
        if ($multi_delete) {
            foreach ($archives as $archive)
                $em->remove($archive);
            $em->flush();
        }
        

        $archives = $archiveRepository->getListArchive($search, $sort, $order, $page, $limit,true);
        $countItem = $archives["nbr_result"];
        $archives = $archiveRepository->getListArchive($search, $sort, $order, $page, $limit);
        $itemsPagination = $paginator->paginate(array_fill(0, $countItem, ""), $page, $limit);

        if ($request->isXmlHttpRequest()) {
            $content = $this->renderView("Referentiel/Archive/liste.html.twig", ["archives" => $archives]);
            $pagination = $this->renderView("Generale/knp_paginator/pagination_render.html.twig", ["items" => $itemsPagination]);

            return new JsonResponse(compact(["content", "pagination", "countItem"]));
        }

        return $this->render("Referentiel/Archive/index.html.twig", [
            "form" => $form->createView(),
            "form_search" => $form_search->createView(),
            "archives" => $archives,
            "itemsPagination" => $itemsPagination,
            "countItem" => $countItem
        ]);
    }

    /**
     * @Route("/new", name="referentiel_archive_new", methods={"GET","POST"})
     */
    public function newArchive(Request $request, EntityManagerInterface $em): Response
    {
        $archive = new Archive();
        $form = $this->createForm(ArchiveType::class, $archive);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (method_exists($archive, "setTranslatableLocale"))
                $archive->setTranslatableLocale($request->getLocale());
            $em->persist($archive);
            $em->flush();

            $content = $this->renderView("Referentiel/Archive/item.html.twig", ["archive" => $archive]);
            return new Response($content);
        }
    }

    /**
     * @Route("/{id}/edit", name="referentiel_archive_edit", methods={"GET","POST"})
     */
    public function editArchive(Request $request, Archive $archive, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ArchiveType::class, $archive);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($archive, "setTranslatableLocale"))
                $archive->setTranslatableLocale($request->getLocale());
            $em->flush();

            $content = $this->renderView("Referentiel/Archive/item.html.twig", ["archive" => $archive]);
            return new Response($content);
        }

        $content = $this->renderView("Referentiel/Archive/form.html.twig", [
            "form" => $form->createView(),
            "action" => $this->generateUrl("referentiel_archive_edit", [
                "id" => $archive->getId()
            ])
        ]);
        return new Response($content);
    }

    /**
     * @Route("/{id}/delete", name="referentiel_archive_delete")
     */
    public function deleteArchive(Archive $archive, EntityManagerInterface $em): Response
    {
        $em->remove($archive);
        $em->flush();

        return new Response(1);
    }
}
