<?php
/**
 * Created by PhpStorm.
 * User: MohamedAli
 * Date: 21/04/2020
 * Time: 06:38
 */

namespace App\Service\Generale;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\Response;

require __DIR__ . '/../../../public/lib/index.php';

class Generale
{
    private $em;
    private $container;

    /**
     * GeneraleExtension constructor.
     * @param $em
     * @param $container
     */
    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function getFieldsGouvernorat()
    {
        return array(
            'entity_name' => 'gouvernorat',
            'entity_fields' =>
                array(
                    'libelle' =>
                        array(
                            'libelle' => 'Désignation',
                            'type' => 'string',
                            'required' => true,
                        ),
                ),
        );
    }

    public function getFieldsSociete()
    {
        return array(
            'entity_name' => 'societe',
            'entity_fields' =>
                array(
                    'nom' =>
                        array(
                            'libelle' => 'Nom',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'code' =>
                        array(
                            'libelle' => 'Code',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'activite' =>
                        array(
                            'libelle' => 'Activité',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'gerant' =>
                        array(
                            'libelle' => 'Gérant',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'tel' =>
                        array(
                            'libelle' => 'Tél',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'gsm' =>
                        array(
                            'libelle' => 'GSM',
                            'type' => 'string',
                            'required' => false,
                        ),
                    'fax' =>
                        array(
                            'libelle' => 'Fax',
                            'type' => 'string',
                            'required' => false,
                        ),
                    'email' =>
                        array(
                            'libelle' => 'Email',
                            'type' => 'string',
                            'required' => false,
                        ),
                    'adresse' =>
                        array(
                            'libelle' => 'Adresse',
                            'type' => 'string',
                            'required' => false,
                        ),
                    'avis' =>
                        array(
                            'libelle' => 'Avis',
                            'type' => 'integer',
                            'required' => false,
                        ),
                    'rib' =>
                        array(
                            'libelle' => 'RIB',
                            'type' => 'string',
                            'required' => false,
                        ),
                    'matricule_fiscale' =>
                        array(
                            'libelle' => 'Matricule Fiscale',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'registre_commerce' =>
                        array(
                            'libelle' => 'Registre Commerce',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'remarque' =>
                        array(
                            'libelle' => 'Remarque',
                            'type' => 'text',
                            'required' => false,
                        ),
                    'pieceJoint' =>
                        array(
                            'libelle' => 'Pièce Joint',
                            'type' => 'file',
                            'required' => false,
                        )
                ),
            'foreign_fields' =>
                array(
                    'gouvernorat' =>
                        array(
                            'class' => 'App\\Entity\\Referentiel\\Gouvernorat',
                            'key' => 'libelle',
                            'entity_fields' =>
                                array(
                                    'libelle' =>
                                        array(
                                            'libelle' => 'Gouvernorat',
                                            'type' => 'string',
                                            'required' => false,
                                        )
                                ),
                        ),
                ),
        );
    }

    public function getFieldsPays()
    {
        return array(
            'entity_name' => 'pays',
            'entity_fields' =>
                array(
                    'code' =>
                        array(
                            'libelle' => 'Code',
                            'type' => 'string',
                            'required' => true,
                        ),
                    'libelle' =>
                        array(
                            'libelle' => 'Désignation',
                            'type' => 'string',
                            'required' => true,
                        ),
                ),
        );
    }

    function getValueFieldSheet($col, $data, $field, $fields, $ligne)
    {
        $error = null;
        if ($col == "")
            $value = "";
        else {
            $data[$col] = str_replace([';', ','], [',', '.'], $data[$col]);
            $value = trim($data[$col]);
        }

        if ($value == "") {
            if ($fields[$field]['required'])
                $error = "<b>Ligne $ligne</b>: le champ `<b>{$fields[$field]['libelle']}</b>` est vide";
            else
                $value = "";
        } elseif ($fields[$field]['type'] == 'date' and !$this->validateDate($value))
            $error = "<b>Ligne $ligne</b>: le format de champ `<b>{$fields[$field]['libelle']}</b>` est non valide, le format valide est aaaa-mm-jj";
        elseif ($fields[$field]['type'] == 'numeric' and !is_numeric($value))
            $error = "<b>Ligne $ligne</b>: le champ `<b>{$fields[$field]['libelle']}</b>` doit être un champ numérique";

        if ($field == 'cnrps')
            $value = (int)$value;

        return [
            'value' => $value,
            'error' => $error
        ];
    }

    public function getFiledsFileXSL($file, $entityName, $name, $add_html = '')
    {
        ini_set("memory_limit", "-1");
        $fields = "getFields$entityName";
        $_fields = $this->$fields();

        $fields = $_fields['entity_fields'];
        $foreign_fields = isset($_fields['foreign_fields']) ? $_fields['foreign_fields'] : [];
        foreach ($foreign_fields as $key => $item) {
            $this->array_key_prefix_suffix($item['entity_fields'], "$key.");
            $fields = array_merge($fields, $item['entity_fields']);
        }

        $file->move("files-excel/$entityName/", $name);

        $spreadsheet = IOFactory::load("files-excel/$entityName/$name");
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $html = '<h5>Nombre des lignes = {{ nbrLignes }}</h5>
                <input type="hidden" name="file-path" value="{{ file_path }}">
                <input type="hidden" name="file-name" value="{{ file_name }}">
                <input type="hidden" name="entity-name" value="{{ entity_name }}">
                ' . $add_html . '
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Col. Base des données</th>
                            <th>Col. Fichier excel</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for key,field in fields %}
                            <tr>
                                <td>{{ field.libelle }}{% if field.required %} <span class="symbol required"></span>{% endif %}</td>
                                <td>
                                    <select name="cols-sheet[{{ key }}]" class="select"{% if field.required %} required{% endif %}>
                                        <option value="">Aucun</option>
                                        {% for key,sheet in sheets %}
                                            <option value="{{ key }}">{{ sheet }}</option>
                                        {% endfor %}
                                    </select>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>';
        $loader = new \Twig\Loader\ArrayLoader([
            'modale.html.twig' => $html,
        ]);
        $twig = new \Twig\Environment($loader);
        return new Response($twig->render('modale.html.twig', [
            'nbrLignes' => count($sheetData) - 1,
            'fields' => $fields,
            'sheets' => $sheetData[1],
            'file_name' => $name,
            'entity_name' => $entityName,
            'file_path' => "files-excel/$entityName/$name"]));
    }

    public function importData($entityName, $colsSheet, $file_path)
    {
        $fields = "getFields$entityName";
        $_fields = $this->$fields();

        $entity_name = $_fields['entity_name'];

        $fields = $_fields['entity_fields'];

        $foreign_fields = isset($_fields['foreign_fields']) ? $_fields['foreign_fields'] : [];
        $_foreign_fields = $foreign_fields;
        $this->array_key_prefix_suffix($_foreign_fields, '', '_id');

        $sql_fields = implode(',', array_merge(array_keys($fields), array_keys($_foreign_fields)));

        foreach ($foreign_fields as $key => $item) {
            ${"{$key}s"} = $this->em->createQueryBuilder()
                ->select("$key.id , $key.{$item['key']}")
                ->from($item['class'], $key)
                ->orderBy("$key.id", 'ASC')
                ->getQuery()->getResult();

            ${"{$key}s"} = array_column(${"{$key}s"}, 'id', $item['key']);
            ${"lignesInsert" . ucwords($key)} = "";

            $this->array_key_prefix_suffix($item['entity_fields'], "$key.");
            $fields = array_merge($fields, $item['entity_fields']);
        }

        $spreadsheet = IOFactory::load($file_path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        unlink($file_path);
        unset($sheetData[1]);
        $lignesInsert = "";

        $error = [];
        foreach ($sheetData as $ligne => $data) {

            foreach ($colsSheet as $field => $col) {
                ${"$field"} = $this->getValueFieldSheet($col, $data, $field, $fields, $ligne);
                if (${"$field"}['error'])
                    $error[] = ${"$field"}['error'];
                ${"$field"} = ${"$field"}['value'];
            }

            $ids = [];
            foreach ($foreign_fields as $key => $item) {

                $key_field = "$key.{$item['key']}";
                if (isset(${"{$key}s"}[${"$key_field"}]))
                    $id = ${"{$key}s"}[${"$key_field"}];
                else {
                    $id = !empty(${"{$key}s"}) ? (end(${"{$key}s"}) + 1) : 1;
                    ${"{$key}s"}[${"$key_field"}] = $id;

                    ${"lignesInsert" . ucwords($key)} .= "$id";
                    foreach ($item['entity_fields'] as $_key => $entity_field) {
                        $__key = $key . "." . $_key;
                        ${"lignesInsert" . ucwords($key)} .= ";${"$__key"}";
                    }
                    ${"lignesInsert" . ucwords($key)} .= "\n";
                }
                $ids[] = $id;
            }

            $vals = [];
            foreach (array_keys($fields) as $key)
                if (!strpos($key, '.'))
                    $vals[] = ${"$key"};
            $vals = array_merge($vals, $ids);
            $lignesInsert .= implode(';', $vals) . "\n";
        }

        if (!empty($error))
            return $error;

        $conn = $this->em->getConnection();
        foreach ($foreign_fields as $key => $item) {
            if (${"lignesInsert" . ucwords($key)} != "") {
                $_fields = implode(',', array_keys($item['entity_fields']));
                file_put_contents("files-excel/$key.txt", ${"lignesInsert" . ucwords($key)});
                $realpath = realpath("files-excel/$key.txt");
                $realpath = str_replace('\\', '/', $realpath);
                $sql = "LOAD DATA LOCAL INFILE '$realpath' 
                         INTO TABLE $key
                         FIELDS TERMINATED BY ';'
                         (id,$_fields);";
                $stmt = $conn->prepare($sql);
                //$stmt->execute();
            }
        }

        file_put_contents("files-excel/$entity_name.txt", $lignesInsert);
        $realpath = realpath("files-excel/$entity_name.txt");
        $realpath = str_replace('\\', '/', $realpath);
        $sql = "LOAD DATA LOCAL INFILE '$realpath' 
                         INTO TABLE $entity_name
                         FIELDS TERMINATED BY ';'
                         ($sql_fields);";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return true;
    }

    function array_key_prefix_suffix(&$array, $prefix = '', $suffix = '')
    {
        if (empty($array))
            return false;
        $key_array = array_keys($array);
        $key_string = $prefix . implode($suffix . ',' . $prefix, $key_array) . $suffix;
        $key_array = explode(',', $key_string);
        $array = array_combine($key_array, $array);
    }

    function array_group_by($key, $array)
    {
        return array_reduce($array, function (array $accumulator, array $element) use ($key) {
            $accumulator[$element[$key]][] = $element;

            return $accumulator;
        }, []);
    }

    public function printEtatWord($file, $reference, $params, $path = 'EtatWord/pdf')
    {
        if (file_exists("EtatWord/customize/$file.docx"))
            $templateProcessor = new TemplateProcessor("EtatWord/customize/$file.docx");
        else
            $templateProcessor = new TemplateProcessor("EtatWord/default/$file.docx");

        function array_key_suffix(&$array, $suffix = '')
        {
            array_key_prefix_suffix($array, '', $suffix);
            foreach ($array as &$item)
                if (is_array($item) and !empty($item))
                    array_key_suffix($item, $suffix);
        }

        function array_key_prefix_suffix(&$array, $prefix = '', $suffix = '')
        {
            $key_array = array_keys($array);
            $key_string = $prefix . implode($suffix . ',' . $prefix, $key_array) . $suffix;
            $key_array = explode(',', $key_string);
            $array = array_combine($key_array, $array);
        }

        function valSpecialChars(&$value)
        {
            $value = strip_tags($value);
            $value = html_entity_decode($value, ENT_QUOTES, "UTF-8");
            $value = htmlspecialchars($value, ENT_COMPAT, 'UTF-8');
            $value = preg_replace('~\R~u', '</w:t><w:br/><w:t>', $value);
            return $value;
        }

        function setValuesTemplate($templateProcessor, $params)
        {
            foreach ($templateProcessor->getVariables() as $variable) {
                $code = '';
                if (strlen($variable) >= 4)
                    $code = substr($variable, 0, 6);

                if (array_key_exists($variable, $params))
                    if ($code == 'image_') {
                        if (file_exists($params[$variable])) {
                            try {
                                $templateProcessor->setImageValue($variable, $params[$variable]);
                            } catch (\Exception $exception) {
                                $templateProcessor->setValue($variable, '');
                            }
                        } else
                            $templateProcessor->setValue($variable, $params[$variable]);
                    } elseif ($code == 'thtml_') {
                        $wordTable = new \PhpOffice\PhpWord\Element\Table();
                        $wordTable->addRow();
                        $cell = $wordTable->addCell();
                        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $params[$variable]);
                        $templateProcessor->setComplexValue($variable, $wordTable);
                    } elseif ($code == 'block_') {
                        $templateProcessor->cloneBlock($variable, count($params[$variable]), true, true);
                        $i = 1;
                        foreach ($params[$variable] as &$param) {
                            array_key_suffix($param, "#$i");

                            setValuesTemplate($templateProcessor, $param);
                            setValuesTemplate($templateProcessor, $param);
                            $i++;
                        }
                    } else
                        $templateProcessor->setValue($variable, valSpecialChars($params[$variable]));

                if ($code == 'table_') {
                    $key_row = substr($variable, 0, strpos($variable, '.'));
                    if (strpos($variable, '#'))
                        $key_row .= substr($variable, strpos($variable, '#'));

                    if (in_array($variable, $templateProcessor->getVariables()) and isset($params[$key_row]) and isset(current($params[$key_row])[$variable])) {
                        foreach ($params[$key_row] as &$item)
                            foreach ($item as &$_item)
                                valSpecialChars($_item);
                        $templateProcessor->cloneRowAndSetValues($variable, array_values($params[$key_row]));
                    }

                }
            }
        }

        setValuesTemplate($templateProcessor, $params);

        if (!is_dir($path))
            mkdir($path, 0777, true);

        if (!is_dir("EtatWord/pdf"))
            mkdir("EtatWord/pdf", 0777, true);

        $templateProcessor->saveAs("EtatWord/pdf/$file-$reference.docx");

        //install in server : sudo apt-get install libreoffice
        exec("libreoffice --headless --convert-to pdf EtatWord/pdf/$file-$reference.docx --outdir $path/", $output, $worked);
        unlink("EtatWord/pdf/$file-$reference.docx");

        return true;
    }

    /**
     * @param $url
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public function checkURL($url, array $options = array())
    {
        if (empty($url)) {
            throw new \Exception('URL is empty');
        }

        // list of HTTP status codes
        $httpStatusCodes = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            207 => 'Multi-Status',
            208 => 'Already Reported',
            226 => 'IM Used',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => 'Switch Proxy',
            307 => 'Temporary Redirect',
            308 => 'Permanent Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Payload Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            418 => 'I\'m a teapot',
            422 => 'Unprocessable Entity',
            423 => 'Locked',
            424 => 'Failed Dependency',
            425 => 'Unordered Collection',
            426 => 'Upgrade Required',
            428 => 'Precondition Required',
            429 => 'Too Many Requests',
            431 => 'Request Header Fields Too Large',
            449 => 'Retry With',
            450 => 'Blocked by Windows Parental Controls',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates',
            507 => 'Insufficient Storage',
            508 => 'Loop Detected',
            509 => 'Bandwidth Limit Exceeded',
            510 => 'Not Extended',
            511 => 'Network Authentication Required',
            599 => 'Network Connect Timeout Error'
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if (isset($options['timeout'])) {
            $timeout = (int)$options['timeout'];
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        }

        curl_exec($ch);
        $returnedStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (array_key_exists($returnedStatusCode, $httpStatusCodes)) {
            return $returnedStatusCode == 200;
        } else {
            return false;
        }
    }
}