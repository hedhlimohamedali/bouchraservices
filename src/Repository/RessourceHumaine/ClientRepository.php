<?php

namespace App\Repository\RessourceHumaine;

use App\Entity\RessourceHumaine\Client;
use App\Entity\RessourceHumaine\Search\ClientSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * @return Client[] Returns an array of Client objects
     */
    public function getListClient(ClientSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("client");

        if ($count)
            $query->select("COUNT(client.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("client.id", $ids));

        if ($search->getId())
            $query->andWhere("client.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getNomPrenom())
            $query->andWhere($query->expr()->like("UPPER(client.nomPrenom)", "UPPER('%".$search->getNomPrenom()."%')"));

        if ($search->getCin())
            $query->andWhere($query->expr()->like("UPPER(client.cin)", "UPPER('%".$search->getCin()."%')"));

        if ($search->getDateCinDe())
            $query->andWhere("client.dateCin >= :dateCin_de")
                ->setParameter("dateCin_de", $search->getDateCinDe());

        if ($search->getDateCinAu())
            $query->andWhere("client.dateCin <= :dateCin_au")
                ->setParameter("dateCin_au", $search->getDateCinAu());

        if ($search->getTel())
            $query->andWhere($query->expr()->like("UPPER(client.tel)", "UPPER('%".$search->getTel()."%')"));

        if ($search->getGsm())
            $query->andWhere($query->expr()->like("UPPER(client.gsm)", "UPPER('%".$search->getGsm()."%')"));

        if ($search->getFax())
            $query->andWhere($query->expr()->like("UPPER(client.fax)", "UPPER('%".$search->getFax()."%')"));

        if ($search->getEmail())
            $query->andWhere($query->expr()->like("UPPER(client.email)", "UPPER('%".$search->getEmail()."%')"));

        if ($search->getAdresse())
            $query->andWhere($query->expr()->like("UPPER(client.adresse)", "UPPER('%".$search->getAdresse()."%')"));

        if ($search->getAvisDe())
            $query->andWhere("client.avis >= :avis_de")
                ->setParameter("avis_de", $search->getAvisDe());

        if ($search->getAvisAu())
            $query->andWhere("client.avis <= :avis_au")
                ->setParameter("avis_au", $search->getAvisAu());

        if ($search->getGouvernorat())
            $query->andWhere("client.gouvernorat = :gouvernorat")
                ->setParameter("gouvernorat", $search->getGouvernorat());

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("client.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
