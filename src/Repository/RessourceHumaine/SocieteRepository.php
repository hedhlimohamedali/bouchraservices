<?php

namespace App\Repository\RessourceHumaine;

use App\Entity\RessourceHumaine\Societe;
use App\Entity\RessourceHumaine\Search\SocieteSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Societe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Societe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Societe[]    findAll()
 * @method Societe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocieteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Societe::class);
    }

    /**
     * @return Societe[] Returns an array of Societe objects
     */
    public function getListSociete(SocieteSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("societe");

        if ($count)
            $query->select("COUNT(societe.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("societe.id", $ids));

        if ($search->getId())
            $query->andWhere("societe.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getNom())
            $query->andWhere($query->expr()->like("UPPER(societe.nom)", "UPPER('%".$search->getNom()."%')"));

        if ($search->getCode())
            $query->andWhere($query->expr()->like("UPPER(societe.code)", "UPPER('%".$search->getCode()."%')"));

        if ($search->getActivite())
            $query->andWhere($query->expr()->like("UPPER(societe.activite)", "UPPER('%".$search->getActivite()."%')"));

        if ($search->getGerant())
            $query->andWhere($query->expr()->like("UPPER(societe.gerant)", "UPPER('%".$search->getGerant()."%')"));

        if ($search->getTel())
            $query->andWhere($query->expr()->like("UPPER(societe.tel)", "UPPER('%".$search->getTel()."%')"));

        if ($search->getGsm())
            $query->andWhere($query->expr()->like("UPPER(societe.gsm)", "UPPER('%".$search->getGsm()."%')"));

        if ($search->getFax())
            $query->andWhere($query->expr()->like("UPPER(societe.fax)", "UPPER('%".$search->getFax()."%')"));

        if ($search->getEmail())
            $query->andWhere($query->expr()->like("UPPER(societe.email)", "UPPER('%".$search->getEmail()."%')"));

        if ($search->getGouvernorat())
            $query->andWhere("societe.gouvernorat = :gouvernorat")
                ->setParameter("gouvernorat", $search->getGouvernorat());

        if ($search->getAdresse())
            $query->andWhere($query->expr()->like("UPPER(societe.adresse)", "UPPER('%".$search->getAdresse()."%')"));

        if ($search->getAvisDe())
            $query->andWhere("societe.avis >= :avis_de")
                ->setParameter("avis_de", $search->getAvisDe());

        if ($search->getAvisAu())
            $query->andWhere("societe.avis <= :avis_au")
                ->setParameter("avis_au", $search->getAvisAu());

        if ($search->getRib())
            $query->andWhere($query->expr()->like("UPPER(societe.rib)", "UPPER('%".$search->getRib()."%')"));

        if ($search->getMatriculeFiscale())
            $query->andWhere($query->expr()->like("UPPER(societe.matriculeFiscale)", "UPPER('%".$search->getMatriculeFiscale()."%')"));

        if ($search->getRegistreCommerce())
            $query->andWhere($query->expr()->like("UPPER(societe.registreCommerce)", "UPPER('%".$search->getRegistreCommerce()."%')"));

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("societe.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
