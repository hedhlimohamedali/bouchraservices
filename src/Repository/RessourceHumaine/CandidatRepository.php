<?php

namespace App\Repository\RessourceHumaine;

use App\Entity\RessourceHumaine\Candidat;
use App\Entity\RessourceHumaine\Search\CandidatSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Candidat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Candidat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Candidat[]    findAll()
 * @method Candidat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Candidat::class);
    }

    /**
     * @return Candidat[] Returns an array of Candidat objects
     */
    public function getListCandidat(CandidatSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("candidat");

        if ($count)
            $query->select("COUNT(candidat.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("candidat.id", $ids));

        if ($search->getId())
            $query->andWhere("candidat.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getCivilite())
            $query->andWhere($query->expr()->like("UPPER(candidat.civilite)", "UPPER('%".$search->getCivilite()."%')"));

        if ($search->getNomPrenom())
            $query->andWhere($query->expr()->like("UPPER(candidat.nomPrenom)", "UPPER('%".$search->getNomPrenom()."%')"));

        if ($search->getNumIdentite())
            $query->andWhere($query->expr()->like("UPPER(candidat.numIdentite)", "UPPER('%".$search->getNumIdentite()."%')"));

        if ($search->getDateNaissanceDe())
            $query->andWhere("candidat.dateNaissance >= :dateNaissance_de")
                ->setParameter("dateNaissance_de", $search->getDateNaissanceDe());

        if ($search->getDateNaissanceAu())
            $query->andWhere("candidat.dateNaissance <= :dateNaissance_au")
                ->setParameter("dateNaissance_au", $search->getDateNaissanceAu());

        if ($search->getLieuNaissance())
            $query->andWhere($query->expr()->like("UPPER(candidat.lieuNaissance)", "UPPER('%".$search->getLieuNaissance()."%')"));

        if ($search->getGouvernorat())
            $query->andWhere("candidat.gouvernorat = :gouvernorat")
                ->setParameter("gouvernorat", $search->getGouvernorat());

        if ($search->getAdresse())
            $query->andWhere($query->expr()->like("UPPER(candidat.adresse)", "UPPER('%".$search->getAdresse()."%')"));

        if ($search->getTel1())
            $query->andWhere($query->expr()->like("UPPER(candidat.tel1)", "UPPER('%".$search->getTel1()."%')"));

        if ($search->getTel2())
            $query->andWhere($query->expr()->like("UPPER(candidat.tel2)", "UPPER('%".$search->getTel2()."%')"));

        if ($search->getTel3())
            $query->andWhere($query->expr()->like("UPPER(candidat.tel3)", "UPPER('%".$search->getTel3()."%')"));

        if ($search->getModeService())
            $query->andWhere("candidat.modeService = :modeService")
                ->setParameter("modeService", $search->getModeService());

        if ($search->getTypeService())
            $query->andWhere("candidat.typeService = :typeService")
                ->setParameter("typeService", $search->getTypeService());

        if ($search->getRepos())
            $query->andWhere("candidat.repos = :repos")
                ->setParameter("repos", $search->getRepos());

        if ($search->getAnneeInscrire())
            $query->andWhere($query->expr()->like("UPPER(candidat.anneeInscrire)", "UPPER('%".$search->getAnneeInscrire()."%')"));

        if ($search->getNumFiche())
            $query->andWhere($query->expr()->like("UPPER(candidat.numFiche)", "UPPER('%".$search->getNumFiche()."%')"));

        if ($search->getArchive())
            $query->andWhere("candidat.archive = :archive")
                ->setParameter("archive", $search->getArchive());

        if ($search->getEtoileDe())
            $query->andWhere("candidat.etoile >= :etoile_de")
                ->setParameter("etoile_de", $search->getEtoileDe());

        if ($search->getEtoileAu())
            $query->andWhere("candidat.etoile <= :etoile_au")
                ->setParameter("etoile_au", $search->getEtoileAu());

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("candidat.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
