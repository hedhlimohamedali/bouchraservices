<?php

namespace App\Repository\User;

use App\Entity\User\User;
use App\Entity\User\UserSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function getListUser(UserSearch $search,$sort="id",$order="DESC")
    {
        $query = $this->createQueryBuilder("user");


        if ($search->getNomPrenom())
            $query->andWhere($query->expr()->like("UPPER(user.nom_prenom)", "UPPER('%".$search->getNomPrenom()."%')"));

        if ($search->getUsername())
            $query->andWhere($query->expr()->like("UPPER(user.username)", "UPPER('%".$search->getUsername()."%')"));

        if ($search->getEmail())
            $query->andWhere($query->expr()->like("UPPER(user.email)", "UPPER('%".$search->getEmail()."%')"));

        if ($search->getEnabled())
            $query->andWhere("user.enabled = :enabled")
                ->setParameter("enabled", $search->getEnabled());

        return $query
            ->orderBy("user.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
