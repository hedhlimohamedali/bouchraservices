<?php

namespace App\Repository\Referentiel;

use App\Entity\Referentiel\TypeService;
use App\Entity\Referentiel\Search\TypeServiceSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeService|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeService|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeService[]    findAll()
 * @method TypeService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeService::class);
    }

    /**
     * @return TypeService[] Returns an array of TypeService objects
     */
    public function getListTypeService(TypeServiceSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("typeservice");

        if ($count)
            $query->select("COUNT(typeservice.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("typeservice.id", $ids));

        if ($search->getId())
            $query->andWhere("typeservice.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getLibelle())
            $query->andWhere($query->expr()->like("UPPER(typeservice.libelle)", "UPPER('%".$search->getLibelle()."%')"));

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("typeservice.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
