<?php

namespace App\Repository\Referentiel;

use App\Entity\Referentiel\Archive;
use App\Entity\Referentiel\Search\ArchiveSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Archive|null find($id, $lockMode = null, $lockVersion = null)
 * @method Archive|null findOneBy(array $criteria, array $orderBy = null)
 * @method Archive[]    findAll()
 * @method Archive[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArchiveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Archive::class);
    }

    /**
     * @return Archive[] Returns an array of Archive objects
     */
    public function getListArchive(ArchiveSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("archive");

        if ($count)
            $query->select("COUNT(archive.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("archive.id", $ids));

        if ($search->getId())
            $query->andWhere("archive.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getLibelle())
            $query->andWhere($query->expr()->like("UPPER(archive.libelle)", "UPPER('%".$search->getLibelle()."%')"));

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("archive.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
