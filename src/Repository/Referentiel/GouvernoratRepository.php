<?php

namespace App\Repository\Referentiel;

use App\Entity\Referentiel\Gouvernorat;
use App\Entity\Referentiel\Search\GouvernoratSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gouvernorat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gouvernorat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gouvernorat[]    findAll()
 * @method Gouvernorat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GouvernoratRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gouvernorat::class);
    }

    /**
     * @return Gouvernorat[] Returns an array of Gouvernorat objects
     */
    public function getListGouvernorat(GouvernoratSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("gouvernorat");

        if ($count)
            $query->select("COUNT(gouvernorat.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("gouvernorat.id", $ids));

        if ($search->getId())
            $query->andWhere("gouvernorat.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getLibelle())
            $query->andWhere($query->expr()->like("UPPER(gouvernorat.libelle)", "UPPER('%".$search->getLibelle()."%')"));

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("gouvernorat.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
