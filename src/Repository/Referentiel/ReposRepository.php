<?php

namespace App\Repository\Referentiel;

use App\Entity\Referentiel\Repos;
use App\Entity\Referentiel\Search\ReposSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Repos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Repos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Repos[]    findAll()
 * @method Repos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReposRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Repos::class);
    }

    /**
     * @return Repos[] Returns an array of Repos objects
     */
    public function getListRepos(ReposSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("repos");

        if ($count)
            $query->select("COUNT(repos.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("repos.id", $ids));

        if ($search->getId())
            $query->andWhere("repos.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getLibelle())
            $query->andWhere($query->expr()->like("UPPER(repos.libelle)", "UPPER('%".$search->getLibelle()."%')"));

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("repos.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
