<?php

namespace App\Repository\Referentiel;

use App\Entity\Referentiel\ModeService;
use App\Entity\Referentiel\Search\ModeServiceSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModeService|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModeService|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModeService[]    findAll()
 * @method ModeService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModeServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModeService::class);
    }

    /**
     * @return ModeService[] Returns an array of ModeService objects
     */
    public function getListModeService(ModeServiceSearch $search,$sort="id",$order="DESC", $page = 1, $limit = 10, $count = false, $ids = [])
    {
        $query = $this->createQueryBuilder("modeservice");

        if ($count)
            $query->select("COUNT(modeservice.id) As nbr_result");

        if (!empty($ids))
            $query->andWhere($query->expr()->in("modeservice.id", $ids));

        if ($search->getId())
            $query->andWhere("modeservice.id = :id")
                ->setParameter("id", $search->getId());

        if ($search->getLibelle())
            $query->andWhere($query->expr()->like("UPPER(modeservice.libelle)", "UPPER('%".$search->getLibelle()."%')"));

        if ($count)
            return $query->getQuery()->getOneOrNullResult();

        if (is_integer($page) and is_integer($limit))
            $query->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        return $query
            ->orderBy("modeservice.$sort", $order)
            ->getQuery()
            ->getResult();
    }
}
