public function getFieldsSociete(){
 return array (
  'entity_name' => 'societe',
  'entity_fields' => 
  array (
    'nom' => 
    array (
      'libelle' => 'Nom',
      'type' => 'string',
      'required' => true,
    ),
    'code' => 
    array (
      'libelle' => 'Code',
      'type' => 'string',
      'required' => true,
    ),
    'activite' => 
    array (
      'libelle' => 'Activité',
      'type' => 'string',
      'required' => true,
    ),
    'gerant' => 
    array (
      'libelle' => 'Gérant',
      'type' => 'string',
      'required' => true,
    ),
    'tel' => 
    array (
      'libelle' => 'Tél',
      'type' => 'string',
      'required' => true,
    ),
    'gsm' => 
    array (
      'libelle' => 'GSM',
      'type' => 'string',
      'required' => false,
    ),
    'fax' => 
    array (
      'libelle' => 'Fax',
      'type' => 'string',
      'required' => false,
    ),
    'email' => 
    array (
      'libelle' => 'Email',
      'type' => 'string',
      'required' => false,
    ),
    'adresse' => 
    array (
      'libelle' => 'Adresse',
      'type' => 'string',
      'required' => false,
    ),
    'avis' => 
    array (
      'libelle' => 'Avis',
      'type' => 'integer',
      'required' => false,
    ),
    'rib' => 
    array (
      'libelle' => 'RIB',
      'type' => 'string',
      'required' => false,
    ),
    'matricule_fiscale' => 
    array (
      'libelle' => 'Matricule Fiscale',
      'type' => 'string',
      'required' => true,
    ),
    'registre_commerce' => 
    array (
      'libelle' => 'Registre Commerce',
      'type' => 'string',
      'required' => true,
    ),
    'remarque' => 
    array (
      'libelle' => 'Remarque',
      'type' => 'text',
      'required' => false,
    ),
    'pieceJoint' => 
    array (
      'libelle' => 'Pièce Joint',
      'type' => 'file',
      'required' => false,
    ),
    'updated' => 
    array (
      'libelle' => 'updated',
      'type' => 'datetime',
      'required' => true,
    ),
  ),
  'foreign_fields' => 
  array (
    'gouvernorat' => 
    array (
      'class' => 'App\\Entity\\Referentiel\\Gouvernorat',
      'key' => '',
      'entity_fields' => 
      array (
      ),
    ),
  ),
)
;}